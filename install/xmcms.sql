/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50067
Source Host           : localhost:3306
Source Database       : xmcms

Target Server Type    : MYSQL
Target Server Version : 50067
File Encoding         : 65001

Date: 2014-02-23 20:00:22
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `xm_admingroup`
-- ----------------------------
DROP TABLE IF EXISTS `xm_admingroup`;
CREATE TABLE `xm_admingroup` (
  `admingroup_id` int(11) NOT NULL auto_increment,
  `groupname` varchar(255) default NULL,
  PRIMARY KEY  (`admingroup_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_admingroup
-- ----------------------------
INSERT INTO xm_admingroup VALUES ('1', '内容发布组');

-- ----------------------------
-- Table structure for `xm_admingroup_permission`
-- ----------------------------
DROP TABLE IF EXISTS `xm_admingroup_permission`;
CREATE TABLE `xm_admingroup_permission` (
  `admingroup_id` int(11) default NULL,
  `authlang` varchar(255) default NULL,
  `self` tinyint(4) default NULL,
  `operpermission` text,
  `modulepermission` text,
  `actpermission` varchar(255) default NULL,
  `operpermissionarray` text,
  `actpermissionarray` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_admingroup_permission
-- ----------------------------
INSERT INTO xm_admingroup_permission VALUES ('1', 'zh_cn', '0', 'a:28:{i:0;s:9:\"sysconfig\";i:1;s:19:\"sysconfig_basicinfo\";i:2;s:16:\"sysconfig_upload\";i:3;s:10:\"pageconfig\";i:4;s:16:\"pageconfig_flash\";i:5;s:17:\"pageconfig_online\";i:6;s:12:\"moduleconfig\";i:7;s:19:\"moduleconfig_config\";i:8;s:13:\"contentconfig\";i:9;s:21:\"contentconfig_content\";i:10;s:19:\"contentconfig_basic\";i:11;s:21:\"contentconfig_article\";i:12;s:21:\"contentconfig_product\";i:13;s:22:\"contentconfig_download\";i:14;s:20:\"contentconfig_images\";i:15;s:22:\"contentconfig_employee\";i:16;s:21:\"contentconfig_message\";i:17;s:22:\"contentconfig_feedback\";i:18;s:18:\"contentconfig_foot\";i:19;s:19:\"contentconfig_other\";i:20;s:9:\"seoconfig\";i:21;s:20:\"seoconfig_friendlink\";i:22;s:10:\"userconfig\";i:23;s:15:\"userconfig_user\";i:24;s:16:\"userconfig_group\";i:25;s:16:\"userconfig_admin\";i:26;s:21:\"userconfig_admingroup\";i:27;s:17:\"userconfig_config\";}', 's:68:\"12,20,22,23,13,25,26,14,27,44,28,45,15,16,30,31,17,18,34,37,38,39,40\";', '', 'a:28:{s:9:\"sysconfig\";s:9:\"sysconfig\";s:19:\"sysconfig_basicinfo\";s:19:\"sysconfig_basicinfo\";s:16:\"sysconfig_upload\";s:16:\"sysconfig_upload\";s:10:\"pageconfig\";s:10:\"pageconfig\";s:16:\"pageconfig_flash\";s:16:\"pageconfig_flash\";s:17:\"pageconfig_online\";s:17:\"pageconfig_online\";s:12:\"moduleconfig\";s:12:\"moduleconfig\";s:19:\"moduleconfig_config\";s:19:\"moduleconfig_config\";s:13:\"contentconfig\";s:13:\"contentconfig\";s:21:\"contentconfig_content\";s:21:\"contentconfig_content\";s:19:\"contentconfig_basic\";s:19:\"contentconfig_basic\";s:21:\"contentconfig_article\";s:21:\"contentconfig_article\";s:21:\"contentconfig_product\";s:21:\"contentconfig_product\";s:22:\"contentconfig_download\";s:22:\"contentconfig_download\";s:20:\"contentconfig_images\";s:20:\"contentconfig_images\";s:22:\"contentconfig_employee\";s:22:\"contentconfig_employee\";s:21:\"contentconfig_message\";s:21:\"contentconfig_message\";s:22:\"contentconfig_feedback\";s:22:\"contentconfig_feedback\";s:18:\"contentconfig_foot\";s:18:\"contentconfig_foot\";s:19:\"contentconfig_other\";s:19:\"contentconfig_other\";s:9:\"seoconfig\";s:9:\"seoconfig\";s:20:\"seoconfig_friendlink\";s:20:\"seoconfig_friendlink\";s:10:\"userconfig\";s:10:\"userconfig\";s:15:\"userconfig_user\";s:15:\"userconfig_user\";s:16:\"userconfig_group\";s:16:\"userconfig_group\";s:16:\"userconfig_admin\";s:16:\"userconfig_admin\";s:21:\"userconfig_admingroup\";s:21:\"userconfig_admingroup\";s:17:\"userconfig_config\";s:17:\"userconfig_config\";}', '');

-- ----------------------------
-- Table structure for `xm_announcement`
-- ----------------------------
DROP TABLE IF EXISTS `xm_announcement`;
CREATE TABLE `xm_announcement` (
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_announcement
-- ----------------------------

-- ----------------------------
-- Table structure for `xm_article`
-- ----------------------------
DROP TABLE IF EXISTS `xm_article`;
CREATE TABLE `xm_article` (
  `article_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `description` text,
  `createtime` date default NULL,
  `orderby` int(11) default NULL,
  `sender_id` int(11) default NULL,
  `module_id` int(11) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  PRIMARY KEY  (`article_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_article
-- ----------------------------
INSERT INTO xm_article VALUES ('26', '如何选择网站关键词?', '网站关键词是SEO优化的核心，关键词的选择将直接影响网站优化推广效果及网站的价值，选择关键词应该注意一下几点：<br />\r\n<br />\r\n考虑用户搜索习惯，而并非企业名称或产品名称，尤其是对于一种新的产品，当市场还没有人知道的时候，自然不会有人去搜索这个关键词，因此选择关键词首先就应该去分析你的目标群体会在搜索引擎中搜索哪些关键词，而你应该选择那些与你的产品相关并能为你的销售带来帮助的关键词。<br />\r\n<br />\r\n中小企业切忌不要将自己的公司名称或品牌作为主要关键词，因为在你的目标群体中，很多人是没有听说过你的公司和品牌的，小企业做产品，对于中小企业来说，客户一般都是通过产品和服务来了解你的公司和品牌的，因此，网站关键词如果设置为公司名称，则不能达到良好的营销效果。<br />\r\n<br />\r\n关键词不宜过长，太长的关键词很少会有人搜索；点击量不宜太热，点击量太大搜索的人越多那么竞争也就会越激烈；同时关键词也不能太冷门，冷门关键词排到第一位也不会有多少人搜索访问，具体可以参考“百度指数”中的关键词访问量。<br />\r\n<br />\r\n碰到热门关键词时，最好在关键词前面或后面加上地域限制，譬如“塑钢门窗”，这个关键词竞争是非常激烈，但是“长沙塑钢门窗”就比较好做了，而且如果你做的是有地域限制的业务，那么其他地方搜索到你的网站也没有多大意义。<br />\r\n<br />\r\n标题关键词应该控制在1-3个之间，除非第四个以后的关键词是比较冷门的，否则在标题中添加多个关键词是没有任何意义的，反而会影响主关键词的排名。<br />', '2013-12-03', '0', '2', '26', 'zh_cn');
INSERT INTO xm_article VALUES ('28', '为什么企业要建多国语言网站？', '<p style=\"text-align:start;\">\r\n	互联网在不断发展壮大，已成为企业和个人寻求生意机会，对商品、服务和信息进行了解的首选方式。从站在增强一个企业竞争优势的角度看，建设一个多语言网站是不断增加客户数量的前提和提高销售的高效手段。\r\n</p>\r\n<div style=\"text-align:start;\">\r\n	随着国际化进程的不断加快，多语言网站将成为企业和组织必不可少的一部分。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站帮助企业面对众多的非英语国家互联网用户\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	随着时间变化，非英语国家互联网用户的比例已不断在攀升。相比以前由讲英语的用户控制互联网的局面已产生了根本改变。尼尔森集团(一个美国互联网研究咨询公司)在05年3月谈到：国外互联网市场是“将要成熟的果子”，只要你愿意付出较少的努力就能够得到巨大的收获。结果显示，互联网的增长在美国、德国、英国和瑞典趋于缓慢，在其他国家例如法国，香港、意大利和日本则增长显着。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	尼尔森的一位高级分析员阐述：“最容易得到的机会是在这个国家的互联网使用习惯和用户(或站点)关系被确立之前。在当前成长阶段培养的客户将成为未来忠诚客户的基础。”多语言网站是企业有效的营销工具有能力对多数互联网用户用他们自己的语言进行沟通，这不仅仅意味着销售的增加，更可贵的是在当地市场建立起有关您的品牌、服务和产品的知晓。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	建设多语言网站是一个非常伟大的计划，因为它很可能是企业进行市场营销，抓住新的用户，建立与新的客户关系和赋予企业品牌一个国际形象的最有效方式。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站能为企业带来新的顾客\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	通过把您的多语言网站暴露在各个国家，地区潜在的顾客面前，自然而然地您会获得这些本国语用户的注意。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站能够为企业增加销售\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	在网站上每增加一种语言都有可能潜在的提高一倍的销售额。即便只把网站建成只包含有西班牙语，法语，德语和意大利语这四种世界主要语言的网站，其销售额也有可能潜在的提升4倍。这是一种较少见的方式利用如此小的投资获得如此大的效果。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站能够传达以顾客为中心的服务意识\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	您的多语言网站显示出您为顾客思考。您以顾客为中心的思想及关爱顾客的努力将得到顾客的感激，因为您的这份额外的工作，他们将会更多的倾向于惠顾您的企业。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站能够获得更多的信任\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	许多通过互联网执行的交易往往由于彼此并不熟悉相互的语言而产生不同文化间的信任问题。提供给顾客多语言的选择帮助他们感到安全，确切地清楚他们在如何交易，与谁交易。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站帮助克服文化上的敏感\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	一个经过恰当设计的多语言网站，通过使用目标市场的本土语能够克服潜在的文化障碍。因为多语言网站自动为客户创造了“文化舒适区”，使他们能够自如地导航，了解网站信息，并与您展开互动。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站能够有效打击竞争者\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	获得竞争优势需要在当今的环境中跳出思维的框架。许多企业试图把自己与竞争者区别出来。观察您的竞争者，如果他们有多语种网站为什么您没有呢？假若他们没有，为什么不在他们之前主导这个市场首先建立你公司的品牌呢？\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	多语言网站展示企业的国际性风范\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	一个多语种网站展示您的思想，经营及国际性的业务流程。国际性企业形象及行业领导地位由此树立多语言网站帮助面向不同地域国家的查寻引擎，搜索引擎会带着人们来到您的站点。然而在一些国家和地区有他们自己受欢迎的搜索引擎，主要是这些搜索引擎依据母语的使用习惯来工作，适应了人们的需要，所以获得了成功。这类搜索引擎引擎是开启本地市场的钥匙，除此之外，除非他们能够使用一种特定的语言(比如英语)否则很难找到您。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	另外，许多关键字搜索引擎，尤其是Google，在多语言开发方面能力优异。拥有多语言页面的网站能够最大限度的保障您的网站能够被众多搜索引擎检索并展示。事务总是在不断地转移和变化。当前多语言网站仍处在早期阶段，许多跨国公司使用多语言网站来巩固其国际地位。然而，接下来的影响将自然地发生，多语言网站将成为互联网存在的主要部分。不管企业是选择现在或将来，这都是网站建设的唯一选择。\r\n</div>', '2013-12-03', '0', '2', '25', 'zh_cn');
INSERT INTO xm_article VALUES ('27', '企业网站应该多长时间备份一次？', '<div style=\"text-align:start;\">\r\n	企业网站的信息量一般比较少，使用MetInfo企业网站管理系统让网站备份操作非常简单，我们建议用户没有必要经常去备份网站，一般只需要做到一下几点即可：\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		网站初次安装、初次配置、添加基本内容可以备份数据库及整站打包下载到本地；\r\n	</li>\r\n	<li>\r\n		网站运行中可以更加信息的更新频率1-3个月备份一次数据库；\r\n	</li>\r\n	<li>\r\n		如果你的网站图片特别多，且担心空间不稳定，你可以选择1-2个月备份一次网站上传文件。\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	&nbsp;\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	注意：当将备份文件下载到本地后，为了节省空间，建议删除服务器上的备份文件。\r\n</div>', '2013-12-03', '0', '2', '26', 'zh_cn');
INSERT INTO xm_article VALUES ('29', '如何获取XmCMS网站管理系统商业授权？', '方法一：参加“个性化网站模板换取商业授权活动”<br />\r\n提供一套符合《XmCMS用户手册》模板制作规范要求的企业网站模板，便可以申请一个永久商业授权。具体要求如下：<br />\r\n1）修改官方或已有模板者需要风格和功能与已有模板有明显的区别；<br />\r\n2）模板文件应符合《XmCMS用户手册》模板制作规范要求，所有文件完整有序；<br />\r\n3）模板首页栏目、相关联系方式及一切重要信息均可以通过后台配置进行修改；<br />\r\n&nbsp;<br />\r\n方法二：直接购买永久商业授权<br />\r\n普通版1000元，高级版3000元。<br />\r\n&nbsp;<br />\r\n商业授权常见问答：<br />\r\n1、已经100%开源为什么还要收取商业授权费用？<br />\r\n官方解答：XmCMS企业网站管理系统是一款100%开源的企业网站管理系统，对于个人网站及非赢利网站是可以永久免费使用的，如果您是单位用户且将我们的系统用于商业用途，我们需要收取相应的版权使用费，以维持软件的开发成本和进一步发展，商业用户包括但不仅限于企业、事业单位、政府部门、学校等等；当然您也可以参加我们的促销活动，免费获取商业授权。<br />\r\n2、获取商业授权有什么好处？<br />\r\n官方解答：除了得到官方正式商业授权外，可以享受相应等级的官方技术支持服务。<br />\r\n3、哪些网站不需要购买商业授权？如何区分个人网站和单位网站？<br />\r\n官方解答：所有个人网站（不限制用途）、非盈利性质的单位网站不需要购买商业版权，但未经商业授权许可，不允许去除官方版权信息。个人网站是指网站所有信息均以个人名义（包括无工商登记的个人和个体工商户）呈现的商业和非商业网站，以个人名义注册的域名和备案但用于宣传企业的网站不属于个人网站范畴。', '2013-12-03', '0', '2', '13', 'zh_cn');
INSERT INTO xm_article VALUES ('30', '商业版和免费版在系统功能上有区别吗？', '商业版和免费版在系统功能上没有任何区别。<br />\r\n商业版和免费版区别：<br />\r\n如您将程序用于商业用途，请自觉购买商业授权，否则我们将保留追究法律责任的权利；<br />\r\n免费版和商业版在网站功能上没有任何区别，商业用户可以获取专业的技术支持服务；<br />\r\n免费版如去除【Powered by XmCMS】版权标识将不能正常运行！新麦网并将追求相应的法律责任！', '2013-12-03', '0', '2', '13', 'zh_cn');

-- ----------------------------
-- Table structure for `xm_config`
-- ----------------------------
DROP TABLE IF EXISTS `xm_config`;
CREATE TABLE `xm_config` (
  `syskey` varchar(255) default NULL,
  `content` text,
  `keytype` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  `lang` varchar(255) NOT NULL default 'zh_cn'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_config
-- ----------------------------
INSERT INTO xm_config VALUES ('webname', '惠山区长安新麦网络信息技术服务部', 'basic', '网站名称', 'zh_cn');
INSERT INTO xm_config VALUES ('weblogo', '/attached/image/20131114/20131114060041_48367.png', 'basic', '网站LOGO', 'zh_cn');
INSERT INTO xm_config VALUES ('weburl', 'http://www.x-mai.com', 'basic', '网站网址', 'zh_cn');
INSERT INTO xm_config VALUES ('webkeyword', '新麦CRM,新麦CMS，新麦信息管理系统,新麦客户关系管理系统', 'basic', '网站关键字', 'zh_cn');
INSERT INTO xm_config VALUES ('webdesc', '客户满意，就是我们的追求', 'basic', '网站描述', 'zh_cn');
INSERT INTO xm_config VALUES ('showchange', null, 'lang', '显示网站语言切换', 'zh_cn');
INSERT INTO xm_config VALUES ('defaultlang', 'zh_cn', 'lang', '网站默认语言', 'zh_cn');
INSERT INTO xm_config VALUES ('copyright', 'r34sdfsdfsd', 'foot', '版权信息', 'zh_cn');
INSERT INTO xm_config VALUES ('postcode', 'rrr3s但是复苏的', 'foot', '地址邮编', 'zh_cn');
INSERT INTO xm_config VALUES ('contact', 'rrrsdfwe2', 'foot', '联系方式', 'zh_cn');
INSERT INTO xm_config VALUES ('othercode', '435问问', 'foot', '第三方代码', 'zh_cn');
INSERT INTO xm_config VALUES ('otherinfo', '43522斯蒂芬', 'foot', '底部其他信息', 'zh_cn');
INSERT INTO xm_config VALUES ('senduser', 'dddss', 'emailconfig', '发件人姓名', 'system');
INSERT INTO xm_config VALUES ('sendemail', 'dd', 'emailconfig', '邮箱帐号', 'system');
INSERT INTO xm_config VALUES ('emailsmtp', 'dd', 'emailconfig', '邮件SMTP服务器', 'system');
INSERT INTO xm_config VALUES ('emailpwd', 'dd', 'emailconfig', '邮箱密码', 'system');
INSERT INTO xm_config VALUES ('icp', '苏ICP备13028336号', 'basic', 'ICP备案号', 'zh_cn');
INSERT INTO xm_config VALUES ('open', '0', 'urlrewrite', '开启伪静态', 'system');
INSERT INTO xm_config VALUES ('webtel', '1', 'basic', '网站联系电话', 'zh_cn');
INSERT INTO xm_config VALUES ('webphone', '15358062815', 'basic', '网站联系手机', 'zh_cn');
INSERT INTO xm_config VALUES ('webemail', '458820281@qq.com', 'basic', '网站联系Email', 'zh_cn');
INSERT INTO xm_config VALUES ('webname', '惠山区长安新麦网络信息技术服务部', 'basic', '网站名称', 'en_us');
INSERT INTO xm_config VALUES ('weblogo', '/attached/image/20131114/20131114060041_48367.png', 'basic', '网站LOGO', 'en_us');
INSERT INTO xm_config VALUES ('weburl', 'http://www.x-mai.com', 'basic', '网站网址', 'en_us');
INSERT INTO xm_config VALUES ('webkeyword', '新麦CRM,新麦CMS，新麦信息管理系统,新麦客户关系管理系统', 'basic', '网站关键字', 'en_us');
INSERT INTO xm_config VALUES ('webdesc', '客户满意，就是我们的追求', 'basic', '网站描述', 'en_us');
INSERT INTO xm_config VALUES ('showchange', null, 'lang', '显示网站语言切换', 'en_us');
INSERT INTO xm_config VALUES ('defaultlang', 'en_us', 'lang', '网站默认语言', 'en_us');
INSERT INTO xm_config VALUES ('copyright', 'r34sdfsdfsd', 'foot', '版权信息', 'en_us');
INSERT INTO xm_config VALUES ('postcode', 'rrr3s但是复苏的', 'foot', '地址邮编', 'en_us');
INSERT INTO xm_config VALUES ('contact', 'rrrsdfwe2', 'foot', '联系方式', 'en_us');
INSERT INTO xm_config VALUES ('othercode', '435问问', 'foot', '第三方代码', 'en_us');
INSERT INTO xm_config VALUES ('otherinfo', '43522斯蒂芬', 'foot', '底部其他信息', 'en_us');
INSERT INTO xm_config VALUES ('icp', '苏ICP备13028336号', 'basic', 'ICP备案号', 'en_us');
INSERT INTO xm_config VALUES ('open', '0', 'urlrewrite', '开启伪静态', 'system');
INSERT INTO xm_config VALUES ('webtel', '', 'basic', '网站联系电话', 'en_us');
INSERT INTO xm_config VALUES ('webphone', '15358062815', 'basic', '网站联系手机', 'en_us');
INSERT INTO xm_config VALUES ('webemail', '458820281@qq.com', 'basic', '网站联系Email', 'en_us');
INSERT INTO xm_config VALUES ('theme', 'classic', 'theme', '系统主题', 'system');

-- ----------------------------
-- Table structure for `xm_download`
-- ----------------------------
DROP TABLE IF EXISTS `xm_download`;
CREATE TABLE `xm_download` (
  `download_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `description` text,
  `createtime` date default NULL,
  `orderby` int(11) NOT NULL default '0',
  `module_id` int(11) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `sender_id` int(11) default NULL,
  `fileurl` varchar(255) NOT NULL,
  `filetitle` varchar(255) default NULL,
  PRIMARY KEY  (`download_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_download
-- ----------------------------
INSERT INTO xm_download VALUES ('1', '新麦CMS企业网站管理系统v2.0 正式版【免费下载】', '&nbsp;XmCMS企业网站管理系统采用PHP+Mysql架构，全站内置了SEO搜索引擎优化机制，支持用户自定义界面语言(全球各种语言)，拥有企业网站常用的模块功能（企业简介模块、新闻模块、产品模块、下载模块、图片模块、招聘模块、在线留言、反馈系统、在线交流、友情链接、网站地图、会员与权限管理）。强大灵活的后台管理功能、个性化模块添加功能、不同栏目自定义FLASH样式功能等可为企业打造出大气漂亮且具有营销力的精品网站。<br />\r\n<br />\r\n<div style=\"text-align:start;\">\r\n	支持语言\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		支持全球各种语言（包括简体中文、繁体中文、英文、日文等等），用户可以无限自定义多种语言，并可以在后台根据需要设置相应语言访问域名和自定义默认首页；\r\n	</li>\r\n	<li>\r\n		独创的语言包编辑功能，用户可以通过网站管理后台轻松的编辑网站语言包,从而控制前台显示语言和语言参数（字体、文字大小、网页背景、显示字数等等）；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	界面风格\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		全站文字、系统栏目、信息内容及主要图片支持用户自定义，所见即可改；\r\n	</li>\r\n	<li>\r\n		独创的栏目标识显示控制方法，管理员只需要通过设置栏目标识便可以灵活的控制首页显示栏目及相关信息列表等；\r\n	</li>\r\n	<li>\r\n		前台模板与程序完全分离，独创的各种语言共用一套风格模板技术，也可以为每种语言设置不同的页面风格；\r\n	</li>\r\n	<li>\r\n		风格模板制作简单灵活，同时支持标签化模板制作及PHP标准语法模板制作方法；\r\n	</li>\r\n	<li>\r\n		独创的同一套模板支持不同的颜色风格、同时支持多种首页布局，用户可以通过后台轻松切换；\r\n	</li>\r\n	<li>\r\n		缩略图自动生成、图片文字水印后台自定义、上传图片时水印自动添加功能；\r\n	</li>\r\n	<li>\r\n		Flash动画（广告）可以根据栏目设置不同样式（包括图片轮播、Flash动画、单张图片），并可以选择多种不同的图片轮播方式；\r\n	</li>\r\n	<li>\r\n		内置多种在线交流随屏幕滚动样式；\r\n	</li>\r\n	<li>\r\n		独创的产品图片及图片模块图片多种样式显示模式，支持同一产品添加无限张图片；\r\n	</li>\r\n	<li>\r\n		翻页样式、每页信息显示数、时间格式、热门信息点击次数、最新信息发布天数等完全实现后台自定义；\r\n	</li>\r\n	<li>\r\n		社会化分享收藏按钮，分为侧栏式和工具式，且能够自定义代码；\r\n	</li>\r\n	<li>\r\n		DIV+CSS布局，全部兼容IE6、IE7、IE8、火狐、谷歌、TT、360度、遨游等主流浏览器；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	网站内容\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		简介、文章、产品、下载、图片模块三级栏目添加功能，栏目显示方式及显示与否完全有管理员通过后台控制；\r\n	</li>\r\n	<li>\r\n		后台管理员管理权限互相独立，可以为每一个管理员设置不同的内容管理权限，创始人无法删除且拥有最高级权限；\r\n	</li>\r\n	<li>\r\n		产品、图片、下载字段自定义功能（包括简短、下拉、文本、多选、单选、图片字段），每个产品可以上传多张产品图片；\r\n	</li>\r\n	<li>\r\n		后台信息管理支持批量删除、批量转移、批量复制、批量权限设置操作；\r\n	</li>\r\n	<li>\r\n		支持多种信息列表排序方式；\r\n	</li>\r\n	<li>\r\n		支持信息置顶、推荐等设置；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	SEO优化\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		可为网站设置全局SEO参数，也可以单独设置所有页面的页面title，关键词、页面描述，静态页面名称等；\r\n	</li>\r\n	<li>\r\n		全站静态页面生成功能，可自动或手动一键生成全站静态页面，并可以个性化静态页面名称及静态页面格式，支持中文静态页面名称；\r\n	</li>\r\n	<li>\r\n		全站伪静态功能、全站静态打包功能（打包静态页面文件程序完全分离，可将打包文件上传至任何空间正常访问）；\r\n	</li>\r\n	<li>\r\n		页面标题已经按照[信息标题-栏目名称-网站Title]方式进行全面优化而且可以自由调整页面标题构成方式，所有Title内容默认均由系统自动生成，并支持自定义页面title；\r\n	</li>\r\n	<li>\r\n		内置热门标签功能，可有效增加网站内部链接和关键词链接，可以自定义替换次数；\r\n	</li>\r\n	<li>\r\n		官方模板通过DIV+CSS构架对标题H1、图片ALT、超链接Title、页头页尾关键词、首页信息更新排版、相关信息内链等全方位SEO优化；\r\n	</li>\r\n	<li>\r\n		内置网站地图模块，可以生成最新的Html，xml，txt网站地图在网站根目录，让搜索引擎更快的收录你网站的所有页面；\r\n	</li>\r\n	<li>\r\n		内置友情链接模块，可以轻松完成与其他网站的相互连接；\r\n	</li>\r\n	<li>\r\n		支持栏目文件夹自定义和栏目内容页面静态页面名称及格式自定义，可以有效区分竞争对手同质化信息，增强SEO的效果；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	互动营销\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		内置完整的在线交流后台管理功能、可添加QQ、MSN、淘宝旺旺、阿里旺旺、SKYPE、第三方客服交流软件等，并可以设置显示风格和图标；\r\n	</li>\r\n	<li>\r\n		内置强大的在线反馈系统，可添加任意数量的反馈系统，表单字段完全自定义，可以用于产品订购、在线报名、在线调查、意见反馈等，可以自动将反馈信息发送至管理员设置的指定邮箱（可设置多个邮箱），并可以分类导出EXCEL表格形式的反馈信息；\r\n	</li>\r\n	<li>\r\n		在线留言后台管理功能及邮件发送功能，管理员可以通过后台审核和回复留言内容；\r\n	</li>\r\n	<li>\r\n		招聘信息发布和应聘者简历在线提交功能，简历自动发送到指定邮箱功能；\r\n	</li>\r\n	<li>\r\n		后台内置站长统计功能，可以轻松监控网站浏览及来访者详情、搜索引擎来访和搜索的关键词等信息；\r\n	</li>\r\n	<li>\r\n		后台内置短信群发功能，支持自定义发送或获取所有会员手机号码群发短信。\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	会员与权限管理\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		系统内置游客、普通会员、代理商、系统管理员四种权限角色，并可以实现普通会员、代理商前台注册与后台管理；\r\n	</li>\r\n	<li>\r\n		语言内容、全站栏目、信息页面、字段参数均可以进行权限控制，进而可以灵活的设置代理价格、资料下载、最新产品公布等等的权限管理；\r\n	</li>\r\n	<li>\r\n		可直接根据后台栏目设置管理员权限及操作权限功能，从而可以实现网站栏目及内容多人独立管理，且可以使管理员操作界面简洁明了；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	安全与效率\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		内置数据库备份和恢复功能，可一键轻松完成整站数据库备份与恢复，支持配置文件和整站后台压缩打包下载；\r\n	</li>\r\n	<li>\r\n		后台管理文件夹名称后台更改功能，可以让你轻松的隐藏管理登陆路径，从而提高系统安全性能；\r\n	</li>\r\n	<li>\r\n		验证码、防刷新机制、SQL危险符号和语句过滤机制等大大增强了系统防恶意攻击的能力；\r\n	</li>\r\n	<li>\r\n		管理员密码邮箱找回功能、会员密码邮箱找回功能增强了系统的自助安全性能；\r\n	</li>\r\n	<li>\r\n		公共信息读取效率控制参数、模板调用数据库控制文件（由模板制作者控制）大大提高了系统的运行效率；\r\n	</li>\r\n	<li>\r\n		上传文件夹及可视化文件管理功能，可以轻松管理删除垃圾文件；\r\n	</li>\r\n	<li>\r\n		PHP+MYSQL构架、全站静态功能、系统文件权限设置等大大增强了系统抗击木马和恶意攻击的能力；\r\n	</li>\r\n	<li>\r\n		后台内置网站体检功能，可以发现网站存在的细节问题，并保护网站安全；\r\n	</li>\r\n</ol>\r\n<br />\r\n<div style=\"text-align:start;\">\r\n	官方网站：http://www.x-mai.com\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	系统开发：新麦网络信息技术服务部\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	发布日期：2013年12月3日\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	<p>\r\n		联系电话：15358062815\r\n	</p>\r\n	<p>\r\n		<br />\r\n	</p>\r\n	<p>\r\n		<br />\r\n	</p>\r\n</div>', '2013-11-21', '0', '16', 'zh_cn', null, '/attached/file/20131202/20131202012559_79002.txt', '/attached/file/20131202/20131202012559_79002.txt');

-- ----------------------------
-- Table structure for `xm_employee`
-- ----------------------------
DROP TABLE IF EXISTS `xm_employee`;
CREATE TABLE `xm_employee` (
  `employ_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `place` varchar(255) default NULL,
  `pay` varchar(255) default NULL,
  `employnum` int(11) default NULL,
  `createtime` date default NULL,
  `lang` varchar(50) default 'zh_cn',
  `orderby` int(11) NOT NULL default '0',
  `module_id` int(11) NOT NULL,
  `description` text,
  `sender_id` int(11) default NULL,
  PRIMARY KEY  (`employ_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_employee
-- ----------------------------
INSERT INTO xm_employee VALUES ('3', 'PHP技术支持', '无锡', '面议', '1', '2013-11-24', 'zh_cn', '0', '34', '<p style=\"text-align:start;\">\r\n	主要工作内容：\r\n</p>\r\n<div style=\"text-align:start;\">\r\n	1. 负责XmCMS企业网站管理系统技术支持；\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	2. 为客服人员提供技术支持；\r\n</div>\r\n<div style=\"text-align:start;\">\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	岗位要求：\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		php能看懂且对PHP感兴趣，理解面向对象基本概念，写过一些小程序；\r\n	</li>\r\n	<li>\r\n		懂MySql数据库备份、恢复等基本操作，熟悉PHP环境的搭建和配置；\r\n	</li>\r\n	<li>\r\n		javascript能看懂，了解jquery等js框架；\r\n	</li>\r\n	<li>\r\n		html/css会写，懂linux的优先；\r\n	</li>\r\n	<li>\r\n		擅长网上查找资料解决问题；\r\n	</li>\r\n	<li>\r\n		有PHP作品（留言板，blog等）的优先；\r\n	</li>\r\n	<li>\r\n		做事要有耐心，性格谦和，学习能力强，能吃苦耐劳，愿意同公司共同发展。\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	如果你对我们的职位感兴趣，且符合我们的基本要求，请将个人简历投递至458820281@qq.com，或者直接与我们取得联系！\r\n</div>', null);
INSERT INTO xm_employee VALUES ('6', '网页UI设计师', '无锡', '面议', '1', '2013-12-03', 'zh_cn', '0', '34', '<p style=\"text-align:start;\">\r\n	主要工作内容：负责XmCMS界面和公司网站的界面设计等，重视用户体验。\r\n</p>\r\n<div style=\"text-align:start;\">\r\n	岗位要求：\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		视觉设计、平面设计或美术相关专业，大专以上学历。\r\n	</li>\r\n	<li>\r\n		具有良好的创意设计能力及良好的色彩感，有较高的美术功底，较强的网页设计能力和整体布局感。\r\n	</li>\r\n	<li>\r\n		精通photoshop、Illustrator、Fireworks、Dreamweaver等图形设计工具中至少两种。\r\n	</li>\r\n	<li>\r\n		了解网页交互设计知识，对作品有不断追求完美的精神特质。\r\n	</li>\r\n	<li>\r\n		有网站UI设计同等职位工作经验、能提供过往作品者优先。\r\n	</li>\r\n</ol>', null);
INSERT INTO xm_employee VALUES ('7', 'Web前端开发人员', '无锡', '面议', '1', '2013-12-03', 'zh_cn', '0', '34', '<div style=\"text-align:start;\">\r\n	主要工作内容：负责XmCMS企业网站管理系统前台模板功能设计、用户体验提升和Web界面技术优化。\r\n</div>\r\n<div style=\"text-align:start;\">\r\n	岗位要求：\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		熟悉JavaScript、Ajax、JQuery等前端开发脚本技能；\r\n	</li>\r\n	<li>\r\n		精通HTML/XHTML、CSS，能够快速手写CSS代码，熟悉页面架构和布局。\r\n	</li>\r\n	<li>\r\n		熟悉Web网站以及Web应用的可用性及用户分析方法，深入理解产品流程、用户体验和用户需求，对提升用户体验有一定经验及造诣。\r\n	</li>\r\n	<li>\r\n		对Web技术发展有强烈兴趣，有良好的学习能力和强烈的进取心，愿意同公司共同发展。\r\n	</li>\r\n	<li>\r\n		能跟踪最新的WEB前端设计、懂PHP基本语言者优先。\r\n	</li>\r\n</ol>', null);
INSERT INTO xm_employee VALUES ('5', '网络销售', '无锡', '面议', '2', '2013-12-03', 'zh_cn', '0', '34', '<ol style=\"text-align:start;\">\r\n	<li>\r\n		大专以上学历，一年以上网络销售经验；\r\n	</li>\r\n	<li>\r\n		熟悉网络推广，熟悉网站建设基本流程；\r\n	</li>\r\n	<li>\r\n		有网站制作相关工作经验者优先；\r\n	</li>\r\n	<li>\r\n		学习能力强，能吃苦耐劳，愿意同公司共同发展；\r\n	</li>\r\n	<li>\r\n		本岗位招收兼职，投递简历时请说明自己的工作意愿；\r\n	</li>\r\n</ol>\r\n<div style=\"text-align:start;\">\r\n	如果你对我们的职位感兴趣，且符合我们的基本要求，请将个人简历投递至458820281@qq.com，或者直接与我们取得联系！\r\n</div>', null);
INSERT INTO xm_employee VALUES ('8', '电子商务专员', '无锡', '面议', '1', '2013-12-03', 'zh_cn', '0', '34', '<p style=\"text-align:start;\">\r\n	主要工作内容：负责XmCMS企业网站管理系统在线销售工作，向客户提供迅速、准确、周到的服务。\r\n</p>\r\n<div style=\"text-align:start;\">\r\n	岗位要求：\r\n</div>\r\n<ol style=\"text-align:start;\">\r\n	<li>\r\n		熟悉互联网销售与推广；\r\n	</li>\r\n	<li>\r\n		能熟练操作网站管理系统后台，了解基本的建站知识；\r\n	</li>\r\n	<li>\r\n		能敏锐把握网站建设行业市场动向，善于把握客户心理；\r\n	</li>\r\n	<li>\r\n		沟通能力强，勤奋耐劳，办事认真严谨细致；\r\n	</li>\r\n	<li>\r\n		具有较强的学习能力，愿意同公司共同发展；\r\n	</li>\r\n	<li>\r\n		有网站建设相关产品工作经验者优先，电子商务、市场营销等相关专业优先。\r\n	</li>\r\n</ol>', null);

-- ----------------------------
-- Table structure for `xm_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `xm_feedback`;
CREATE TABLE `xm_feedback` (
  `feedback_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `title` varchar(255) default NULL,
  `user` varchar(50) default NULL,
  `description` text,
  `haveread` tinyint(4) default '0',
  `createtime` date default NULL,
  `lang` varchar(50) default 'zh_cn',
  `module_id` int(11) default NULL,
  `feedbacktype_id` int(11) default NULL,
  `email` varchar(255) default NULL,
  PRIMARY KEY  (`feedback_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_feedback
-- ----------------------------
INSERT INTO xm_feedback VALUES ('1', '7', '111', '1', '11', '1', '2013-11-24', 'zh_cn', '22', '1', null);
INSERT INTO xm_feedback VALUES ('2', '7', 'sdf', 'sdf', 'sdfsdf', '1', '2013-12-04', 'zh_cn', '22', '1', 'sdfsdf@3sdf.com');

-- ----------------------------
-- Table structure for `xm_feedbacktype`
-- ----------------------------
DROP TABLE IF EXISTS `xm_feedbacktype`;
CREATE TABLE `xm_feedbacktype` (
  `feedbacktype_id` int(11) NOT NULL auto_increment,
  `feedbacktype` varchar(255) default NULL,
  `enname` varchar(255) NOT NULL default 'zh_cn',
  PRIMARY KEY  (`feedbacktype_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_feedbacktype
-- ----------------------------
INSERT INTO xm_feedbacktype VALUES ('1', '索取资料', '索取资料');
INSERT INTO xm_feedbacktype VALUES ('2', '产品购买', 'en1');
INSERT INTO xm_feedbacktype VALUES ('3', '商务合作', 'en2');
INSERT INTO xm_feedbacktype VALUES ('4', '其他反馈', '33');

-- ----------------------------
-- Table structure for `xm_firendlink`
-- ----------------------------
DROP TABLE IF EXISTS `xm_firendlink`;
CREATE TABLE `xm_firendlink` (
  `friendlink_id` int(11) NOT NULL auto_increment,
  `webname` varchar(255) default NULL,
  `weburl` varchar(255) default NULL,
  `logourl` varchar(255) default NULL,
  `keyword` varchar(255) default NULL,
  `linktype` tinyint(4) default '1' COMMENT '1,文字，2.图片',
  `seq` int(11) default NULL,
  `isauth` tinyint(4) default '0' COMMENT '是否审核，0，未审核，1.审核',
  `haveread` tinyint(4) default '0',
  PRIMARY KEY  (`friendlink_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_firendlink
-- ----------------------------
INSERT INTO xm_firendlink VALUES ('1', '11', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO xm_firendlink VALUES ('2', '222', 'wwsdfsdf', '1', '1', '2', '2', '1', '1');

-- ----------------------------
-- Table structure for `xm_flash`
-- ----------------------------
DROP TABLE IF EXISTS `xm_flash`;
CREATE TABLE `xm_flash` (
  `flash_id` int(11) NOT NULL auto_increment,
  `flashtype` varchar(255) default NULL COMMENT '是flash,还是图片',
  `width` int(11) default NULL,
  `height` int(11) default NULL,
  `title` varchar(255) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `name` varchar(50) default NULL,
  PRIMARY KEY  (`flash_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_flash
-- ----------------------------
INSERT INTO xm_flash VALUES ('2', 'images', '800', '360', '首页图片轮换', 'zh_cn', 'index_slider');
INSERT INTO xm_flash VALUES ('3', 'images', '800', '360', '首页图片轮换', 'en_us', 'index_slider');
INSERT INTO xm_flash VALUES ('4', 'images', '800', '180', '通栏图片', 'zh_cn', 'item_bar');
INSERT INTO xm_flash VALUES ('5', 'images', '800', '180', '通栏图片', 'en_us', 'item_bar');

-- ----------------------------
-- Table structure for `xm_flashimages`
-- ----------------------------
DROP TABLE IF EXISTS `xm_flashimages`;
CREATE TABLE `xm_flashimages` (
  `flashimage_id` int(11) NOT NULL auto_increment,
  `flash_id` int(11) default NULL,
  `imageurl` varchar(255) default NULL,
  `imagedesc` varchar(255) default NULL,
  `seq` int(11) default NULL,
  `title` varchar(255) default NULL,
  `link` varchar(255) default NULL,
  PRIMARY KEY  (`flashimage_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_flashimages
-- ----------------------------
INSERT INTO xm_flashimages VALUES ('2', '2', '/attached/image/20131127/20131127103815_76618.png', '图片描述1', '1', '图片1', 'd');
INSERT INTO xm_flashimages VALUES ('3', '2', '/attached/image/20131127/20131127104222_48822.png', '图片2描述', '2', '图片2', null);
INSERT INTO xm_flashimages VALUES ('4', '2', '/attached/image/20131127/20131127104245_32549.png', '图片2描述', '3', '图片3', null);
INSERT INTO xm_flashimages VALUES ('5', '4', '/attached/image/20131203/20131203143636_25433.png', '通栏图片', '0', '通栏图片', null);
INSERT INTO xm_flashimages VALUES ('6', '3', '/attached/image/20131127/20131127103815_76618.png', '图片描述1', '1', '图片1', null);
INSERT INTO xm_flashimages VALUES ('7', '3', '/attached/image/20131127/20131127104222_48822.png', '图片2描述', '2', '图片2', null);
INSERT INTO xm_flashimages VALUES ('8', '3', '/attached/image/20131127/20131127104245_32549.png', '图片2描述', '3', '图片3', null);
INSERT INTO xm_flashimages VALUES ('9', '5', '/attached/image/20131203/20131203143636_25433.png', '通栏图片', '0', '通栏图片', null);

-- ----------------------------
-- Table structure for `xm_fragment`
-- ----------------------------
DROP TABLE IF EXISTS `xm_fragment`;
CREATE TABLE `xm_fragment` (
  `id` int(11) NOT NULL auto_increment,
  `key` varchar(255) default NULL,
  `content` text,
  `description` varchar(255) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_fragment
-- ----------------------------
INSERT INTO xm_fragment VALUES ('5', 'case_contact', '<p>\r\n	<br />\r\n<strong>更多了解：</strong><br />\r\n如果您对上述解决方案感兴趣，可以通过点击下面地址进行在线交流，在这里我们的销售人员会尽快与您联系，为您提供更加详细的咨询服务。<br />\r\n<br />\r\n<strong>马上咨询：</strong><br />\r\nQQ：458820281@qq.com<br />\r\n邮箱：458820281@qq.com<br />\r\n电话：15358062815<br />\r\n联系人：朱君\r\n</p>', '案例页面下的通用联系信息', 'zh_cn');

-- ----------------------------
-- Table structure for `xm_group`
-- ----------------------------
DROP TABLE IF EXISTS `xm_group`;
CREATE TABLE `xm_group` (
  `group_id` int(11) NOT NULL auto_increment,
  `groupname` varchar(255) default NULL,
  `enname` varchar(255) default NULL,
  PRIMARY KEY  (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_group
-- ----------------------------
INSERT INTO xm_group VALUES ('1', '普通用户', 'Common Users');
INSERT INTO xm_group VALUES ('3', '代理商', 'Agent');

-- ----------------------------
-- Table structure for `xm_guide`
-- ----------------------------
DROP TABLE IF EXISTS `xm_guide`;
CREATE TABLE `xm_guide` (
  `module_id` int(11) default NULL,
  `description` text,
  `lang` varchar(50) NOT NULL default 'zh_cn'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_guide
-- ----------------------------
INSERT INTO xm_guide VALUES ('12', '新麦网络信息技术服务部位于江苏无锡惠山区长安镇，是在高新技术应用领域中专业从事应用系统开发、信息安全服务的高新技术企业。专注于企业网站、企业信息化信息化领域，有着多年在应用系统开发以及信息安全服务的经验。<br />\r\n<br />\r\n新麦信息倡导“专业、务实、高效、创新”的企业精神，具有良好的内部机制。优良的工作环境以及良好的激励机制，吸引了一批年轻的、有学识的、具有实干精神的人才。高素质、高水平、高效率的人才是新麦信息在当今激烈的市场中立于不败之地的保障。<br />\r\n<br />\r\n我们可以提供：<br />\r\n<br />\r\n——安全、完整的系统策划和设计：<br />\r\n公司在产品研发方面的高投入，众多项目的实际应用，让我们具备了相应的的创造力和丰富的经验，这将成为您解决疑问和难题的良好保证。可为您提供多方面完善的策划：如项目的规划立项、总体方案设计、方案评估论证。<br />\r\n<br />\r\n——先进、专业的技术支持：<br />\r\n公司众多一流人才的深层磨合，对最新技术执拗的探讨精神，使我们能够保证为你提供最专业的应用，最专业的服务。<br />\r\n<br />\r\n——完善、快速的售后服务：<br />\r\n以最快的速度、最有效的方法、最先进的技术保障系统的效果发挥到极至，解除您的后顾之忧。<br />\r\n<br />\r\n我们可以解决您：<br />\r\n系统集成、应用系统开发<br />\r\n与国内外知名IT厂商建立紧密的合作关系，与业内知名的IT服务供应商建立的战略伙伴合作关系可以让您时刻把握前沿的技术脉搏和行业动态，这一切，新麦信息愿与您共享。新麦信息真诚希望能够有机会参与您的企业信息化建设，解决您在信息建设过程中出现的各类的问题，为您信息化建设做出我们的贡献。<br />\r\n<br />\r\n企业文化<br />\r\n1 客户至上<br />\r\n以客户业务为中心，以客户实际业务需求为导向，从客户利益出发，为客户提供完善的信息化应用解决方案。<br />\r\n<br />\r\n2 尊重个人<br />\r\n作为新麦的员工，不论职位的高低、分工的不同，都将受到充分的尊重和平等的对待。公司给予员工同等的机会，鼓励员工与公司共同发展，为员工制定与公司发展步调一致的个人年度事业发展计划。<br />\r\n<br />\r\n3 鼓励创新<br />\r\n系统开发是一项智力型的事业，公司提供富于挑战性的事业发展空间，鼓励每一位员工创造性地工作，充分发挥个人的最大价值，从而创造出最佳的工作效果。<br />\r\n<br />\r\n4 团队合作<br />\r\n在工作中，公司强调相互尊重、理解和有效的沟通，从而在平等和谐的合作氛围中创造“1 + 1&gt;2”的工作效应。', 'zh_cn');
INSERT INTO xm_guide VALUES ('20', '<p>\r\n	新麦网络信息技术服务部专注于企业信息化及企业信息应用系统领域，通过信息化技术为客户解决网络营销、企业信息管理。\r\n</p>\r\n<p>\r\n	主要业务包括：网站系统开发、网站建设、网站推广、企业管理系统开发、企业管理系统数据优化。\r\n</p>\r\n<p>\r\n	<strong>主打产品</strong> \r\n</p>\r\n<p>\r\n	新麦企业信息管理系统，采用PHP+Mysql架构，支持用户自定义界面语言，拥有企业网站常用的模块功能（企业简介模块、新闻模块、产品模块、下载模块、图片模块、招聘模块、在线留言、反馈系统、在线交流、友情链接、会员与权限管理）。强大灵活的后台管理功能、强大的数据缓存功能、个性化模块添加功能、不同栏目自定义FLASH样式功能等可为企业打造出大气漂亮且具有营销力的精品网站。\r\n</p>\r\n<p>\r\n	新麦网络信息技术服务部秉承“为合作伙伴创造价值”的核心价值观，并以“诚实、宽容、创新、服务”为企业精神，通过自主创新和真诚合作为企业创造价值。\r\n</p>', 'zh_cn');
INSERT INTO xm_guide VALUES ('23', '新麦网络信息技术服务部<br />\r\n电&nbsp; 话：15358062815<br />\r\n邮&nbsp; 编：214400 <br />\r\n地址：惠山区长安镇水厂路100号 无锡, 中国 <br />\r\nEmail：458820281@qq.com<br />\r\n网&nbsp; 址：www.x-mai.com<br />\r\n<img src=\"/attached/image/20131129/20131129023113_86445.png\" alt=\"\" />', 'zh_cn');
INSERT INTO xm_guide VALUES ('44', '', 'zh_cn');
INSERT INTO xm_guide VALUES ('45', '', 'zh_cn');

-- ----------------------------
-- Table structure for `xm_images`
-- ----------------------------
DROP TABLE IF EXISTS `xm_images`;
CREATE TABLE `xm_images` (
  `image_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `image` varchar(255) default NULL,
  `description` text,
  `createtime` date default NULL,
  `sender_id` int(11) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `module_id` int(11) default NULL,
  `orderby` int(11) NOT NULL default '0',
  PRIMARY KEY  (`image_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_images
-- ----------------------------
INSERT INTO xm_images VALUES ('5', '案例一', '/attached/image/20131203/20131203140832_81389.jpg', '案例一', '2013-11-27', '7', 'zh_cn', '17', '0');
INSERT INTO xm_images VALUES ('6', '案例二', '/attached/image/20131203/20131203141030_59938.jpg', '案例二', '2013-12-03', '2', 'zh_cn', '17', '0');
INSERT INTO xm_images VALUES ('7', '案例三', '/attached/image/20131203/20131203141431_28580.jpg', '案例三', '2013-12-03', '2', 'zh_cn', '17', '0');
INSERT INTO xm_images VALUES ('8', '案例四', '/attached/image/20131203/20131203141500_74075.jpg', '案例四', '2013-12-03', '2', 'zh_cn', '17', '0');
INSERT INTO xm_images VALUES ('9', '案例五', '/attached/image/20131203/20131203141522_37163.jpg', '案例五', '2013-12-03', '2', 'zh_cn', '17', '0');

-- ----------------------------
-- Table structure for `xm_message`
-- ----------------------------
DROP TABLE IF EXISTS `xm_message`;
CREATE TABLE `xm_message` (
  `message_id` int(11) NOT NULL auto_increment,
  `description` text,
  `reply` text,
  `replyuser_id` int(11) default NULL,
  `createtime` date default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `user` varchar(50) NOT NULL,
  `isAuth` tinyint(4) NOT NULL default '0',
  `haveread` tinyint(4) default '0',
  `module_id` int(11) default NULL,
  `othercontact` varchar(255) default NULL,
  `user_id` int(11) default NULL,
  `email` varchar(255) default NULL,
  `phone` varchar(50) default NULL,
  `replytime` date default NULL,
  PRIMARY KEY  (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_message
-- ----------------------------
INSERT INTO xm_message VALUES ('4', '访问网站出现MySQL Server Error错误怎么办？', '出现这样的报错不用过于紧张，空间和程序都没有问题，这是由于数据库无法连接造成的。', '2', '2013-12-04', 'zh_cn', '张三', '1', '1', '15', '', '7', '458820281@qq.com', '15358062815', '2013-12-04');

-- ----------------------------
-- Table structure for `xm_module`
-- ----------------------------
DROP TABLE IF EXISTS `xm_module`;
CREATE TABLE `xm_module` (
  `module_id` int(11) NOT NULL auto_increment,
  `category` varchar(255) default NULL,
  `parentid` int(11) NOT NULL default '-1',
  `seq` int(11) default '0',
  `target` varchar(50) NOT NULL default '_self',
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `module` varchar(50) default NULL,
  `url` varchar(255) default NULL,
  `name` varchar(255) default 'int',
  `ishid` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`module_id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_module
-- ----------------------------
INSERT INTO xm_module VALUES ('12', '关于我们', '-1', '0', '_self', 'zh_cn', 'guide', '', '12::', '0');
INSERT INTO xm_module VALUES ('13', '新闻资讯', '-1', '0', '_self', 'zh_cn', 'article', '', '13::', '0');
INSERT INTO xm_module VALUES ('14', '产品资讯', '-1', '0', '_self', 'zh_cn', 'product', '', '14::', '0');
INSERT INTO xm_module VALUES ('15', '在线留言', '-1', '0', '_self', 'zh_cn', 'message', '', '15::', '0');
INSERT INTO xm_module VALUES ('16', '下载中心', '-1', '0', '_self', 'zh_cn', 'download', '', '16::', '0');
INSERT INTO xm_module VALUES ('17', '案例展示', '-1', '0', '_self', 'zh_cn', 'image', '', '17::', '0');
INSERT INTO xm_module VALUES ('18', '招贤纳士', '-1', '0', '_self', 'zh_cn', 'employee', '', '18::', '0');
INSERT INTO xm_module VALUES ('20', '公司简介', '12', '0', '_self', 'zh_cn', 'guide', null, '12::20::', '0');
INSERT INTO xm_module VALUES ('22', '在线反馈', '12', '0', '_self', 'zh_cn', 'feedback', null, '12::22::', '0');
INSERT INTO xm_module VALUES ('23', '联系我们', '12', '0', '_self', 'zh_cn', 'guide', null, '12::23::', '0');
INSERT INTO xm_module VALUES ('25', '公司动态', '13', '0', '_self', 'zh_cn', 'article', null, '13::25::', '0');
INSERT INTO xm_module VALUES ('26', '业界资讯', '13', '0', '_self', 'zh_cn', 'article', null, '13::26::', '0');
INSERT INTO xm_module VALUES ('27', '饰品珠宝', '14', '2', '_self', 'zh_cn', 'product', '', '14::27::', '0');
INSERT INTO xm_module VALUES ('28', '数码家电', '25', '0', '_self', 'zh_cn', 'product', '', '13::25::28::', '0');
INSERT INTO xm_module VALUES ('30', '软件下载', '16', '0', '_self', 'zh_cn', 'download', '', '16::30::', '0');
INSERT INTO xm_module VALUES ('31', '文件下载', '16', '0', '_self', 'zh_cn', 'download', '', '16::31::', '0');
INSERT INTO xm_module VALUES ('34', '人才招聘', '18', '0', '_self', 'zh_cn', 'employee', '', '18::34::', '0');
INSERT INTO xm_module VALUES ('37', 'About Us', '-1', '0', '_self', 'en_us', 'guide', '', '37::', '0');
INSERT INTO xm_module VALUES ('38', 'Company Information', '37', '0', '_self', 'en_us', 'guide', '', '37::38::', '0');
INSERT INTO xm_module VALUES ('39', 'Feedback', '37', '0', '_self', 'en_us', 'feedback', '', '37::39::', '0');
INSERT INTO xm_module VALUES ('40', 'Contact Us', '37', '0', '_self', 'en_us', 'guide', '', '37::40::', '0');
INSERT INTO xm_module VALUES ('44', '宝石', '27', '0', '_self', 'zh_cn', 'guide', '', '14::27::44::', '0');
INSERT INTO xm_module VALUES ('45', '交通工具', '14', '1', '_self', 'zh_cn', 'guide', '', '14::45::', '0');
INSERT INTO xm_module VALUES ('46', 'Message', '37', '0', '_self', 'en_us', 'message', '', '37::46::', '0');

-- ----------------------------
-- Table structure for `xm_online`
-- ----------------------------
DROP TABLE IF EXISTS `xm_online`;
CREATE TABLE `xm_online` (
  `online_id` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `seq` int(11) default NULL,
  `qq` varchar(255) default NULL,
  `taobaowangwang` varchar(255) default NULL,
  `msn` varchar(255) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  PRIMARY KEY  (`online_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_online
-- ----------------------------
INSERT INTO xm_online VALUES ('1', '售前', '1', '458820281', '0', '', 'zh_cn');
INSERT INTO xm_online VALUES ('2', '售后', '2', '458820281', null, null, 'zh_cn');

-- ----------------------------
-- Table structure for `xm_other`
-- ----------------------------
DROP TABLE IF EXISTS `xm_other`;
CREATE TABLE `xm_other` (
  `other_id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `description` text,
  `createtime` datetime default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `enname` varchar(50) default NULL,
  PRIMARY KEY  (`other_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_other
-- ----------------------------
INSERT INTO xm_other VALUES ('1', '首页简介内容', '<p>\r\n	新麦网络信息技术服务部专注于企业信息化及企业信息应用系统领域，通过信息化技术为客户解决网络营销、企业信息管理。\r\n</p>\r\n<p>\r\n	主要业务包括：网站系统开发、网站建设、网站推广、企业管理系统开发、企业管理系统数据优化。\r\n</p>\r\n<p>\r\n	主要产品：新麦企业信息管理系统、新麦客户关系管理系统。\r\n</p>\r\n<p>\r\n	新麦网络信息技术服务部秉承“为合作伙伴创造价值”的核心价值观，并以“诚实、宽容、创新、服务”为企业精神，通过自主创新和真诚合作为企业创造价值。\r\n</p>', '2013-11-27 13:30:58', 'zh_cn', 'index_summary');
INSERT INTO xm_other VALUES ('2', '联系方式内容', '新麦网络信息技术服务部<br />\r\n电&nbsp; 话：15358062815<br />\r\n邮&nbsp; 编：214400<br />\r\nEmail：458820281@qq.com<br />\r\n网&nbsp; 址：www.x-mai.com', '2013-11-27 13:31:03', 'zh_cn', 'contact_content');
INSERT INTO xm_other VALUES ('3', '首页简介内容', 'fff', '2013-11-27 13:31:01', 'en_us', 'index_summary');
INSERT INTO xm_other VALUES ('4', '联系方式内容', 'dd', '2013-11-27 13:31:05', 'en_us', 'contact_content');

-- ----------------------------
-- Table structure for `xm_product`
-- ----------------------------
DROP TABLE IF EXISTS `xm_product`;
CREATE TABLE `xm_product` (
  `product_id` int(11) NOT NULL auto_increment,
  `productname` varchar(255) default NULL,
  `productimage` varchar(255) default NULL,
  `productdesc` varchar(255) default NULL,
  `createtime` date default NULL,
  `module_id` int(11) default NULL,
  `lang` varchar(50) NOT NULL default 'zh_cn',
  `orderby` int(11) default NULL,
  `sender_id` int(11) default NULL,
  PRIMARY KEY  (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_product
-- ----------------------------
INSERT INTO xm_product VALUES ('1', '新麦企业信息管理系统（XmCMS）', '/attached/image/20131123/20131123013402_21340.gif', '<p>\r\n	<strong>便于网络营销的功能设计</strong> \r\n</p>\r\n<p>\r\n	新麦企业CMS拥有全面的SEO优化机制。内置访问统计，可以随时了解网站访问情况。还有在线反馈、在线交流、邮件功能等互动营销功能。网站需要有营销价值，否则只是个“摆设”。\r\n</p>\r\n<p>\r\n	<strong>轻松打造多语言国际网站</strong> \r\n</p>\r\n<p>\r\n	可轻松添加简体中文、繁体中文、英文、日文、韩文或自定义的其他任意网站语言，可以实现不同域名访问不同的网站语言。\r\n</p>', '2013-11-20', '14', 'zh_cn', '1', '2');

-- ----------------------------
-- Table structure for `xm_tag`
-- ----------------------------
DROP TABLE IF EXISTS `xm_tag`;
CREATE TABLE `xm_tag` (
  `tag_id` int(11) NOT NULL auto_increment,
  `tag` varchar(255) default NULL,
  `type` varchar(255) default NULL,
  `relid` int(11) default NULL,
  `lang` varchar(255) NOT NULL default 'zh_cn',
  PRIMARY KEY  (`tag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_tag
-- ----------------------------
INSERT INTO xm_tag VALUES ('114', 'XmCMS', 'product', '7', 'zh_cn');
INSERT INTO xm_tag VALUES ('115', '新麦CMS', 'product', '7', 'zh_cn');
INSERT INTO xm_tag VALUES ('165', 'seo', 'article', '36', 'zh_cn');
INSERT INTO xm_tag VALUES ('168', '', 'product', '1', 'zh_cn');

-- ----------------------------
-- Table structure for `xm_user`
-- ----------------------------
DROP TABLE IF EXISTS `xm_user`;
CREATE TABLE `xm_user` (
  `user_id` int(11) NOT NULL auto_increment,
  `loginname` varchar(255) default NULL,
  `loginpass` varchar(255) default NULL COMMENT '默认密码：21232f297a57a5a743894a0e4a801fc3，admin',
  `username` varchar(255) default NULL,
  `createtime` datetime NOT NULL,
  `group_id` int(11) default NULL,
  `isvalid` tinyint(4) default '1',
  `isadmin` tinyint(4) default '0',
  `last_loginip` varchar(50) default NULL,
  `last_logintime` datetime default NULL,
  `login_num` int(11) NOT NULL default '0',
  `email` varchar(255) default NULL,
  `phone` varchar(255) default NULL,
  `isnew` tinyint(4) default '0',
  `sex` int(11) default '0' COMMENT '0女,1男',
  `tel` varchar(50) default NULL,
  `admingroup_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xm_user
-- ----------------------------
INSERT INTO xm_user VALUES ('2', 'administrator', '21232f297a57a5a743894a0e4a801fc3', '管理员', '2013-10-28 20:02:22', null, '1', '1', '127.0.0.1', '2014-02-23 18:03:52', '137', '458820281@qq.com', '15358062815', '0', '1', '15358062815', '-1');
INSERT INTO xm_user VALUES ('7', 'zhangsan', 'd41d8cd98f00b204e9800998ecf8427e', '张三', '2013-11-26 14:01:22', null, '1', '1', '127.0.0.1', '2013-12-05 15:12:43', '40', '458820281@qq.com', '15358062815', '0', '1', '15358062815', '1');
