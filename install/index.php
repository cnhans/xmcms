<?php
if(file_exists("../config/install.lock"))
{
    header("location:/");
}
session_start();
$action = $_REQUEST['action'] == null?1:$_REQUEST['action'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>新麦企业网站管理系统安装</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <script type="text/javascript" src="js/jquery.js"></script>
</head>
<body>
    <div id="wrap">
        <div class="fixed" id="header" style="border-bottom: 1px solid #EFEFEF;height:85px;">
            <div class="fixed" id="logo-header-widget-1"><a href="http://www.x-mai.com" id="logo"> <img alt="logo" src="images/logo.png"> </a>
            </div>
        </div>
        <div class="clr"></div>
        <div class="step">
            <ul>
                <li><span <?php echo $action==1?'class="active"':$action; ?> >1</span>阅读使用协议</li>
                <li><span <?php echo $action==2?'class="active"':$action; ?> >2</span>系统环境检测</li>
                <li><span <?php echo $action==3?'class="active"':$action; ?> >3</span>数据库设置</li>
                <li><span <?php echo $action==4?'class="active"':$action; ?> >4</span>管理员设置</li>
                <li><span <?php echo $action==5?'class="active"':$action; ?> >5</span> 安装完成</li>
            </ul>
        </div>
        <?php
        if($action==1){
            $_SESSION['install']='xminfo';
        }
         include 'include/'.$action.'.php';
        ?>
        <div id="footer" class="tac">
            Powered by XmInfo2.0 ©20012-2015  XmInfo Inc.
        </div>
    </div>
</body>
</html>