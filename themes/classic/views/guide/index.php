<?php
require Yii::app()->theme->viewPath.'/include/doctype.php';
?>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $guide['category']; ?>&nbsp;-&nbsp;Power by XmCMS</title>
    <meta name="Description" content="<?php echo csubstr(strip_tags($guide['description']),0,255); ?>"/>
    <meta name="Keywords" content="<?php echo strip_tags($guide['category']); ?>"/>
    <?php
    require Yii::app()->theme->viewPath.'/include/htmlheader.php';
    ?>
<head>

</head>

</head>
<body>

<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath.'/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <h1><?php echo $guide['category']; ?></h1>
                <p><?php echo $guide['description']; ?></p>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>
    </div>
    <?php
    require Yii::app()->theme->viewPath.'/include/footer.php';
    ?>
</div>

</body>
</html>