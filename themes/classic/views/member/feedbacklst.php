<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
    <style>
        .info td {
            padding: 0px !important;
            border: none !important;
        }
    </style>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <ul id="tab-1" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-1-1"><?php echo Yii::t('front','feedbacklist'); ?></a></li>
                </ul>
                <div id="content-tab-1-1" class="tabs-content" style="padding: 20px;">
                    <table cellspacing="0" cellpadding="0" style="width: 100%;">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('front','feedbacktitle'); ?></th>
                            <th><?php echo Yii::t('front','submittime'); ?></th>
                            <th><?php echo Yii::t('front','oper'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach($feedbacklst as $f){
                            if($i%2==0){
                                ?>
                                <tr>
                                    <td><?php echo $f['title']; ?></td>
                                    <td><?php echo $f['createtime']; ?></td>
                                    <td style="width: 60px;"><a href="feedbacklstshow?fid=<?php echo $f['feedback_id']; ?>"><?php echo Yii::t('front','see'); ?></a></td>
                                </tr>
                            <?php
                            }else{
                                ?>
                                <tr class="alt">
                                    <td><?php echo $f['title']; ?></td>
                                    <td><?php echo $f['createtime']; ?></td>
                                    <td style="width: 60px;"><a href="feedbacklstshow?fid=<?php echo $f['feedback_id']; ?>"><?php echo Yii::t('front','see'); ?></a></td>
                                </tr>
                            <?php
                            }
                            $i++;
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php
                    echo $pb->getbar();
                    ?>
                </div>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/memberbar.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>

</body>
</html>