<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
    <style>
        .info td {
            padding: 0px !important;
            border: none !important;
        }
    </style>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <ul id="tab-1" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-1-1"><?php echo Yii::t('front','feedbackinfo'); ?></a></li>
                </ul>
                <div id="content-tab-1-1" class="tabs-content" style="padding: 20px;">
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','username'); ?> </label><?php echo $message['user']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','content'); ?> </label><br><?php echo $message['email']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','phone'); ?> </label><?php echo $message['phone']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','email'); ?> </label><?php echo $message['email']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','othercontact'); ?> </label><?php echo $message['othercontact']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','haveread'); ?> </label><?php echo $message['haveread']==0?"<?php echo Yii::t('front','no'); ?>":"<?php echo Yii::t('front','yes'); ?>"; ?></p>
                    <?php
                    if($message['reply']!=null){
                        ?>
                        <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','reply'); ?> </label><br><?php echo $message['reply']; ?></p>
                        <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','replyer'); ?> </label><?php echo $message['replyer']; ?></p>
                        <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','replytime'); ?> </label><?php echo $message['replytime']; ?></p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/memberbar.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>

</body>
</html>