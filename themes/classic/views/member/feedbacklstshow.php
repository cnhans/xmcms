<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
    <style>
        .info td {
            padding: 0px !important;
            border: none !important;
        }
    </style>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <ul id="tab-1" class="tabs-menu fixed">
                <li class="current"><a href="#content-tab-1-1"><?php echo Yii::t('front','feedbackinfo'); ?></a></li>
                </ul>
                <div id="content-tab-1-1" class="tabs-content" style="padding: 20px;">
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','feedbacktype'); ?></label><?php echo $feedback['feedbacktype']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','username'); ?> </label><?php echo $feedback['user']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','email'); ?> </label><?php echo $feedback['email']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','subject'); ?> </label><?php echo $feedback['title']; ?></p>
                    <p><label style="width: 100px;display: inline-block;" for="name"><?php echo Yii::t('front','content'); ?> </label><br><?php echo $feedback['description']; ?></p>
                </div>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/memberbar.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>

</body>
</html>