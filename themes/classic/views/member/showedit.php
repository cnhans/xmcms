<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
    <style>
        .info td{padding: 0px !important;border: none !important;}
    </style>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <ul id="tab-1" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-1-1"><?php echo Yii::t('front','updatebasicinfo'); ?></a></li>
                </ul>
                <div id="content-tab-1-1" class="tabs-content" style="padding: 20px;">
                    <form action="edit" method="post" class="fixed" id="contact-form" name="contact-form" onSubmit="return submitForm()">
                    <p><label for="newpwd" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','newpass'); ?> </label> <input type="text"
                                                                                               value=""
                                                                                               name="newpass"
                                                                                               id="newpass"
                                                                                               class="text validate[]">
                    </p>
                    <p><label for="newpwd_confirm" style="width: 100px;display: inline-block;"<?php echo Yii::t('front','confirmpass'); ?>> </label><input type="text"
                                                                                                   value=""
                                                                                                   name="newpass_confirm"
                                                                                                   id="newpass_confirm"
                                                                                                   class="text validate[equals[newpass]]">
                    </p>
                    <p><label for="tel" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','tel'); ?> <span class="required">*</span></label> <input type="text"
                                                                                                 value="<?php echo $user['tel']; ?>"
                                                                                                 name="tel"
                                                                                                 id="tel"
                                                                                                 class="text validate[required]">
                    </p>
                    <p><label for="phone" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','phone'); ?> <span class="required">*</span></label> <input type="text"
                                                                                                 value="<?php echo $user['phone']; ?>"
                                                                                                 name="phone"
                                                                                                 id="phone"
                                                                                                 class="text validate[required]">
                    </p>
                    <p><label for="email" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','email'); ?> <span class="required">*</span></label> <input type="text"
                                                                                                 value="<?php echo $user['email']; ?>"
                                                                                                 name="email"
                                                                                                 id="email"
                                                                                                 class="text validate[required,custom[email]]">
                    </p>
                    <p><input type="submit" value="<?php echo Yii::t('front','save'); ?>" name="submit" ></p>
                    </form>
                </div>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/memberbar.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>
<script>
    function submitForm(){
        if($("#contact-form").validationEngine('validate')){
            return true;
        }
        return false;
    }
</script>
</body>
</html>