<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
<style>
    .info td{padding: 0px !important;border: none !important;}
</style>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <ul id="tab-1" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-1-1"><?php echo Yii::t('front','basicinfo'); ?></a></li>
                </ul>
                <div id="content-tab-1-1" class="tabs-content" style="padding: 20px;">
                    <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','username'); ?> </label><?php echo Yii::app()->session['member']['username']; ?></p>
                    <?php
                    if(Yii::app()->session['member']['isadmin']==1){
                        ?>
                        <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','membertype'); ?> </label><?php echo Yii::t('front','manager'); ?></p>
                        <?php
                    }else{
                        if(Yii::app()->language=='zh_cn'){
                            ?>
                            <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','membertype'); ?> </label><?php echo Yii::app()->session['member']['groupname']; ?></p>
                            <?php
                        }else{
                            ?>
                            <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','membertype'); ?> </label><?php echo Yii::app()->session['member']['enname']; ?></p>
                            <?php
                        }

                    }
                    ?>

                    <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','loginnum'); ?> </label><?php echo Yii::app()->session['member']['login_num'];?></p>
                    <p><label for="name" style="width: 100px;display: inline-block;"><?php echo Yii::t('front','lastlogintime'); ?> </label><?php echo Yii::app()->session['member']['last_logintime'] ?></p>
                </div>

                <ul id="tab-2" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-2-1"><?php echo Yii::t('front','activestatus'); ?></a></li>
                </ul>
                <div id="content-tab-2-1" class="tabs-content" style="padding: 20px;">
                    <code>
                        <table style="width: 100%;margin: 0px;border: none;" class="info">
                            <tr>
                                <td style="width: 33%;"><div class="m1"></div><?php echo Yii::t('front','mgrfeedback'); ?>(<?php echo $feedback_counts; ?>)</td>
                                <td style="width: 33%;"><div class="m2"></div><span class="red"><?php echo $feedback_noread; ?></span><?php echo Yii::t('front','infohaveread'); ?></td>
                                <td style="width: 33%;"><div class="m3"></div><span class="red"><?php echo $feedback_haveread; ?></span><?php echo Yii::t('front','infonoread'); ?></span></td>
                            </tr>
                        </table>
                    </code>

                    <code>
                        <table style="width: 100%;margin: 0px;border: none;" class="info">
                            <tr>
                                <td style="width: 33%;"><div class="m1"></div><?php echo Yii::t('front','mgrfeedback'); ?>(<?php echo $message_counts; ?>)</td>
                                <td style="width: 33%;"><div class="m2"></div><span class="red"><?php echo $message_noread; ?></span><?php echo Yii::t('front','infohaveread'); ?></td>
                                <td style="width: 33%;"><div class="m3"></div><span class="red"><?php echo $message_haveread; ?></span><?php echo Yii::t('front','infonoread'); ?></span></td>
                            </tr>
                        </table>
                    </code>

                </div>

                <ul id="tab-3" class="tabs-menu fixed">
                    <li class="current"><a href="#content-tab-3-1"><?php echo Yii::t('front','membernotice'); ?></a></li>
                </ul>
                <div id="content-tab-3-1" class="tabs-content" style="padding: 20px;"><p>
                       <?php echo Yii::t('front','welcomeforregister'); ?>
                </p></div>


            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/memberbar.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>

</body>
</html>