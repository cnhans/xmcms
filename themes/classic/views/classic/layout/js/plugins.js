(function (a) {
    a(document).ready(function () {
        themeSliderTimeout = 1e4;
        themeSliderPauseOnHover = true;
        a(".tip").tipsy({gravity: "n", fade: true});
        a(".tip2").tipsy({gravity: "w", fade: true});
        a("a[rel^='prettyPhoto']").prettyPhoto({opacity: .8, show_title: false, default_width: 500, default_height: 500, theme: "light_square", hideflash: false, modal: false});
        if (a("#slideshow-index").size()) {
            a("#slideshow-index ul").cycle({timeout: themeSliderTimeout, fx: "fade", prev: "#text-slideshow-prev", next: "#text-slideshow-next", pager: "#index-slideshow-pager", delay: 0, speed: 1e3, pause: themeSliderPauseOnHover, cleartypeNoBg: true, pauseOnPagerHover: 0})
        }
        if (a("#slideshow-clients").size()) {
            a("#slideshow-clients ul").cycle({timeout: 8e3, fx: "fade", delay: 0, speed: 1e3, pause: true, cleartypeNoBg: true, pauseOnPagerHover: 0})
        }
        if (a("#slideshow-portfolio").size()) {
            a("#slideshow-portfolio ul").cycle({timeout: themeSliderTimeout, fx: "fade", pager: "#slideshow-portfolio-pager", delay: 0, speed: 1e3, pause: themeSliderPauseOnHover, cleartypeNoBg: true, pauseOnPagerHover: 0})
        }
        a("#tab-1, #tab-2, #tab-3, #tab-4, #tab-5").tabify();
        a("#accordion-1, #accordion-2, #accordion-3, #accordion-4, #accordion-5").accordion()
    })
})(window.jQuery)