(function (a) {
    a(document).ready(function () {
        a("ul#dropdown-menu li").hover(function () {
            a(this).addClass("hover");
            a("ul:first", this).css({visibility: "visible", display: "none"}).slideDown(200)
        }, function () {
            a(this).removeClass("hover");
            a("ul:first", this).css({visibility: "hidden"})
        });
        if (!(a.browser.msie && a.browser.version == 6)) {
            a("ul#dropdown-menu li ul li:has(ul)").find("a:first").addClass("arrow")
        }
        a(".service-overview li, .service-overview-index li").click(function () {
            window.location = a(this).find("a").attr("href");
            return false
        });
        a(".preview-options").css("opacity", "0");
        a(".portfolio-item-preview").hover(function () {
            a(this).find(".preview-options").stop().fadeTo(400, 1)
        }, function () {
            a(this).find(".preview-options").stop().fadeTo(200, 0)
        })
    })
})(window.jQuery)