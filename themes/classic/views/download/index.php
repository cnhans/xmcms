<?php
require Yii::app()->theme->viewPath.'/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $module['category']; ?>&nbsp;-&nbsp;Power by XmCMS</title>
    <meta name="Description" content=" <?php echo $module['category']; ?>"/>
    <meta name="Keywords" content="<?php echo $module['category']; ?>"/>
    <?php
    require Yii::app()->theme->viewPath.'/include/htmlheader.php';
    ?>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath.'/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <?php
                if(sizeof($downloads)>0){
                    foreach($downloads as $d){
                        ?>
                        <div class="blog-post">
                            <h3 class="blog-post-title"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/download/view/id/<?php echo $d['download_id']; ?>"><?php echo $d['title']; ?></a></h3>
                            <div class="blog-post-date">
                                <span><?php echo Yii::t('front','senduser'); ?><?php echo $d['username']; ?>
                                    &nbsp;&nbsp;<?php echo Yii::t('front','sendtime'); ?><?php echo $d['createtime']; ?></span>&nbsp;&nbsp;
                                <span class="fr"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/download/view/id/<?php echo $d['download_id']; ?>">
								<?php echo Yii::t('front','more'); ?>
								</a></span>
                            </div>
                            <?php
                            $content = $d['description'];
                            if(get_magic_quotes_gpc()){
                                $content=stripslashes($content);
                            }
                            echo csubstr(strip_tags($content),0,255);
                            ?>
                        </div>
                        <div class="hr"></div>
                    <?php
                    }
                }
                ?>
                <?php echo $pb->getBar(); ?>
            </div>
            <div class="col280 last">
                <ul class="side-nav">
                    <li <?php if ($menulist['module_id'] == $id) {
                        echo 'class=\'current\'';
                    } ?> ><a
                            href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $menulist['module']; ?>/index/id/<?php echo $menulist['module_id']; ?>"><?php echo $menulist['category']; ?></a>
                    </li>
                    <?php
                    foreach ($menulist['children'] as $m) {
                        ?>
                        <li style="padding-left: 25px;" <?php if ($m['module_id'] == $id) {
                            echo 'class=\'current\'';
                        } ?> ><a
                                href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $m['module']; ?>/index/id/<?php echo $m['module_id']; ?>"><?php echo $m['category']; ?></a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath.'/include/footer.php';
    ?>

</div>
</body>
</html>