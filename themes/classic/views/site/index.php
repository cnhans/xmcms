<?php
require Yii::app()->theme->viewPath.'/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content'];?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath.'/include/htmlheader.php';
    ?>
</head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath.'/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="slideshow-index">
            <ul>
                <?php
                if(sizeof($flashs['index_slider'])>0){
                    foreach($flashs['index_slider'] as $f){
                        ?>
                        <li><img src="<?php echo $f['imageurl']; ?>" width="880" height="360" alt="<?php echo $f['title']; ?>"/>
                            <div class="calltoaction"><h4><a href=""><?php echo $f['title']; ?></a></h4></div>
                            <div class="slidetext"><p><?php echo $f['imagedesc']; ?></p></div>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>
            <div id="index-slideshow-pager">&nbsp;</div>
        </div>
        <div class="fixed">
            <div class="col280"><h5 class="hasicon"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/index/icons/2.png" height="20" />
                <?php echo Yii::t('front','whatwedo'); ?></h5>
                <p class="last"><?php echo Yii::t('front','siteinfo_1'); ?></p></div>
            <div class="col280"><h5 class="hasicon"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/index/icons/1.png" height="20" />
                    <?php echo Yii::t('front','outsupport'); ?></h5>
                <p class="last"> <?php echo Yii::t('front','siteinfo_2'); ?></p></div>
            <div class="col280 last"><h5 class="hasicon"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/index/icons/3.png" height="20" alt=""/>
                    <?php echo Yii::t('front','ourwork'); ?></h5>
                <p class="last"><?php echo Yii::t('front','siteinfo_3'); ?></p>
            </div>
        </div>
        <div class="hr"></div>
        <div class="fixed">
            <div class="col280"><h5><?php echo Yii::t('front','whoweare'); ?></h5>
                <?php
                echo $other['index_summary']['description'];
                ?>
            </div>
            <div class="col580 last">
                <div class="fixed">
                    <div class="col280"><h5><?php echo Yii::t('front', 'ourservices'); ?></h5> <br/></div>
                    <div class="col280 text-right last"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/product/index">&#8212;<?php echo Yii::t('front', 'fullservicelist'); ?></a></div>
                </div>
                <div class="service-overview-index fixed">
                    <ul class="fixed">
                        <li><a href="javascript:return false;"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/services/280x130-1.png" width="280" height="130" alt=""/></a>
						 <h5><?php echo Yii::t('front','cmstitle'); ?></h5>
                            <p class="last"><?php echo Yii::t('front','cmssummary'); ?></p></li>
                        <li class="last"><a href="javascript:return false;"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/services/280x130-2.png" width="280" height="130" alt=""/></a>
                            <h5><?php echo Yii::t('front','crmtitle'); ?></h5>
                            <p class="last"><?php echo Yii::t('front','crmsummary'); ?></p></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php
    require Yii::app()->theme->viewPath.'/include/footer.php';
    ?>
</div>
</body>
</html>