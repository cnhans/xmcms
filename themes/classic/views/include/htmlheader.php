<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/style.css" rel="stylesheet"
      type="text/css" media="all"/>
<link href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/style-print.css" rel="stylesheet"
      type="text/css" media="print"/>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/jquery.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/tipsy/jquery.tipsy.js"
        type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/tipsy/css.tipsy.css" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/prettyphoto/jquery.prettyPhoto.js"
        type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/prettyphoto/css.prettyPhoto.css"
      rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/cycle/jquery.cycle.all.min.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/tabify/jquery.tabify-1.4.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/accordion/jquery.accordion.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/scripts.js"
        type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/plugins.js"
        type="text/javascript"></script>
<link href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/validationEngine/css/validationEngine.jquery.css"
      rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/validationEngine/jquery.validationEngine.js"
        type="text/javascript"></script>
<?php
if(Yii::app()->language=='en_us'){
    ?>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/validationEngine/jquery.validationEngine-en.js"
            type="text/javascript"></script>
    <?php
}else{
    ?>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/js/validationEngine/jquery.validationEngine-zh_CN.js"
            type="text/javascript"></script>
    <?php
}
?>
<script>
    function changelang(lang){
        $.get('<?php echo Yii::app()->request->baseUrl?>/index.php/site/changelang',{lang:lang},function(res){
            if(res == 1){
                var baseurl = "<?php echo Yii::app()->request->baseUrl?>";
                if(baseurl == ""){
                    baseurl = "/";
                }
                window.location.href=baseurl;
            }
        });
    }
</script>