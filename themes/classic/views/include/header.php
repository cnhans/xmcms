<div id="header" class="fixed">
    <div id="logo-header-widget-1" class="fixed"><a id="logo" href="index.html"> <img
                src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/images/logo.png"
                alt="logo"/> </a>
    </div>
    <div class="nav_lang">
        <ul>
            <li><a href="javascript:changelang('zh_cn')"><img alt="简体中文"
                                                                        src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/images/zh_cn.gif"></a>
            </li>
            <li><a href="javascript:changelang('en_us')"><img alt="English"
                                                                        src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/images/en.gif"></a>
            </li>
        </ul>
    </div>
    <div class="clr"></div>
    <div id="menu-header-wigdet-2" class="fixed">
        <div class="col655">
            <ul id="dropdown-menu" class="fixed">
                <li <?php
                if ($active == 'home') {
                    echo 'class=\'current\'';
                }
                ?> ><a href="/index.php"><?php echo Yii::t('front', 'home'); ?></a></li>
                <?php
                if(sizeof($modules)>0){
                    foreach ($modules as $m) {
                        ?>
                        <li <?php
                        if ($active == $m['module_id']) {
                            echo 'class=\'current\'';
                        }
                        ?> >

                            <?php
                            if($m['module']!='link'){
                                ?>
                                <a  target="<?php echo $m['target']; ?>"  href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $m['module']; ?>/index/id/<?php echo $m['module_id']; ?>"><?php echo $m['category']; ?></a>
                                <?php
                            }else{
                                ?>
                                <a target="<?php echo $m['target']; ?>" href="<?php echo $m['url']; ?>"><?php echo $m['category']; ?></a>
                                <?php
                            }
                            ?>


                            <?php
                            if ($m['cldcount'] > 0) {
                                ?>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($m['childs'] as $c) {
                                        ?>
                                        <li>

                                            <?php
                                            if($c['module']!='link'){
                                                ?>
                                                <a href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $c['module']; ?>/index/id/<?php echo $c['module_id']; ?>"><?php echo $c['category']; ?></a>
                                            <?php
                                            }else{
                                                ?>
                                                <a target="<?php echo $c['target']; ?>" href="<?php echo $c['url']; ?>"><?php echo $c['category']; ?></a>
                                            <?php
                                            }
                                            ?>


                                            <?php
                                            if ($c['cldcount'] > 0) {
                                                ?>
                                                <ul class="sub-menu">
                                                    <?php
                                                    foreach ($c['childs'] as $i) {
                                                    ?>
                                                    <li>

                                                        <?php
                                                        if($i['module']!='link'){
                                                            ?>
                                                            <a href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $i['module']; ?>/index/id/<?php echo $i['module_id']; ?>"><?php echo $i['category']; ?></a>
                                                        <?php
                                                        }else{
                                                            ?>
                                                            <a target="<?php echo $i['target']; ?>" href="<?php echo $i['url']; ?>"><?php echo $i['category']; ?></a>
                                                        <?php
                                                        }
                                                        ?>


                                                    </li>
                                                    <?php
                                                    }
                                                    ?>
                                                </ul>
                                                <?php
                                            }
                                            ?>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            <?php
                            }
                            ?>
                        </li>
                    <?php
                    }
                }
                ?>

            </ul>
        </div>
        <div id="header-widget-2" class="col205 last"><h6
                class="last text-right"><?php echo Yii::t('front', 'ourphone'); ?><?php echo $config_basic['webphone']['content']; ?></h6>
        </div>
    </div>
</div>