<ul class="side-nav">
    <li><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/member/index"><?php echo Yii::t('front','membercenter_index'); ?></a></li>
    <li><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/member/showedit"><?php echo Yii::t('front','membercenter_basic'); ?></a></li>
    <li><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/member/feedbacklst"><?php echo Yii::t('front','membercenter_feedback'); ?></a></li>
    <li><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/member/messagelst"><?php echo Yii::t('front','membercenter_message'); ?></a></li>
    <li><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/member/loginout"><?php echo Yii::t('front','membercenter_exit'); ?></a></li>
</ul>