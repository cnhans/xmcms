<div id="footer" class="fixed">
    <div class="fixed">
        <div id="footer-widget-1" class="col205"><p><img
                    src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/layout/images/logo.png" width="205"
                    alt=""/></p>

            <p class="last">
                <?php echo Yii::t('front', 'copyright'); ?>
                <br/><br/>
                备案号：<?php echo $config_basic['icp']['content']; ?>
            </p></div>
        <div id="footer-widget-2" class="col205"><h6><span><?php echo Yii::t('front', 'navigation'); ?></span></h6>
            <ul class="footer-nav">
                <?php
                if (Yii::app()->language == 'zh_cn') {
                    ?>
                    <li class="first"><a
                            href="<?php echo Yii::app()->request->baseUrl ?>/index.php/message/index/id/15"><?php echo Yii::t('front', 'message'); ?></a>
                    </li>
                <?php
                } else {
                    ?>
                    <li class="first"><a
                            href="<?php echo Yii::app()->request->baseUrl ?>/index.php/message/index/id/46"><?php echo Yii::t('front', 'message'); ?></a>
                    </li>
                <?php
                }
                ?>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/feedback/index/id/22"><?php echo Yii::t('front', 'onlinefeedback'); ?></a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/user/index"><?php echo Yii::t('front', 'membercenter'); ?></a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/manage/default/index"><?php echo Yii::t('front', 'manage'); ?></a>
                </li>
            </ul>
        </div>
        <div id="footer-widget-3" class="col205"><h6><span><?php echo Yii::t('front', 'welcometoourwebsite'); ?></span>
            </h6>
            <?php echo Yii::t('front', 'welcomeinfo'); ?>
        </div>
        <div id="footer-widget-4" class="col205 last"><h6><span><?php echo Yii::t('front', 'contact'); ?></span></h6>
            <ul id="contact-info">
                <li class="adress"><?php echo Yii::t('front', 'footaddress'); ?></li>
                <li class="phone"><?php echo $config_basic['webphone']['content']; ?></li>
                <li class="email"><a
                        href="mailto:<?php echo $config_basic['webemail']['content']; ?>"><?php echo $config_basic['webemail']['content']; ?></a>
                </li>
            </ul>
        </div>
    </div>
</div>


<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/online/css/base.css"/>
<div id="cus_ser">
    <div class="cus_ser_">
        <div class="title"></div>
        <ul>
            <li id="zqq"><a
                    href="tencent://message/?uin=458820281&Site=<?php echo $config_basic['webname']['content'];?>&Menu=yes"
                    target="_blank">458820281</a></li>
            <li id="zphone"><?php echo $config_basic['webphone']['content']; ?></li>
        </ul>
    </div>
    <span class="close"><img src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/online/images/icon_close.png"/></span></div>
<script>
    $("#cus_ser .close").click(function () {
        $("#cus_ser").css({
            display: 'none'
        });
    })
    $("#cus_ser").mouseover(function () {
        $(this).stop();
        $(this).animate({
                width: 165
            },
            400, 'swing');
    })
    $("#cus_ser").mouseout(function () {
        $("#cus_ser").stop();
        $("#cus_ser").animate({
                width: 32
            },
            400, 'swing');
    })
</script>
<div style="text-align:center;clear:both">
</div>

<div style="display: none;">
    <script type="text/javascript">
        var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F1257b5ae28f0de7e9190965080f43eaa' type='text/javascript'%3E%3C/script%3E"));
    </script>
</div>
