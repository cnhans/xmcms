<ul class="side-nav">
    <li <?php if ($menulist['module_id'] == $id) {
        echo 'class=\'current\'';
    } ?> ><a
            href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $menulist['module']; ?>/index/id/<?php echo $menulist['module_id']; ?>"><?php echo $menulist['category']; ?></a>
    </li>
    <?php
    if(sizeof($menulist['children'])>0){
        foreach ($menulist['children'] as $m) {
            ?>
            <li style="padding-left: 25px;" <?php if ($m['module_id'] == $id) {
                echo 'class=\'current\'';
            } ?> ><a
                    href="<?php echo Yii::app()->request->baseUrl?>/index.php/<?php echo $m['module']; ?>/index/id/<?php echo $m['module_id']; ?>"><?php echo $m['category']; ?></a>
            </li>
        <?php
        }
    }
    ?>
</ul>