<?php
require Yii::app()->theme->viewPath.'/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $module['category']; ?>&nbsp;-&nbsp;Power by XmCMS</title>
    <meta name="Description" content=" <?php echo $module['category']; ?>"/>
    <meta name="Keywords" content="<?php echo $module['category']; ?>"/>
    <?php
    require Yii::app()->theme->viewPath.'/include/htmlheader.php';
    ?>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath.'/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <?php
                if(sizeof($images)>0){
                    foreach($images as $i){
                        ?>
                        <div class="blog-post">
                            <h3 class="blog-post-title"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/image/view/id/<?php echo $i['image_id']; ?>"><?php echo $i['title']; ?></a></h3>
                            <div class="blog-post-date">
                                <span><?php echo Yii::t('front','senduser'); ?><?php echo $i['username']; ?>
                                    &nbsp;&nbsp;<?php echo Yii::t('front','sendtime'); ?><?php echo $i['createtime']; ?></span>&nbsp;&nbsp;
                                <?php
                                if (sizeof($i['tags']) > 0) {
                                    $iden = 0;
                                    foreach ($i['tags'] as $t) {
                                        if ($iden == 0) {
                                            ?>
                                            <a href="#"><?php echo $t['tag']; ?></a>
                                        <?php
                                        } else {
                                            ?>
                                            ,<a href="#"><?php echo $t['tag']; ?></a>
                                        <?php
                                        }
                                        $iden++;
                                    }
                                }
                                ?>
                                <span class="fr"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/image/view/id/<?php echo $i['image_id']; ?>"><?php echo Yii::t('front','more'); ?></a></span>
                            </div>
                            <img src="<?php echo $i['image']; ?>" style="max-width: 400px;"/>
                            <div class="cla"></div>
                            <?php echo csubstr(strip_tags($i['description']),0,255); ?>
                        </div>
                        <div class="hr"></div>
                    <?php
                    }
                }
                ?>
                <?php echo $pb->getBar(); ?>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath.'/include/footer.php';
    ?>

</div>
</body>
</html>