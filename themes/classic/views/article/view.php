<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if lt IE 7 ]>
<html class="ie6" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie7" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie8" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if IE 9 ]>
<html class="ie9" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="Description" content="<?php echo csubstr(strip_tags($article['description']), 0, 255); ?>"/>
    <meta name="Keywords" content="<?php echo $article['title']; ?>"/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>

        <div class="fixed">
            <div class="col580">
                <h1><?php echo $article['title']; ?></h1>
                <p><?php echo $article['description']; ?></p>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>


    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>
</body>
</html>