<?php
require Yii::app()->theme->viewPath.'/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $module['category']; ?>&nbsp;-&nbsp;Power by XmCMS</title>
    <meta name="Description" content="<?php echo $module['category']; ?>"/>
    <meta name="Keywords" content="<?php echo $module['category']; ?>"/>
    <?php
    require Yii::app()->theme->viewPath.'/include/htmlheader.php';
    ?>
<head>
<style type="text/css">
.blog-post-comment{margin-bottom: 30px !important;}
</style>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath.'/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <?php
                if (sizeof($messages) > 0) {
                    foreach ($messages as $m) {
                        ?>
                        <div class="blog-post">
                            <h3 class="blog-post-title"><a href="blog-post.html"><?php echo $m['title']; ?></a></h3>
                            <div class="blog-post-date">
                                <span><?php echo Yii::t('front','senduser'); ?><?php echo $m['user']; ?>
                                    &nbsp;&nbsp;<?php echo Yii::t('front','sendtime'); ?><?php echo $m['createtime']; ?></span></div>
                            <?php echo $m['description']; ?>
                        </div>
                        <div class="hr"></div>
                        <div class="blog-post-comment">
                            <p class="who"><?php echo Yii::t('front','reply'); ?><?php echo $m['replayer']; ?> / <?php echo $m['replytime']; ?></p>
                            <?php echo $m['reply']; ?></div>
                    <?php
                    }
                }
                ?>
                <?php echo $pb->getBar(); ?>
                <div class="clr"></div>
                <form  action="/<?php echo Yii::app()->request->baseUrl?>/index.php/message/message" method="post" id="contact-form" name="contact-form" onSubmit="return submitForm()">
                    <input type="hidden" name="module_id" value="<?php echo $id; ?>" />
                    <fieldset style="margin-top: 10px;"><p id="formstatus"></p>
                        <p><label for="name"><?php echo Yii::t('front','username'); ?> <span class="required">*</span></label> <br> <input type="text"
                                                                                                     value=""
                                                                                                     name="user"
                                                                                                     id="user"
                                                                                                     class="text validate[required]">
                        </p>
                        <p><label for="phone"><?php echo Yii::t('front','phone'); ?><span class="required">*</span></label> <br> <input type="text"
                                                                                                    value=""
                                                                                                    name="phone"
                                                                                                    id="phone"
                                                                                                    class="text validate[required]">
                        </p>
                        <p><label for="email"><?php echo Yii::t('front','email'); ?><span class="required">*</span></label> <br> <input
                                type="text" value="" name="email" id="email" class="text validate[required,custom[email]]"></p>
                        <p><label for="othercontact"><?php echo Yii::t('front','othercontact'); ?><span class="required">*</span></label> <br> <input
                                type="text"name="othercontact" id="othercontact" class="text validate[required]">&nbsp;&nbsp;<?php echo Yii::t('front','exampleqqmsn'); ?></p>
                        <p><label for="descrption"><?php echo Yii::t('front','content'); ?></label> <br> <textarea cols="25" rows="3" name="description"
                                                                             id="description" class="validate[required]"></textarea></p>
                        <p><label for="captcha"><?php echo Yii::t('front','validcode'); ?> <span class="required">*</span></label> <br> <input
                                type="text" value="" name="captcha" id="captcha" class="text validate[required]"></p>
                        <p>
                            <img src="/extra/captcha/captcha.php" id="_captcha" />
                            <a href="javascript:reloadCaptcha()" id="change-image"><?php echo Yii::t('front','catnotsee'); ?></a>
                        </p>
                        <?php
                        if($error == -1){
                            ?>
                            <div class="successmsg" style="margin: 25px;"><?php echo Yii::t('front','sendsuccess'); ?></div>
                        <?php
                        }else if($error == 3){
                            ?>
                            <div class="errormsg" style="margin:25px;"><?php echo Yii::t('front','validcodeerror'); ?></div>
                        <?php
                        }
                        ?>
                        <p><input type="submit" value="<?php echo Yii::t('front','send'); ?>" name="submit" ></p>
                    </fieldset>
                </form>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath.'/include/footer.php';
    ?>

</div>
<script>
    function submitForm(){
        if($("#contact-form").validationEngine('validate')){
            return true;
        }
        return false;
    }
    function reloadCaptcha(){
        document.getElementById('_captcha').src='/extra/captcha/captcha.php?'+Math.random()
    }
</script>
</body>
</html>