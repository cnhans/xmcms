<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $config_basic['webname']['content']; ?></title>
    <meta name="Description" content=" add description  ... "/>
    <meta name="Keywords" content=" add keywords     ... "/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180"
                                   alt=""/>

            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col280"><p class="last"><img style="margin-top:0;" alt="" class="img-align-left"
                                                     src="<?php echo Yii::app()->theme->baseUrl ?>/views/classic/content/contact/contact.png">
                      <?php echo Yii::t('front','worktime'); ?><br>
                    <?php echo Yii::t('front','worktime_1'); ?> <br> <?php echo Yii::t('front','worktime_2'); ?></p></div>
            <div class="col280"><p class="last"><strong><?php echo Yii::t('front','campanyaddress'); ?></strong> <br> <?php echo Yii::t('front','campanyaddressroad'); ?> <br>
                    <?php echo Yii::t('front','citypostcodecountry'); ?></p></div>
            <div class="col280 last"><p class="last"><strong><?php echo Yii::t('front','contact'); ?></strong> <br><?php echo Yii::t('front','phoneandfax'); ?> 15358062815
                    <br><?php echo Yii::t('front','email'); ?><a href="mailto:#">458820281@qq.com</a></p></div>
        </div>
        <div class="hr"></div>
        <div class="fixed">
            <div class="col580">
                <form action="/<?php echo Yii::app()->request->baseUrl?>/index.php/feedback/feedback" method="post" class="fixed" id="contact-form" name="contact-form" onSubmit="return submitForm()">
                    <input type="hidden" name="module_id" value="<?php echo $id; ?>" />
                    <fieldset><p id="formstatus"></p>

                        <p><label for="name"> <?php echo Yii::t('front','feedbacktype'); ?><span class="required">*</span></label> <br>
                            <select class="text" name="feedbacktype_id" style="width: 250px;padding: 5px;">
                                <?php
                                if(sizeof($feedbacktypes)>0){
                                    foreach($feedbacktypes as $ftype){
                                        if(Yii::app()->language =='zh_cn'){
                                            ?>
                                            <option value="<?php echo $ftype['feedbacktype_id']; ?>"><?php echo $ftype['feedbacktype']; ?></option>
                                        <?php
                                        }else{
                                            ?>
                                            <option value="<?php echo $ftype['feedbacktype_id']; ?>"><?php echo $ftype['enname']; ?></option>
                                        <?php
                                        }

                                    }
                                }
                                ?>
                            </select>
                        </p>
                        <p><label for="name"> <?php echo Yii::t('front','username'); ?><span class="required">*</span></label> <br> <input type="text"
                                                                                                     value=""
                                                                                                     name="user"
                                                                                                     id="user"
                                                                                                     class="text validate[required]">
                        </p>

                        <p><label for="email"> <?php echo Yii::t('front','email'); ?><span class="required">*</span></label> <br> <input
                                type="text" value="" name="email" id="email" class="text validate[required,custom[email]]"></p>

                        <p><label for="subject"><?php echo Yii::t('front','subject'); ?> <span class="required">*</span></label> <br> <input type="text"
                                                                                                       value=""
                                                                                                       name="title"
                                                                                                       id="title"
                                                                                                       class="text validate[required]">
                        </p>

                        <p><label for="descrption"><?php echo Yii::t('front','content'); ?> </label> <br> <textarea cols="25" rows="3" name="descrption"
                                                                          id="descrption" class="validate[required]"></textarea></p>
                        <p><label for="captcha"><?php echo Yii::t('front','validcode'); ?>  <span class="required">*</span></label> <br> <input
                                type="text" value="" name="captcha" id="captcha" class="text validate[required]"></p>
                        <p>
                            <img src="/extra/captcha/captcha.php" id="_captcha" />
                            <a href="javascript:reloadCaptcha()" id="change-image"><?php echo Yii::t('front','catnotsee'); ?> </a>
                        </p>
                        <?php
                        if($error == -1){
                            ?>
                            <div class="successmsg" style="margin: 25px;"><?php echo Yii::t('front','sendsuccess'); ?></div>
                            <?php
                        }else if($error == 3){
                          ?>
                            <div class="errormsg" style="margin:25px;"><?php echo Yii::t('front','validcodeerror'); ?></div>
                            <?php
                        }
                        ?>
                        <p><input type="submit" value="<?php echo Yii::t('front','send'); ?>" name="submit" ></p>
                    </fieldset>
                </form>
            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>
<script>
function submitForm(){
    if($("#contact-form").validationEngine('validate')){
        return true;
    }
    return false;
}
function reloadCaptcha(){
    document.getElementById('_captcha').src='/extra/captcha/captcha.php?'+Math.random()
}
</script>
</body>
</html>