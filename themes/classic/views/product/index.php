<?php
require Yii::app()->theme->viewPath . '/include/doctype.php';
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $module['category']; ?>&nbsp;-&nbsp;Power by XmCMS</title>
    <meta name="Description" content="<?php echo $module['category']; ?>"/>
    <meta name="Keywords" content=" <?php echo $module['category']; ?>"/>
    <?php
    require Yii::app()->theme->viewPath . '/include/htmlheader.php';
    ?>
<head>
<body>
<div id="wrap">

    <?php
    require Yii::app()->theme->viewPath . '/include/header.php';
    ?>

    <div id="content" class="fixed">
        <div id="page-header"><img src="<?php echo $flashs['item_bar'][0]['imageurl']; ?>" width="880" height="180" alt=""/>
            <div id="page-header-title"><?php echo $flashs['item_bar'][0]['title']; ?></div>
        </div>
        <div class="fixed">
            <div class="col580">
                <?php
                if (sizeof($products) > 0) {
                    foreach ($products as $p) {
                        ?>
                        <div class="blog-post">
                            <h3 class="blog-post-title"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/product/view/id/<?php echo $p['product_id']; ?>"><?php echo $p['productname']; ?></a>
                            </h3>

                            <div class="blog-post-date">
                                <span><?php echo Yii::t('front','senduser'); ?><?php echo $p['username']; ?>
                                    &nbsp;&nbsp;<?php echo Yii::t('front','sendtime'); ?><?php echo $p['createtime']; ?></span>
                                <?php
                                if (sizeof($p['tags']) > 0) {
                                    $i = 0;
                                    foreach ($p['tags'] as $t) {
                                        if ($i == 0) {
                                            ?>
                                            &nbsp;&nbsp;<a href="#"><?php echo $t['tag']; ?></a>
                                        <?php
                                        } else {
                                            ?>
                                            ,<a href="#"><?php echo $t['tag']; ?></a>
                                        <?php
                                        }
                                        $i++;
                                    }
                                }
                                ?>
                                &nbsp;&nbsp;<span class="fr"><a href="<?php echo Yii::app()->request->baseUrl?>/index.php/product/view/id/<?php echo $p['product_id']; ?>"><?php echo Yii::t('front','more'); ?></a></span>
                            </div>
                            <?php echo csubstr(strip_tags($p['productdesc']), 0, 255); ?>
                        </div>
                        <div class="hr"></div>
                    <?php
                    }
                }
                ?>
                <?php echo $pb->getBar(); ?>

            </div>
            <div class="col280 last">
                <?php
                require Yii::app()->theme->viewPath . '/include/menulist.php';
                ?>
            </div>
        </div>
    </div>

    <?php
    require Yii::app()->theme->viewPath . '/include/footer.php';
    ?>

</div>
</body>
</html>