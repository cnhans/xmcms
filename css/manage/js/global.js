function fadeOut(id){
	$("#"+id).fadeOut("slow");
}
function formView(current,form){
	$("input[name=current]").val(current);
	document.forms[form].submit();
}
function changeposition(formid,wo) {
	jQuery('#'+formid).validationEngine('hide');
	jQuery('input').attr('data-prompt-position',wo);
	jQuery('input').data('promptPosition',wo);
	jQuery('textarea').attr('data-prompt-position',wo);
	jQuery('textarea').data('promptPosition',wo);
	jQuery('select').attr('data-prompt-position',wo);
	jQuery('select').data('promptPosition',wo);
}
function hideid(id){
    $("#"+id).hide();
}
function alert(messa)
{
    var msg = '<div class=\"message message-error\" > <div class=\"image\"> <img height=\"32\" alt=\"Error\" src=\"/css/manage/style/images/error.png\"> </div> <div class=\"text\"> <h6>错误信息</h6> <span>'+messa+'</span> </div> <div class=\"dismiss\"> <a href=\"javascript:hideid(\'message\')\"></a> </div> </div>';
    $("#message").html(msg);
    $("#message").show();
}
function success(messa){
    var msg = '<div class=\"message message-success\" id=\"message-success\"> <div class=\"image\"> <img height=\"32\" alt=\"Success\" src=\"/css/manage/style/images/success.png\"> </div> <div class=\"text\"> <h6>成功信息</h6> <span>' + messa+'</span> </div> <div class=\"dismiss\"> <a href=\"javascript:hideid(\'message\')\"></a> </div> </div>';
    $("#message").html(msg);
    $("#message").show();
}
function notice(messa){

}
function warn(messa){

}

function confirm(callback,msg)
{
	 var msgstr = "确定删除吗?";
	 if(typeof(msg) != "undefined"){
		 msgstr = msg;
	 }
	 if($("#dialogconfirm").length==0)
	 {
		  $("body").append('<div id="dialogconfirm" ><div class="alert_msg" >'+msgstr+'</div></div>');
		 
	 }
	 $("#dialogconfirm").dialog({
		   autoOpen:true,
		   title:'删除消息框',
		   resizable: false,
		   height:140,
		   modal: true,
		   buttons:{
			    '确定':function(ev){
			    	  ev.preventDefault();
			          callback();
			          $(this).dialog('close');
			    },
			    '取消':function(ev){
			    	   ev.preventDefault();
			           $(this).dialog('close');
			    }
		   }
	  });
}
function mm(arr) {
	var hash = {};
	for (var i in arr) {
		if (hash[arr[i]]) {
			return true;
		}
		hash[arr[i]] = true;
	}
	return false;
}

// kindeditor拓展
var editoritem_all = [ 'source', '|', 'undo', 'redo', '|', 'preview', 'print',
    'template', 'cut', 'copy', 'paste', 'plainpaste', 'wordpaste', '|',
    'justifyleft', 'justifycenter', 'justifyright', 'justifyfull',
    'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent',
    'subscript', 'superscript', 'clearhtml', 'quickformat', 'selectall',
    '|', 'fullscreen', '/', 'formatblock', 'fontname', 'fontsize', '|',
    'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
    'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'flash',
    'media', 'insertfile', 'table', 'hr', 'emoticons', 'map', 'code',
    'pagebreak', 'anchor', 'link', 'unlink' ];
var editoritem_default = [ 'source', '|', 'undo', 'redo', '|', 'justifyleft',
    'justifycenter', 'justifyright', 'justifyfull', 'insertorderedlist',
    'insertunorderedlist', 'indent', 'outdent', 'subscript', 'superscript',
    'clearhtml', 'selectall', '/', 'formatblock', 'fontname', 'fontsize',
    '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
    'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'flash',
    'media', 'insertfile', 'table', 'emoticons', 'code', 'link', 'unlink' ];
var editoritem_more = [ 'source', '|', 'undo', 'redo', '|', 'preview', 'print',
    'template', 'cut', 'copy', 'paste', 'plainpaste', 'wordpaste', '|',
    'justifyleft', 'justifycenter', 'justifyright', 'justifyfull',
    'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent',
    'subscript', 'superscript', 'clearhtml', 'quickformat', 'selectall',
    '/', 'formatblock', 'fontname', 'fontsize', '|', 'forecolor',
    'hilitecolor', 'bold', 'italic', 'underline', 'strikethrough',
    'lineheight', 'removeformat', '|', 'image', 'flash', 'media',
    'insertfile', 'table', 'hr', 'emoticons', 'code', 'pagebreak',
    'anchor', 'link', 'unlink' ];
function triggerCK(){
    var cked = $("input[name=checkall]").attr("checked");
    if(typeof(cked) == "undefined"){
        $("input[name=ck]").removeAttr("checked");
    }else{
        $("input[name=ck]").attr("checked",true);
    }
}
function cancel(){
    window.history.go(-1);
}
/*
 JavaScript正则验证字符串是否为空
 用途：检查输入字符串是否为空或者全部都是空格
 输入量是一个字符串：str
 返回：如果输入量全是空返回true,否则返回false
 */
function isNull(str){
    if ( str == "" ) return true;
    var regu = "^[ ]+$";
    var re = new RegExp(regu);
    return re.test(str);
}

function initEdit(name,module,resizeType){
    if(resizeType == null){
        resizeType =1;
    }
    var editor = KindEditor.create('textarea[name="'+name+'"]', {
        resizeType : 1,
        allowPreviewEmoticons : true,
        allowImageUpload : true,
        allowFileManager : true,
        resizeType:resizeType,
        urlType : 'domain',
        items:editoritem_more,
        uploadJson:'../../common/file/upload.action',
        fileManagerJson:'../../common/file/filemanager.action',
        fileloc:"attach/"+module+"/"
    });
    return editor;
}
jQuery(function($){
    $.datepicker.regional['zh-CN'] = {
        closeText: '关闭',
        prevText: '&#x3c;上月',
        nextText: '下月&#x3e;',
        currentText: '今天',
        monthNames: ['一月','二月','三月','四月','五月','六月',
            '七月','八月','九月','十月','十一月','十二月'],
        monthNamesShort: ['一','二','三','四','五','六',
            '七','八','九','十','十一','十二'],
        dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
        dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
        dayNamesMin: ['日','一','二','三','四','五','六'],
        weekHeader: '周',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'};
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);


    $('input').attr('data-prompt-position','topRight');
    $('input').data('promptPosition','topRight');
    $('textarea').attr('data-prompt-position','topRight');
    $('textarea').data('promptPosition','topRight');
    $('select').attr('data-prompt-position','topRight');
    $('select').data('promptPosition','topRight');

});


function isNull( str ){
    if ( str == "" ) return true;
    var regu = "^[ ]+$";
    var re = new RegExp(regu);
    return re.test(str);
}
var ztree_ids = new Array();
function hideZtreeContent(event){
   if(!(event.target.className == 'ztree' || $(event.target).parents(".ztree").length>0 || event.target.className.indexOf('ztreeinput')!=-1)){
        if(ztree_ids.length>0){
            for(var i=0;i<ztree_ids.length;i++){
                $("#"+ztree_ids[i]).fadeOut("fast");

            }
        }
       $("body").unbind("mousedown", hideZtreeContent);
   }
}
function getSwfObjTempImg(placeholderid,filename,params,success,queued,errorh){
    return new SWFUpload({
        upload_url: "../../../extra/kindeditor/upload_json.php?dir=image",
        file_post_name:filename,
        file_size_limit : "10 MB",
        file_types : "*.jpg;*.png;*.gif",
        file_upload_limit : "0",
        file_queue_limit : "1",
        post_params:params,
        button_image_url : "../../../css/manage/plugins/swfupload/images/btn_swfupload.png",
        button_placeholder_id : placeholderid,
        button_width: 80,
        button_height: 30,
        flash_url : "../../../css/manage/plugins/swfupload/swfupload.swf",
        file_queued_handler:queued,
        upload_error_handler:errorh,
        upload_success_handler : success ,
        debug: false
    });
}
function clearCache(){
    $.get('/index.php/manage/seo/clearcache',null,function(res){
        if(res ==1){
            success("缓存刷新成功！");
        }
    });
}