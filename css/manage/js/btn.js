$(function(){
	$( "button.btn" ).button();
	$( "button.add" ).button({
        icons: {
            primary: "ui-icon-plus"
        }
    });
    $( "button.edit" ).button({
        icons: {
            primary: "ui-icon-pencil"
        }
    });
    $( "button.mgr" ).button({
        icons: {
            primary: "ui-icon-wrench"
        }
    });
    $( "button.del" ).button({
        icons: {
            primary: "ui-icon-trash"
        }
    });
    $( "button.select" ).button({
        icons: {
            primary: "ui-icon-check"
        }
    });
    $( "button.imp" ).button({
        icons: {
            primary: "ui-icon-arrowthick-1-s"
        }
    });
    $( "button.exp" ).button({
        icons: {
            primary: "ui-icon-arrowthick-1-n"
        }
    });
    $( "button.mail" ).button({
        icons: {
            primary: "ui-icon-mail-closed"
        }
    });
    $('.edit').bind('click', function(ev) {
        ev.preventDefault();
    });
    $('.del').bind('click', function(ev) {
        ev.preventDefault();
    });
    $('button.btn').bind('click', function(ev) {
        ev.preventDefault();
    });
});