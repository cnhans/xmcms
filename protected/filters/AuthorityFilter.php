<?php
Yii::import("application.modules.manage.util.LoginStat");
class AuthorityFilter extends CFilter{
	
	public function preFilter($filterChain){
		$requesturi = Yii::app()->request->requestUri;
		if(strpos($requesturi, '/index.php/manage/default')!=false){
			return true;
		}else{
			$user = Yii::app ()->session ['user'];
			if ($user != null) {
				return true;
			}else{

                $member = Yii::app ()->session ['member'];
                if($member['isadmin'] == 1){
                    $loginstat = new LoginStat();
                    $loginnum = $member['login_num']+1;

                    Yii::app()->db->createCommand("update xm_user set last_loginip = '".$loginstat->GetIP()."',last_logintime=now(),login_num= ".$loginnum." where user_id = ".$member['user_id'])->query();

                    session_start();
                    $_SESSION['IsAuthorized'] = true;
                    $userobj = Array();
                    $user = Yii::app()->db->createCommand("select * from xm_user where user_id = ".$member['user_id'])->queryRow();
                    $userobj['user'] = $user;
                    $admingrouppermission = Yii::app()->db->createCommand("select * from xm_admingroup_permission where admingroup_id = ".$user['admingroup_id'])->queryRow();
                    $userobj['admingrouppermission'] = $admingrouppermission;

                    if($member['loginname']!='administrator'){
                        $authlang = $admingrouppermission['authlang'];
                        if($authlang != 'All'){
                            Yii::app()->session['mgrlang'] = $authlang;
                        }
                    }else{
                        Yii::app()->session['mgrlang'] = 'zh_cn';
                    }

                    Yii::app()->session['user'] = $userobj;

                    header("Location:/index.php/manage/console/index");
                }else{
                    header("Location:/index.php/manage/default/index");
                }
			}
		}
	}
	
}