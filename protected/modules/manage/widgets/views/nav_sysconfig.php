<?php
require Yii::app()->basePath.'/modules/manage/util/MenuList.php';
?>
<div class="sidebar-nav">

	<div class="list-box">
        <div class="list-group" style="display: block;">
            <h2>系统设置<i></i></h2>
            <ul style="display: block;" >
                <?php
                $menuleft = $MenuList[0]['child'];
                $operpermissionarray = unserialize($user['admingrouppermission']['operpermissionarray']);
                if($user['user']['loginname'] == 'administrator'){
                    foreach($menuleft as $m){
                        $active = "item";
                        if($m['enkey']=='sysconfig_systeminfo'){
                            $active = "item selected";
                        }
                        ?>
                        <li>
                            <a class="<?php echo $active; ?>" target="mainframe" href="<?php echo $m['url']; ?>" navid="site_channel_category">
                                <div class="arrow"></div><div class="expandable"></div><div class="folder open"></div><span><?php echo $m['name']; ?></span>
                            </a>
                        </li>
                    <?php
                    }
                }else{
                    foreach($menuleft as $m){
                        if(array_key_exists($m['enkey'],$operpermissionarray)){
                            ?>
                            <li><a class="item" href="<?php echo $m['url']; ?>"><?php echo $m['name']; ?></a></li>
                        <?php
                        }
                    }
                }
                ?>
            </ul>
        </div>
	</div>
</div>