<?php
require Yii::app()->basePath.'/modules/manage/util/MenuList.php';
?>

<div class="sidebar-nav">
    <div class="list-box">
        <div class="list-group" style="display: block;">
            <h2>内容管理<i></i></h2>
            <ul style="display: block;"  >
                <?php
                $menuleft = $MenuList[3]['child'];
                $operpermissionarray = unserialize($user['admingrouppermission']['operpermissionarray']);
                if($user['user']['loginname'] == 'administrator'){
                    foreach($menuleft as $m){
                        if(sizeof($m['child'])>0){
                            $href="";
                            if($m['url']!=""){
                                $href = "href='".$m['url']."'";
                            }
                            ?>
                            <li>
                                <a class="item" target="mainframe" <?php echo $href; ?> navid="site_channel_category">
                                    <div class="arrow"></div><div class="expandable open"></div><div class="folder close"></div><span><?php echo $m['name']; ?></span>
                                </a>
                            </li>
                            <ul style="display: block;">
                                <?php
                                foreach($m['child'] as $c){
                                    ?>
                                    <li>
                                        <a class="item" target="mainframe" style="padding-left:40px;" href="<?php echo $c['url']; ?>" navid="site_channel_category">
                                            <div class="arrow"></div><div class="expandable"></div><div class="folder open"></div><span><?php echo $c['name']; ?></span>
                                        </a>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>
                            <?php
                        }else{
                            ?>
                            <li>
                                <a class="item" target="mainframe" href="<?php echo $m['url']; ?>" navid="site_channel_category">
                                    <div class="arrow"></div><div class="expandable"></div><div class="folder open"></div><span><?php echo $m['name']; ?></span>
                                </a>
                            </li>
                            <?php
                        }
                    }
                }else{
                    foreach($menuleft as $m){
                        if(array_key_exists($m['enkey'],$operpermissionarray)){
                            ?>
                            <li><a href="<?php echo $m['url']; ?>"><?php echo $m['name']; ?></a></li>
                        <?php
                        }
                    }
                }
                ?>
            </ul>


        </div>
    </div>
</div>


