<?php
require Yii::app()->basePath.'/modules/manage/util/MenuList.php';
?>
<style>
    .topmenu, .topmenu * {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .topmenu li {
        position: relative;
    }
    .topmenu li.top {
        border-left: 1px solid #4DC4F0;
        border-right: 1px solid #1DA0D0;
        font-size: 14px;
        height: 42px;
        line-height: 42px;
        padding: 0 15px;
    }
    .topmenu ul {
        position: absolute;
        display: none;
        top: 100%;
        left: 0;
        z-index: 99;
    }
    .topmenu > li {
        float: left;
    }
    .topmenu li:hover > ul,
    .topmenu li.sfHover > ul {
        display: block;
    }
    .topmenu li.sfHover sf-with-ul{
        color: #000000;
    }

    .normal{color: #FFFFFF;}

    .topmenu a {
        display: block;
        position: relative;
    }
    .topmenu ul ul {
        top: 0;
        left: 100%;
    }


    /*** DEMO SKIN ***/
    .topmenu {
        font-weight: bold;
        float: left;
    }
    .topmenu ul {
        box-shadow: 2px 2px 6px rgba(0,0,0,.2);
        min-width: 12em; /* allow long menu items to determine submenu width */
        *width: 12em; /* no auto sub width for IE7, see white-space comment below */
    }
    .topmenu a {
        text-decoration: none;
        zoom: 1; /* IE7 */
    }
    .topmenu a {
        color: #3699DD;
    }
    .topmenu li {
        white-space: nowrap; /* no need for Supersubs plugin */
        *white-space: normal; /* ...unless you support IE7 (let it wrap) */
        -webkit-transition: background .2s;
        transition: background .2s;
    }
    .topmenu ul li {
        color: #000000;
        border-bottom: 1px solid #E1EAF1;
    }
    .topmenu ul ul li {
        background: #FFFFFF;
    }
</style>
<div id="header-inner"  >
		<div class="fr">
            <ul id="user">
                <li class="first">网站语言：
                    <?php
                    $user = Yii::app ()->session ['user'];
                    $authlang = $user['admingrouppermission']['authlang'];
                    ?>
                    <select name="mgrlang" onchange="mgrLang(this.value)">
                        <?php
                        if($authlang=='All' || $user['user']['loginname'] == 'administrator'){
                            if($this->mgrlang=='zh_cn'){
                                ?>
                                <option value="zh_cn" selected="selected">简体中文</option>
                                <option value="en_us">英文</option>
                            <?php
                            }else{
                                ?>
                                <option value="zh_cn" >简体中文</option>
                                <option value="en_us" selected="selected">英文</option>
                            <?php
                            }
                        }else if($authlang=='zh_cn'){
                            ?>
                            <option value="zh_cn" selected="selected" >简体中文</option>
                        <?php
                        }else if($authlang=='en_us'){
                            ?>
                            <option value="en_us" selected="selected">英文</option>
                        <?php
                        }
                        ?>
                    </select>
                </li>
                <li class="highlight last">【<?php echo Yii::app()->session['user']['user']['username']?>】<a href="../default/logout" >退出</a></li>
            </ul>
		</div>
        <h2  class="fl"  style="width: 180px;"><img alt="新麦CMS" src="/css/manage/style/images/logo.png"></h2>
		<ul class="topmenu nav" id="menubar" style="float: left;margin-right: 10px;" >
            <?php
            if($user['user']['loginname'] == 'administrator'){
                foreach($MenuList as $m){
                    ?>
                    <li class="top"><a onclick="showMenu('<?php echo $m['modulekey']; ?>')"><span class="normal"><?php echo $m['name'] ?></span></a></li>
                <?php
                }
            }else{
                //通过group权限进行过滤
                $admingrouppermission = $user['admingrouppermission'];
                $operpermissionarray = unserialize($user['admingrouppermission']['operpermissionarray']);
                if(sizeof($operpermissionarray)!=null){
                    foreach($MenuList as $m){
                        if(array_key_exists($m['modulekey'],$operpermissionarray)){
                            ?>
                            <li class="top"><a><span class="normal"><?php echo $m['name'] ?></span></a></li>
                        <?php
                        }
                    }
                }

            }
            ?>

		</ul>
	</div>
<script>
$(function(){
    bindClick();
});
function showMenu($key){
    $.get('showmenu',{key:$key},function(res){
        $("#left").html(res);
        //1.载入第一个 2.绑定事件
        var alist = $("#left .list-group").find("a");
        var fa = alist[0];
        bindClick();
        if($(fa).attr("href")!=undefined){
            $(fa).trigger("click");
            $("#mainframe").attr("src",$(fa).attr("href"));
        }
    });
}
function bindClick(){
    var alist = $("#left .list-group").find("a");
    $(alist).bind("click",function(){
        var href= $(this).attr("href");
        if(href!=""){
            $(alist).removeClass("selected");
            $(this).addClass("selected");
        }
    });
}


</script>