<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<div id="content" >

    <div class="box" >
        <div class="title">
            <h5>界面设置>在线交流设置</h5>
        </div>
        <?php
        if($this->actExist('Add')){
            ?>
            <div class="viewbar tar ptr10" >
                <button class="btn" onclick="showedit()" >添加</button>
            </div>
        <?php
        }
        ?>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th class="tal">标题</th>
                    <th>QQ</th>
                    <th>淘宝旺旺</th>
                    <th>MSN</th>
                    <th>排序</th>
                    <?php
                    if($this->actExist('Edit,Del')){
                        ?>
                        <th class="last" style="width:150px;">工具</th>
                    <?php
                    }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $onlines ) > 0) {
                    foreach($onlines as $o){
                        ?>
                        <tr>
                            <td class="tal"><?php echo $o['name'];?></td>
                            <td class="tac"><?php echo $o['qq'];?></td>
                            <td class="tac"><?php echo $o['taobaowangwang'];?></td>
                            <td class="tac"><?php echo $o['msn'];?></td>
                            <td class="tac"><?php echo $o['seq'];?></td>
                            <?php
                            if($this->actExist('Edit,Del')){
                                ?>
                                <td class="last tac">
                                    <?php
                                    if($this->actExist('Edit')){
                                        ?>
                                        <button class="btn" onclick="showedit(<?php echo $o['online_id']; ?>)">编辑</button>
                                    <?php
                                    }
                                    if($this->actExist('Del')){
                                        ?>
                                        <button class="btn" onclick="del(<?php echo $o['online_id']; ?>)">删除</button>
                                    <?php
                                    }
                                    ?>
                                </td>
                            <?php
                            }
                            ?>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="hid">
        <div id="dig_online" class="dialog-form" title="图片编辑" >
            <form name="form_online" id="form_online" class="p10" method="post" action="edit" >
                <input type="hidden" name="online_id" />
                <table class="form">
                    <tr>
                        <td class="label">标题</td>
                        <td><input type="text" class="small validate[required] " name="name" />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">QQ</td>
                        <td><input type="text" class="small validate[required] " name="qq" />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">淘宝旺旺</td>
                        <td><input type="text" class="small " name="taobaowangwang" /></td>
                    </tr>
                    <tr>
                        <td class="label">MSN</td>
                        <td><input type="text" class="small " name="msn" /></td>
                    </tr>
                    <tr>
                        <td class="label">排序</td>
                        <td><input type="text" class="small validate[required,custom[integer]] " name="seq"  />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>
var current = null;
var title = "";
$(function(){
    $( "#dig_online" ).dialog({
        autoOpen: false,
        title:title,
        height: 390,
        width: 600,
        modal: true,
        resizable:false,
        draggable:false,
        buttons: {
            '确定':function(){
                if($('#form_online').validationEngine('validate')){
                    document.forms['form_online'].submit();
                }
            },
            '取消':function(){
                $( this ).dialog( "close" );
            }
        },
        close:function(){
            $("input[name=online_id]").val("");
            $("input[name=name]").val("");
            $("input[name=seq]").val("");
            $("input[name=qq]").val("");
            $("input[name=taobaowangwang]").val("");
            $("input[name=msn]").val("");
        },
        open: function() {
            if(current!=null){
                //编辑
                $.get('json',{id:current},function(res){

                    $("input[name=online_id]").val(res.online_id);
                    $("input[name=name]").val(res.name);
                    $("input[name=seq]").val(res.seq);
                    $("input[name=qq]").val(res.qq);
                    $("input[name=taobaowangwang]").val(res.taobaowangwang);
                    $("input[name=msn]").val(res.msn);

                },'json');
            }
        }
    });
});
function showedit(id){
    current = id;
    if(id!=null){
        title="在线交流编辑";
        $( "#dig_online" ).dialog("open");
    }else{
        title="在线交流添加";
        $( "#dig_online" ).dialog("open");
    }
}
function del(id){
    confirm(function(){
       window.location.href="del?id="+id;
    },'删除在线交流？')
}
</script>