<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>系统设置>上传文件管理</h5>
        </div>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/ckfinder/ckfinder.js "></script>
        <!--<script type="text/javascript" src="<?php /*echo Yii::app()->request->baseUrl*/?>/css/manage/plugins/ckfinder/lang/zh-cn.js"></script>-->
        <script type="text/javascript" charset="utf-8">
            $(function(){
                var ckfinder = new CKFinder();
                ckfinder.replace( 'ckfinder');
            });
        </script>
        <div class="form">
            <div id="ckfinder"></div>
        </div>
    </div>
</div>