<div id="content">
    <div class="box">
        <div class="title">
            <h5>界面设置>FLASH设置</h5>
        </div>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th  class="tal">标题</th>
                    <th>类型</th>
                    <th>宽高</th>
                    <th>键</th>
                    <?php
                    if($this->actExist('Edit')){
                        ?>
                        <th class="last" style="width:150px;">工具</th>
                    <?php
                    }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $flashs ) > 0) {
                    foreach($flashs as $f){
                        ?>
                        <tr>
                            <td class="tal"><?php echo $f['title'];?></td>
                            <td class="tac"><?php echo $f['flashtype']=='images'?"图片":"FLASH";?></td>
                            <td class="tac"><?php echo $f['width']."*".$f['height'];?></td>
                            <td class="tac"><?php echo $f['name'];?></td>
                            <?php
                            if($this->actExist('Edit')){
                                ?>
                                <td class="last tac"><button class="btn" onclick="showedit(<?php echo $f['flash_id']; ?>)">编辑图片</button></td>
                            <?php
                            }
                            ?>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function showedit(fid){
        window.location.href="showedit?flash_id="+fid;
    }
</script>