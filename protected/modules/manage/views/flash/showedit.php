<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<div id="content">
    <div class="box">
        <div class="title">
            <h5>界面设置>FLASH图片设置</h5>
        </div>
        <div class="viewbar tar ptr10">
            <button class="btn" onclick="showedit()">添加</button>
        </div>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th  class="tal">图片</th>
                    <th>标题</th>
                    <th>描述</th>
                    <th>排序</th>
                    <th class="last" style="width:150px;">工具</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $flashImages ) > 0) {
                    foreach($flashImages as $f){
                        ?>
                        <tr>
                            <td class="tal"><a href="<?php echo $f['imageurl'];?>" class="review"><img width="200px" height="123" src="<?php echo $f['imageurl'];?>"/></a></td>
                            <td class="tac"><?php echo $f['title'];?></td>
                            <td class="tac"><?php echo $f['imagedesc'];?></td>
                            <td class="tac"><?php echo $f['seq'];?></td>
                            <td class="last tac">
                                <button class="btn" onclick="showedit(<?php echo $f['flashimage_id']; ?>)">编辑</button>
                                <button class="btn" onclick="del(<?php echo $f['flashimage_id']; ?>)">删除</button>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="hid">
        <div id="dig_flashimage" class="dialog-form" title="图片编辑" >
            <form name="form_flashimage" id="form_flashimage" class="p10" method="post" action="edit" >
                <input type="hidden" name="flashimage_id" />
                <input type="hidden" name="flash_id" value="<?php echo $flash_id; ?>" />
                <table class="form">
                    <tr>
                        <td class="label">图片</td>
                        <td class="last">
                            <input type="text" class="small validate[required] " name="imageurl" readonly="readonly">&nbsp;&nbsp;
                            <button class="btn" id="imageselect">选择图片</button>&nbsp;&nbsp;<button class="btn"  onclick="viewImg()">预览</button>&nbsp;&nbsp;<span class="red">*</span>
                            <div class="hid"><a id="prviewimg"><img id="imgprviewimg" src=""/></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">标题</td>
                        <td><input type="text" class="small validate[required] " name="title" />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">链接</td>
                        <td><input type="text" class="small validate[required] " name="link" />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">描述</td>
                        <td><textarea class="w618 validate[required]" name="imagedesc"></textarea>&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">排序</td>
                        <td><input type="text" class="small validate[required,custom[integer]] " name="seq"  />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>
var current = null;
var flash_id = <?php echo $flash_id; ?>;
var title = "";
$(function(){
    $( "#dig_flashimage" ).dialog({
        title:title,
        autoOpen: false,
        height: 370,
        width: 600,
        modal: true,
        resizable:false,
        draggable:false,
        buttons: {
            '确定':function(){
                if($('#form_flashimage').validationEngine('validate')){
                    document.forms['form_flashimage'].submit();
                }
            },
            '取消':function(){
                $( this ).dialog( "close" );
            }
        },
        close:function(){
            $("input[name=flashimage_id]").val("");
            $("input[name=imageurl]").val("");
            $("input[name=title]").val("");
            $("input[name=link]").val("");
            $("textarea[name=imagedesc]").text("");
            $("input[name=seq]").val("");
        },
        open: function() {
            if(current!=null){
                //编辑
                $.get('json',{id:current},function(res){

                    $("input[name=flashimage_id]").val(current);
                    $("input[name=imageurl]").val(res.imageurl);
                    $("input[name=title]").val(res.title);
                    $("input[name=link]").val(res.link);
                    $("textarea[name=imagedesc]").text(res.imagedesc);
                    $("input[name=seq]").val(res.seq);

                    $("#prviewimg").attr("href",res.imageurl);
                    $("a#prviewimg").fancybox();

                },'json');
            }
        }
    });


    KindEditor.ready(function(K) {
        var editor = K.editor({
            allowFileManager : true,
            fileManagerJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/file_manager_json.php',
            uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php'
        });

        K('#imageselect').click(function() {
            editor.loadPlugin('image', function() {
                editor.plugin.imageDialog({
                    showRemote : true,
                    imageUrl : $("input[name=imageurl]").val(),
                    clickFn : function(url, title, width, height, border, align) {
                        $("input[name=imageurl]").val(url);
                        $("#prviewimg").attr("href",url);
                        $("a#prviewimg").fancybox();
                        editor.hideDialog();
                    }
                });
            });
        });

    });

    $("a.review").fancybox();

});
function showedit(fmid){
    current = fmid;
    if(fmid!=null){
        title="图片编辑";
        $( "#dig_flashimage" ).dialog("open");
    }else{
        title="图片添加";
        $( "#dig_flashimage" ).dialog("open");
    }
}
function del(fmid){
    confirm(function(){
        window.location.href="del?flashimage_id="+fmid+"&flash_id="+flash_id;
    },'确定删除图片？')
}
function viewImg(){
    var imageurl = $("input[name=imageurl]").val();
    if(!isNull(imageurl)){
        $("a#prviewimg").trigger("click");
    }else{
        alert("图片不能为空");
    }
}
</script>