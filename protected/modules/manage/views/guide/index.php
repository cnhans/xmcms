<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>内容管理>内容管理</h5>
        </div>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th class="tal">标题</th>
                    <th class="last" style="width:150px;">工具</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $guides ) > 0) {
                    foreach($guides as $g){
                        ?>
                        <tr>
                            <td class="tal"><?php echo $g['category'];?></td>
                            <td class="last tac">
                                <button class="btn" onclick="showedit(<?php echo $g['module_id']; ?>)">编辑</button>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
function showedit(id){
    window.location.href="showedit?id="+id;
}
</script>