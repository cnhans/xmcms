<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>新麦CMS - Powered by X-mai.com </title>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/style/reset.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/style/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/jqueryui/css/jquery-ui.custom.css" media="screen" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/jqueryui/jquery.js"></script>
</head>
<body>
<div id="header">
	<?php $this->widget('application.modules.manage.widgets.ViewWidgets',array('view'=>'menu','mgrlang'=>$this->mgrlang));?>
</div>
<?php echo $content; ?>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/jqueryui/jquery-ui.custom.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/js/global.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/js/btn.js"></script> 
<script>
function mgrLang(mgrlang){
	$.get('../console/mgrlang',{lang:mgrlang},function(res){
		if(res==true){
			window.location.reload();
		}
	},'json');
}
</script>
</body>
</html>    
