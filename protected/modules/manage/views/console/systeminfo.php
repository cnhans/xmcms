<style>
    #content div.box table td{
        border-bottom: none !important;
    }
</style>
<div id="content">
        <div class="box">
            <div class="title" style="margin: 0px !important;">
                <h5>首页&nbsp;›&nbsp;系统信息</h5>
            </div>
            <div class="table p0 mgt_10">
                <table>
                    <tr>
                        <td style="padding: 0px 5px 0px !important;vertical-align: top !important;" class="w50"><fieldset>
                                <legend>用户信息</legend>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="w50">用户名：管理员</td>
                                        <td class="w50">登录次数：<?php echo Yii::app()->session['user']['user']['login_num'];?></td>
                                    </tr>
                                    <tr>
                                        <td class="w50">IP：<?php echo Yii::app()->session['user']['user']['last_loginip'];?></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </fieldset>

                            <fieldset class="mgt_10">
                                <legend>站点信息</legend>
                                <table class="mgt_10">
                                    <tbody>
                                    <tr>
                                        <td class="w50">操作系统：<?php echo php_uname('s');?></td>
                                        <td class="w50">PHP环境：<?php echo PHP_VERSION;?></td>
                                    </tr>
                                    <tr>
                                        <td class="w50">Mysql版本：<?php echo $this->connection->getServerVersion();?></td>
                                        <td class="w50">版权所有： 惠山区长安新麦网络信息技术服务部</td>
                                    </tr>
                                    <tr>
                                        <td class="w50">程序名称：新麦企业网站管理系统 - XMCMS(简称)</td>
                                        <td class="w50">系统版本：v2</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </fieldset>

                            <fieldset class="mgt_10">
                                <legend>服务与支持</legend>
                                <table>
                                    <tr>
                                        <td>技术支持</td>
                                        <td>使用手册</td>
                                    </tr>
                                    <tr>
                                        <td>官方网站</td>
                                        <td>定制开发</td>
                                    </tr>
                                    <tr>
                                        <td>邮箱：service@x-mai.com </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </fieldset>

                        </td>
                        <td style="padding: 0px 0px 0px 5px !important;padding-left: 5px;vertical-align: top !important;"  class="w50"><fieldset>
                                <legend>未读信息</legend>
                                <table >
                                    <tbody>
                                    <tr>
                                        <td class="w50">
                                            反馈信息：<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/manage/feedback/index"><?php echo $new_feedback;?></a>
                                        </td>
                                        <td class="w50">在线留言：<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/manage/message/index"><?php echo $new_message;?></a></td>
                                    </tr>
                                    <tr>
                                        <td class="w50">友情链接：<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/manage/friendlink/index"><?php echo $new_friendlink;?></a></td>
                                        <td class="w50">会员注册：<a href="<?php echo Yii::app()->request->baseUrl?>/index.php/manage/user/index"><?php echo $new_user;?></a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </fieldset>

                            <fieldset class="mgt_10">
                                <legend>使用说明</legend>
                                <table class="table">
                                    <tr><td>第一步：在系统设置中进行设置基本信息、语言设置、安全与效率等</td></tr>
                                    <tr><td>第二步：在界面设置中选择网站模板并设置相关参数</td></tr>
                                    <tr><td>第三步：在栏目配置中设置网站导航栏目及相关参数</td></tr>
                                    <tr><td>第四步：在内容管理中添加网站内容、底部信息等</td></tr>
                                </table>
                            </fieldset>

                        </td>
                    </tr>
                </table>

            </div>
        </div>
</div>