<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<div id="content">
    <div class="box">
        <div class="title">
            <h5>内容管理>片段编辑</h5>
        </div>

        <div class="form" >
            <form name="form_edit" id="form_edit" action="edit" method="post" >
                <input type="hidden" name="id" value="<?php echo $id; ?>" />
                <table class="form">
                    <tr>
                        <td class="label">描述</td>
                        <td><input type="text" class="small validate[required] " name="description" value="<?php echo $fragment['description']; ?>" />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td class="label">关键值</td>
                        <td><input type="text" class="small validate[required] " name="key" value="<?php echo $fragment['key']==null?0:$fragment['key']; ?>"  />&nbsp;&nbsp;<span class="red">*</span></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <textarea name="content" style="width:100%;height:300px;">
                                <?php echo $fragment['content']; ?>
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><button class="btn" onclick="save()">保存</button></td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>

    var editor;
    KindEditor.ready(function(K) {
        editor = K.create('textarea[name="content"]', {
            items:editoritem_more,
            resizeType:1,
            allowFileManager : true,
            uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php',
            fileManagerJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/file_manager_json.php',
            newlineTag:'br'
        });
    });
    function save(){
        if($("#form_edit").validationEngine('validate')){
            editor.sync();
            var content = editor.html();;
            if(content.length>10){
                document.forms['form_edit'].submit();
            }else{
                alert('描述至少需要10字');
            }
        }
    }
</script>