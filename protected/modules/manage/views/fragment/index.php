<div id="content">
    <div class="box">
        <div class="title">
            <h5>内容管理>片段管理</h5>
        </div>
        <?php
        if($this->actExist('Add,Del')){
            ?>
            <div class="viewbar tar ptr10">
                <?php
                $this->actPermission('<button class="btn" onclick="showedit(0)">添加</button>',"Add");
                $this->actPermission('<button class="btn" onclick="delCked()">删除</button>','Del');
                ?>

            </div>
        <?php
        }
        ?>
        <form action="index" name="form_view">
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th class="tac" style="width: 25px;" ><input type="checkbox" name="checkall" onclick="triggerCK()"/></th>
                        <th class="tal">描述</th>
                        <th>关键值</th>
                        <?php
                        if($this->actExist('Edit,Del')){
                            ?>
                            <th class="last" style="width: 150px;">工具</th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(sizeof($fragments)>0){
                        foreach ( $fragments as $i ) {
                            ?>
                            <tr>
                                <td><input type="checkbox" name="ck" value="<?php echo $i['id']; ?>" /></td>
                                <td class="tal"><?php echo $i['description'];?></td>
                                <td class="tac"><?php echo $i['key'];?></td>
                                <?php
                                if($this->actExist('Edit,Del')){
                                    ?>
                                    <td class="tac">
                                        <?php
                                        $id = $i['id'];
                                        $this->actPermission('<button class="btn" onclick="showedit('.$id.')">编辑</button>','Edit');
                                        $this->actPermission('<button class="btn" onclick="del('.$id.')">删除</button>','Del');
                                        ?>
                                    </td>
                                <?php
                                }
                                ?>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<script>
    function showedit(id){
        window.location.href="showedit?id="+id;
    }
    function del(id){
        confirm(function(){
            window.location.href="del?id="+id;
        },'是否删除片段？')
    }
    function delCked(){
        confirm(function(){
            var cks = $("input[name=ck]:checked");
            if(cks.length>0){
                var ids = Array();
                for(var i=0;i<cks.length;i++){
                    ids.push($(cks[i]).val());
                }
                $.post('dels',{ids:ids.toString()},function(res){
                    if(res == 1){
                        window.location.reload();
                    }
                });
            }else{
                alert("请选择片段");
            }
        },'确定删除片段？');
    }
</script>