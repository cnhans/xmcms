<div id="content">
    <div class="box">
        <div class="title">
            <h5>界面设置>模板设置</h5>
        </div>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th  class="tal">模板目录</th>
                    <th>缩略图</th>
                    <th>默认</th>
                    <?php
                    if($this->actExist('Del')){
                        ?>
                        <th class="last" style="width:250px;">工具</th>
                    <?php
                    }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $templates ) > 0) {
                    foreach($templates as $t){
                        ?>
                        <tr>
                            <td class="tal"><?php echo $t;?></td>
                            <td class="tac"><a class="prviewimg" href="/themes/<?php echo $t;?>/view.jpg" ><img src="/themes/<?php echo $t;?>/view.jpg" width="80px" height="80px" /></a></td>
                            <?
                            if($defTheme == $t){
                                ?>
                                <td class="tac">是</td>
                            <?php
                            }else{
                                ?>
                                <td class="tac">否</td>
                            <?php
                            }
                            ?>
                            <?php
                            if($this->actExist('Del')){
                                ?>
                                <td class="last tac"><button class="btn" onclick="setDefault('<?php echo $t; ?>')">设为默认</button><button class="btn" onclick="showedit('<?php echo $t; ?>')">编辑模板</button><button class="btn" onclick="del('<?php echo $t; ?>')">删除模板</button></td>
                            <?php
                            }
                            ?>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.css" />
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
<script>
    var templatesize = <?php echo sizeof ( $templates ); ?>;
    $(function(){
        $("a.prviewimg").fancybox();
    });
    function setDefault(template){
        $.get('setDefault',{template:template},function(res){
            if(res==1){
                success("默认模板设置成功！")
            }
        });
    }
    function del(template){
        if(templatesize==1){
            alert('至少保留一个模板！');
        }else{
            confirm(function(){
                $.get('delTemplate',{template:template},function(res){
                    if(res == 1){
                        window.location.reload();
                    }
                });
            },'确定删除模板？')
        }
    }
    function showedit(template){
        window.location.href="showedit?template="+template;
    }
</script>