<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/zTree/zTreeStyle/zTreeStyle.css" />
<div id="content">
    <div class="box">
        <div class="title">
            <h5>界面设置>模板编辑</h5>
        </div>
        <?php
        if($this->actExist('Edit')){
            ?>
            <div class="viewbar tar ptr10">
                <?php $this->actPermission('<button class="btn" onclick="editTemplate()">编辑</button>',"Edit");?>
            </div>
        <?php
        }
        ?>
        <div class="table">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 200px;vertical-align: top !important;">
                        <ul id="tree_files" class="ztree"></ul>
                    </td>
                    <td class="last" style="vertical-align: top !important;">
                        <textarea name="description" style="width:100%;height:450px;">
                        </textarea>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/zTree/jquery.ztree.core-3.5.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/zTree/jquery.ztree.excheck-3.5.min.js"></script>
<script>
var setting = {
    view: {
        selectedMulti: false
    },
    async: {
        enable: true,
        url:"getFilelist?template=<?php echo $template; ?>",
        autoParam:[ "loc"]
    },
    callback: {
        onClick: fileTreeOnClick
    }
};

var loc = null;
function fileTreeOnClick(event, treeId, treeNode){
    if(treeNode.isParent == false){
       loc = treeNode.loc;

        var d=/\.[^\.]+$/.exec(loc);
        if(d=='.php'||d=='.css'){
            $.get('getCode',{loc:loc},function(res){
                $("textarea[name=description]").val(res);
            });
        }else{
            alert("该文件类型不可编辑");
        }
    }
}

function editTemplate(){
    if(loc!=null){
        $.post('settemplate',{loc:loc,htmlstr:$("textarea[name=description]").val()},function(res){
           if(res==1){
               success("模板编辑成功！");
           }
        });
    }else{
        alert("请选择模板！");
    }
}

$(function(){
    $.fn.zTree.init($("#tree_files"), setting);
});
</script>