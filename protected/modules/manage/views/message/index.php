<div id="content">
    <div class="box">
        <div class="title">
            <h5>内容管理>查看留言</h5>
        </div>
        <?php
        if($this->actExist('Del')){
            ?>
            <div class="viewbar tar ptr10">
                <?php
                $this->actPermission('<button class="btn" onclick="delCked()">删除</button>','Del');
                ?>
            </div>
        <?php
        }
        ?>
        <form action="index" name="form_view">
            <input type="hidden" name="current"/>
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th class="tac" style="width: 25px;" ><input type="checkbox" name="checkall" onclick="triggerCK()"/></th>
                        <th class="tal">作者</th>
                        <th>已读</th>
                        <th>是否审核</th>
                        <th>模块</th>
                        <th>提交时间</th>
                        <th class="last" style="width: 150px;">工具</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(sizeof($messages)>0){
                        foreach ( $messages as $m ) {
                            ?>
                            <tr>
                                <td><input type="checkbox" name="ck" value="<?php echo $f['feedback_id']; ?>" /></td>
                                <td class="tal"><?php echo $m['user'];?></td>
                                <td class="tac"><?php
                                    if($m['haveread']==0){
                                        echo "<span class='red'>否</span>";
                                    }else{
                                        echo "是";
                                    }
                                    ?></td>
                                <td class="tac"><?php
                                    if($m['isAuth']==0){
                                        echo "<span class='red'>否</span>";
                                    }else{
                                        echo "是";
                                    }
                                    ?></td>
                                <td class="tac"><?php echo $m['category']; ?></td>
                                <td class="tac"><?php echo $m['createtime'];?></td>
                                <td class="tac"><button class="btn" onclick="showedit(<?php echo $m['message_id']; ?>)">编辑</button>&nbsp;&nbsp;<button class="btn" onclick="del(<?php echo $m['message_id']; ?>)" >删除</button></td>
                            </tr>
                        <?php
                        }
                    }

                    ?>
                    </tbody>
                </table>
                <?php echo $pagebar->buildBar();?>
            </div>
        </form>
    </div>
</div>
<script>
    var current = <?php echo $pagebar->current;?>;
    function showedit(id){
        window.location.href="showedit?id="+id+"&current="+current;
    }
    function del(id){
        confirm(function(){
            window.location.href="del?id="+id+"&current="+current;
        },'是否删除消息？')
    }
    function delCked(){
        confirm(function(){
            var cks = $("input[name=ck]").val();
            if(cks.length>0){
                var ids = Array();
                for(var i=0;i<cks.length;i++){
                    ids.push(cks[i]);
                }
                $.post('dels',{ids:ids.toString()},function(res){
                    if(res == 1){
                        window.location.reload();
                    }
                });
            }else{
                alert("请选择文章信息");
            }
        },'确定删除文章信息？');
    }
</script>