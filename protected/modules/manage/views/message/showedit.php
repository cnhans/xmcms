<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<div id="content">
    <div class="box">
        <div class="title">
            <h5>内容管理>留言信息</h5>
        </div>

        <div class="form" >
            <form name="form_edit" id="form_edit" action="edit" method="post" >
                <input type="hidden" name="message_id" value="<?php echo $id; ?>" />
                <input type="hidden" name="current" value="<?php echo $current; ?>" />
                <table class="form">
                    <tr>
                        <td class="label">作者</td>
                        <td><?php echo $message['user']; ?></td>
                    </tr>
                    <tr>
                        <td class="label" style="vertical-align: top !important;"  >内容</td>
                        <td>
                            <textarea name="description" style="width:100%;height:250px;">
                                <?php echo $message['description']; ?>
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="label" style="vertical-align: top !important;"  >回复</td>
                        <td>
                            <textarea name="reply" style="width:100%;height:250px;">
                                <?php echo $message['reply']; ?>
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>是否审核</td>
                        <td>
                            <?php
                            if($message['isAuth'] == 0){
                                ?>
                                <input type="checkbox" name="isAuth" value="1"/>
                            <?
                            }else{
                                ?>
                                <input type="checkbox" name="isAuth" value="1" checked="checked"/>
                            <?
                            }
                            ?>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn" onclick="save()">保存</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>
    var editor;
    var editor2;
    KindEditor.ready(function(K) {
        editor = K.create('textarea[name="description"]', {
            items:editoritem_more,
            resizeType:1,
            allowFileManager : true,
            uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php',
            fileManagerJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/file_manager_json.php'
        });
        editor2 = K.create('textarea[name="reply"]', {
            items:editoritem_more,
            resizeType:1,
            allowFileManager : true,
            uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php',
            fileManagerJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/file_manager_json.php'
        });


    });
    function save(){
        if($("#form_edit").validationEngine('validate')){
            editor.sync();
            editor2.sync();
            var replay = editor2.html();
            if(replay.length>0){
                document.forms['form_edit'].submit();
            }else{
                alert('回复不能为空');
            }
        }
    }
</script>