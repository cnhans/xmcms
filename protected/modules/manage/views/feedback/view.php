<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<div id="content">
    <div class="box">
        <div class="title">
            <h5>内容管理>查看反馈</h5>
        </div>
        <div class="viewbar tar ptr10">
            <button class="btn" onclick="back()">返回</button>
        </div>
        <div class="form" >
            <form name="form_edit" id="form_edit" action="edit" method="post" >
                <input type="hidden" name="article_id" value="<?php echo $id; ?>" />
                <input type="hidden" name="current" value="<?php echo $current; ?>" />
                <table class="form">
                    <tr>
                        <td class="label">标题</td>
                        <td><?php echo $feedback['title']; ?></td>
                    </tr>
                    <tr>
                        <td class="label">作者</td>
                        <td><?php echo $feedback['user']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            内容：<?php echo $feedback['descrption'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">类型</td>
                        <td><?php echo $feedback['feedbacktype'] ?></td>
                    </tr>
                </table>
            </form>
        </div>

    </div>
</div>
<script>
function back(){
    window.location.href="index?current=<?php echo $current;?>";
}
</script>