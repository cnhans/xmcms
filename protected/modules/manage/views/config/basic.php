<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<style>
.swfupload{margin-bottom: -10px;}
</style>
<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>系统设置>基本配置</h5>
        </div>

        <div id="message"></div>

        <form name="form_editbasic" id="form_editbasic" method="post" action="edit" >
            <input type="hidden" name="module" value="basic" />
            <table class="form">
                <tr>
                    <td class="label">网站名称</td>
                    <td><input type="text" class="small validate[required] " name="webname" value="<?php echo $basicconfig['webname']['content'];?>"  />&nbsp;&nbsp;<span class="red">*</span></td>
                </tr>
                <tr>
                    <td class="label">网站LOGO</td>
                    <td><input type="text" class="small validate[required] " name="weblogo" value="<?php echo $basicconfig['weblogo']['content'];?>"  />
                        <?php
                        if($this->actExist('Edit')){
                            ?>
                            &nbsp;&nbsp;<button class="btn" id="imageselect">选择图片</button>
                        <?php
                        }
                        ?>
                        &nbsp;&nbsp;<button class="btn"  onclick="viewImg()">预览</button>&nbsp;&nbsp;<span class="red">*</span>
                        <div class="hid"><a id="prviewimg" href="<?php echo $basicconfig['weblogo']['content'];?>"><img id="imgprviewimg" src=""/></a></div>
                    </td>
                </tr>
                <tr>
                    <td class="label">网站网址</td>
                    <td><input type="text" class="small validate[required] " name="weburl" value="<?php echo $basicconfig['weburl']['content'];?>" />&nbsp;&nbsp;<span class="red">*</span></td>
                </tr>
                <tr>
                    <td class="label">网站关键字</td>
                    <td><input type="text" class="small validate[required] " name="webkeyword" value="<?php echo $basicconfig['webkeyword']['content']; ?>" />&nbsp;&nbsp;<span class="red">*</span>&nbsp;&nbsp;<span class="gray">多个关键词请用竖线|隔开，建议3到4个关键词。</span></td>
                </tr>
                <tr>
                    <td class="label">网站描述</td>
                    <td><textarea class="w618" name="webdesc" ><?php echo $basicconfig['webdesc']['content'];?></textarea>&nbsp;&nbsp;<span class="red">*</span>&nbsp;&nbsp;<span class="gray">100字以内</span></td>
                </tr>
                <tr>
                    <td class="label">ICP备案号</td>
                    <td><input type="text" class="small validate[required] " name="icp" value="<?php echo $basicconfig['icp']['content']; ?>" />&nbsp;&nbsp;<span class="red">*</span></span></td>
                </tr>
                <tr>
                    <td class="label">网站联系电话</td>
                    <td><input type="text" class="small  " name="webtel" value="<?php echo $basicconfig['webtel']['content']; ?>" /></td>
                </tr>
                <tr>
                    <td class="label">网站联系手机</td>
                    <td><input type="text" class="small validate[required] " name="webphone" value="<?php echo $basicconfig['webphone']['content']; ?>" />&nbsp;&nbsp;<span class="red">*</span></span></td>
                </tr>
                <tr>
                    <td class="label">网站联系Email</td>
                    <td><input type="text" class="small validate[required] " name="webemail" value="<?php echo $basicconfig['webemail']['content']; ?>" />&nbsp;&nbsp;<span class="red">*</span></span></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php
                        if($this->actExist('Edit')){
                            ?>
                            <button class="btn" onclick="saveBasicConfig()" >保存</button>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </form>

    </div>
</div>



<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>
var module = '<?php echo $module;?>';
var message = '<?php echo $message; ?>';
$(function() {

    if(message!=''){
        success(message);
    }

    KindEditor.ready(function(K) {
        var editor = K.editor({
            allowFileManager : true,
            uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php'
        });

        K('#imageselect').click(function() {
            editor.loadPlugin('image', function() {
                editor.plugin.imageDialog({
                    imageUrl : $("input[name=weblogo]").val(),
                    clickFn : function(url, title, width, height, border, align) {
                        $("input[name=weblogo]").val(url);
                        $("#prviewimg").attr("href",url);
                        $("a#prviewimg").fancybox();
                        editor.hideDialog();
                    }
                });
            });
        });

        $("a#prviewimg").fancybox();
    });

	
});
function saveBasicConfig(){
	if($("#form_editbasic").validationEngine('validate')){
		document.forms['form_editbasic'].submit();
	}
}

function viewImg(){
    var weblogo = $("input[name=weblogo]").val();
    if(!isNull(weblogo)){
        $("a#prviewimg").trigger("click");
    }else{
        alert("图片不能为空");
    }
}
</script>