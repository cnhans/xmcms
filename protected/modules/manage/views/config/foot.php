<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>系统设置>底部信息</h5>
        </div>

        <div class="form" >
            <form name="form_edit" id="form_edit" method="post" action="editfoot" >
                <table class="form">
                    <tr>
                        <td class="label">版权信息</td>
                        <td><input type="text" class="small validate[required] w618" name="copyright" value="<?php echo $foot_array['copyright']['content']; ?>"  /></td>
                    </tr>
                    <tr>
                        <td class="label">地址邮编</td>
                        <td><input type="text" class="small validate[required] w618" name="postcode" value="<?php echo $foot_array['postcode']['content']; ?>"  /></td>
                    </tr>
                    <tr>
                        <td class="label">联系方式</td>
                        <td><input type="text" class="small validate[required] w618" name="contact" value="<?php echo $foot_array['contact']['content']; ?>" /></td>
                    </tr>
                    <tr>
                        <td class="label">第三方代码
                            （如统计、在线客服代码）</td>
                        <td><input type="text" class="small validate[required] w618" name="othercode" value="<?php echo $foot_array['othercode']['content']; ?>" /></td>
                    </tr>
                    <tr>
                        <td class="label" style="vertical-align: top !important;" >底部其他信息</td>
                        <td><textarea name="otherinfo" style="width:100%;height:250px;">
                                <?php echo $foot_array['otherinfo']['content']; ?>
                            </textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <?php
                            if($this->actExist('Edit')){
                                ?>
                                <button class="btn" onclick="save()" >保存</button>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<link rel="stylesheet" href="../../../css/manage/plugins/kindeditor/themes/default/default.css" />
<script src="../../../css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="../../../css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script>
var editor;
KindEditor.ready(function(K) {
	editor = K.create('textarea[name="otherinfo"]', {
		items:editoritem_more,
		resizeType:1,
        allowFileManager : true,
        uploadJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/upload_json.php',
        fileManagerJson:'<?php echo Yii::app()->request->baseUrl?>/extra/kindeditor/file_manager_json.php'
	});
});
function save(){
    editor.sync();
    document.forms['form_edit'].submit();
}
</script>