<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css" />
<style>
    .swfupload{margin-bottom: -10px;}
</style>
<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>系统设置>基本配置</h5>
        </div>
        <div id="message"></div>
        <form name="form_editemail" id="form_editemail" method="post" action="edit" >
            <input type="hidden" name="module" value="email" />
            <table class="form">
                <tr>
                    <td class="label">发件人姓名</td>
                    <td><input type="text" class="small validate[required] " value="<?php echo $emailconfig['senduser']['content'];?>" name="senduser"  /></td>
                </tr>
                <tr>
                    <td class="label">邮箱账号</td>
                    <td><input type="text" class="small validate[required] " value="<?php echo $emailconfig['sendemail']['content'];?>" name="sendemail"  /></td>
                </tr>
                <tr>
                    <td class="label">邮件SMTP服务器</td>
                    <td><input type="text" class="small validate[required] " value="<?php echo $emailconfig['emailsmtp']['content'];?>" name="emailsmtp"  /></td>
                </tr>
                <tr>
                    <td class="label">邮箱密码</td>
                    <td><input type="text" class="small validate[required] " value="<?php echo $emailconfig['emailpwd']['content'];?>" name="emailpwd"  /></td>
                </tr>
                <tr>
                    <td class="label">邮件发送测试</td>
                    <td><input type="text" class="small" id="testemail" name="testemail"/>
                        <?php
                        if($this->actExist('Edit')){
                            ?>
                            <button class="btn" onclick="sendEmailTest()">点击测试</button>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><span class="green" >用于系统发送邮件，站内所有邮件都由该邮箱发送，所以请务必正确填写。</span></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php
                        if($this->actExist('Edit')){
                            ?>
                            <button class="btn" onclick="saveEmailConfig()" >保存</button>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </form>

    </div>
</div>
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/themes/default/default.css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.css" />
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/kindeditor-min.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/kindeditor/lang/zh_CN.js"></script>
<script charset="utf-8" src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/fancybox/jquery.fancybox-1.3.4.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script>
    var module = '<?php echo $module;?>';
    var message = '<?php echo $message; ?>';
    $(function() {
        if(message!=''){
            success(message);
        }

    });
    function sendEmailTest(){
        var testemail = $("input[name=testemail]").val();
        if(isNull(testemail)){
            $('#testemail').validationEngine('showPrompt', '测试邮箱不能为空');
        }else{
            $('#testemail').validationEngine('hide');
            $.get('testEmail',{email:testemail},function(res){
                if(res.type==true){
                    success('测试邮件发送成功');
                }else{
                    alert('邮件发送失败，请检查邮箱设置');
                }
            },'json');
        }
    }
    function saveEmailConfig(){
        if($("#form_editemail").validationEngine('validate')){
            document.forms['form_editemail'].submit();
        }
    }
</script>