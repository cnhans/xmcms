<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新麦CMS&nbsp;-&nbsp;企业信息管理系统</title>
</head>
<style>
    body{
        background: none repeat scroll 0 0 #F4F4F4;
        color: #737F89;
        font-family: Tahoma,'Microsoft Yahei','Simsun',sans-serif !important;
        font-size: 12px;
    }
    .login-input-user {
        background: url("/css/manage/login/username-input.png") repeat-x scroll center top transparent;
        border-color: #A0A4A9 #CED3D8 #CED3D8;
        border-image: none;
        border-radius: 3px;
        border-right: 1px solid #CED3D8;
        border-style: solid;
        border-width: 1px;
        color: #B6C3C9;
        font: 19px ;
        margin-bottom: 19px;
        padding: 10px 10px 10px 40px;
        width: 250px;
    }
    .login-input-pass {
        background: url("/css/manage/login/password-input.png") repeat-x scroll center top transparent;
        border-color: #A0A4A9 #CED3D8 #CED3D8;
        border-image: none;
        border-radius: 3px;
        border-right: 1px solid #CED3D8;
        border-style: solid;
        border-width: 1px;
        color: #B6C3C9;
        font: 19px ;
        margin-bottom: 19px;
        padding: 10px 10px 10px 40px;
        width: 250px;
    }
    .loginform {
        box-shadow: 0 4px 90px #EEEEEE;
        margin: 150px auto 0;
        width: 360px;
    }
    .loginform .title {
        background: url("/css/manage/login/login-title.png") no-repeat scroll 0 0 transparent;
        height: 50px;
        line-height:50px;
        text-align: center;
        color: #FFFFFF;
        font-size:18px;
    }
    .loginform .body {
        background: url("/css/manage/login/login-form-body-bg.png") repeat-x scroll center bottom transparent;
        padding: 23px 29px 40px;
    }
    .loginform .log-lab {
        color: #A4AAB2;
        display: block;
        font-size: 14px;
        font-weight: bold;
        padding-bottom: 11px;
    }
    .loginform .button {
        background: url("/css/manage/login/login-button.png") no-repeat scroll 0 0 transparent;
        border: medium none;
        color: #FFFFFF;
        font-size: 16px;
        font-weight: bold;
        height: 49px;
        width: 300px;
    }
</style>
<body>
	<div id="login" class="loginform">
		<!-- login -->
		<div class="title">
			 新麦CMS管理系统
		</div>
		<?php 
		if($message!=null){
		?>
		<div class="messages">
			<div id="message-error" class="message message-error">
				<div class="image">
					<img src="/css/manage/style/images/icons/error.png" alt="Error" height="32" />
				</div>
				<div class="text">
					<h6>错误信息</h6>
					<span><?php echo $message;?></span>
				</div>
			</div>
		</div>		
		<?php 
		}
		?>
		
		<div class="body">
			<form action="<?php echo Yii::app()->request->baseUrl?>/index.php/manage/default/index" method="post" name="form_DefaultIndex" >
				<div class="form">
					<!-- fields -->
					<div class="fields">
						<div class="field">
							<div class="log-lab">
								<label for="loginname">用户名：</label>
							</div>
							<div class="input">
								<input type="text"  name="loginname" size="40"
									value="" class="login-input-user" />
							</div>
						</div>
						<div class="field">
							<div class="log-lab">
								<label for="loginpass">密&nbsp;&nbsp;码：</label>
							</div>
							<div class="input">
								<input type="password" name="loginpass" size="40"
									value="" class="login-input-pass" />
							</div>
						</div>
						<div class="field">
							<br>
						</div>
						<div class="buttons">
							<input type="submit" class="button" name="sub_DefaultIndex" value="登&nbsp;&nbsp;入" />
						</div>
					</div>
					<div class="links">
						<!--<a href="http://www.x-mai.com/doc/product/cms/v1">用户手册</a>-->
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>


