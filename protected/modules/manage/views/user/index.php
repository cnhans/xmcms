<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>用户管理>会员管理</h5>
        </div>
        <?php
        if($this->actExist('Add')){
            ?>
            <div class="viewbar tar ptr10" >
                <button class="btn" onclick="showedit(0)" >添加</button>
            </div>
        <?php
        }
        ?>
        <form action="index" name="form_view">
            <input type="hidden" name="current"/>
            <input type="hidden" name="current" value="<?php echo $pagebar->会员功能 ?>" />
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th class="tal">姓名</th>
                        <th>会员类型</th>
                        <th>激活</th>
                        <th>最后登入时间</th>
                        <?php
                        if($this->actExist('Edit,Del')){
                            ?>
                            <th class="last" style="width: 150px;">工具</th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(sizeof($users)>0){
                        foreach ( $users as $u ) {
                            ?>
                            <tr>
                                <td><?php echo $u['username'];?></td>
                                <td class="tac"><?php echo $u['groupname'];?></td>
                                <td class="tac"><?php echo $u['isvalid']==1?"是":"否";?></td>
                                <td class="tac"><?php echo $u['last_logintime'];?></td>
                                <?php
                                if($this->actExist('Edit,Del')){
                                    ?>
                                    <td class="tac">
                                        <?php
                                        if($this->actExist('Edit')){
                                            ?>
                                            <button class="btn" onclick="showedit(<?php echo $u['user_id'];?>)">编辑</button>
                                        <?php
                                        }
                                        if($this->actExist('Del')){
                                            ?>
                                            <button class="btn" onclick="del(<?php echo $u['user_id'];?>)">删除</button>
                                        <?php
                                        }
                                        ?>

                                    </td>
                                <?php
                                }
                                ?>
                            </tr>
                        <?php
                        }
                    }

                    ?>
                    </tbody>
                </table>
                <?php echo $pagebar->buildBar();?>
            </div>
        </form>
    </div>
</div>
<script>
    var current = <?php echo $pagebar->current;?>;
    function showedit(id){
        window.location.href="showedit?id="+id+"&current="+current;
    }
    function del(id){
        confirm(function(){
            window.location.href="del?id="+id+"&current="+current;
        },'是否删除用户？')
    }
</script>