<link rel="stylesheet" type="text/css"
      href="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/validationEngine/css/validationEngine.jquery.css"/>
<link rel="stylesheet" type="text/css"
      href="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/zTree/zTreeStyle/zTreeStyle.css"/>
<style>
    .lineicon {
        position: relative;
        top: 6px;
    }

    .lineicon2 {
        position: relative;
        top: 6px;
        margin-left: 25px;
    }
</style>
<div id="content">
    <div class="box">
        <div class="title">
            <h5>栏目设置>栏目配置</h5>
        </div>
        <?php
        if ($this->actExist('Add,Edit')) {
            ?>
            <div class="viewbar tar ptr10">
                <?php
                if ($this->actExist('Add')) {
                    ?>
                    <button class="btn" onclick="showedit()">添加栏目</button>&nbsp;&nbsp;
                <?php
                }
                if ($this->actExist('Edit')) {
                    ?>
                    <button class="btn" onclick="saveSeq()">保存排序</button>
                <?php
                }
                ?>
            </div>
        <?php
        }
        ?>
        <form id="form_seq" name="form_seq" method="post" action="seq">
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th class="tal">标题</th>
                        <th class="tac" style="width: 200px;">模块</th>
                        <th class="tac" style="width: 200px;">排序</th>
                        <?php
                        if ($this->actExist('Del,Edit')) {
                            ?>
                            <th class="last" style="width: 150px;">工具</th>
                        <?
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($modules as $c) {
                        ?>
                        <tr>
                            <td class="tal">
                                <?php
                                if ($c ['cldcount'] > 0) {
                                    ?>
                                    <img src="../../../css/manage/style/images/colum1nx2.gif">&nbsp;&nbsp;
                                <?php
                                }
                                ?>
                                <?php echo $c['category']; ?></td>
                            <td class="tac"><?php echo $this->getModuleName($c['module']); ?></td>
                            <td class="tac"><input type="text" class="small validate[required,custom[integer]] "
                                                   name="seq_<?php echo $c['module_id']; ?>"
                                                   value="<?php echo $c['seq']; ?>"/></td>
                            <?php
                            if ($this->actExist('Del,Edit')) {
                                ?>
                                <td class="tac">
                                <?php
                                if ($this->actExist('Edit')) {
                                    ?>
                                    <button class="btn" onclick="showedit(<?php echo $c['module_id']; ?>)">编辑
                                    </button>&nbsp;&nbsp;
                                <?php
                                }
                                if ($this->actExist('Del')) {
                                    ?>
                                    <button class="btn" onclick="del(<?php echo $c['module_id']; ?>)">删除</button></td>
                                <?php
                                }
                            }
                            ?>

                        </tr>
                        <?php
                        if ($c ['cldcount'] > 0) {
                            foreach ($c ['childs'] as $cld) {
                                ?>
                                <tr>
                                    <td class="tal">
                                        <?php
                                        if ($cld ['cldcount'] > 0) {
                                            ?>
                                            <img src="../../../css/manage/style/images/bg_columnx.gif"
                                                 class="lineicon">&nbsp;&nbsp;<img
                                                src="../../../css/manage/style/images/colum1nx2.gif">&nbsp;&nbsp;
                                        <?php
                                        } else {
                                            ?>
                                            <img src="../../../css/manage/style/images/bg_columnx.gif"
                                                 class="lineicon">&nbsp;&nbsp;
                                        <?php
                                        }
                                        ?>
                                        <?php echo $cld['category']; ?></td>
                                    <td class="tac"><?php echo $this->getModuleName($cld['module']); ?></td>
                                    <td class="tac">
                                        <input type="text" class="small validate[required,custom[integer]] "
                                               name="seq_<?php echo $cld['module_id']; ?>"
                                               value="<?php echo $cld['seq']; ?>"/>
                                    </td>

                                    <?php
                                    if ($this->actExist('Del,Edit')) {
                                        ?>
                                        <td class="tac">
                                        <?php
                                        if ($this->actExist('Edit')) {
                                            ?>
                                            <button class="btn" onclick="showedit(<?php echo $cld['module_id']; ?>)">
                                                编辑
                                            </button>&nbsp;&nbsp;
                                        <?php
                                        }
                                        if ($this->actExist('Del')) {
                                            ?>
                                            <button class="btn" onclick="del(<?php echo $cld['module_id']; ?>)">删除
                                            </button></td>
                                        <?php
                                        }
                                    }
                                    ?>


                                </tr>
                                <?php
                                if ($cld ['cldcount'] > 0) {
                                    foreach ($cld ['childs'] as $lastcld) {
                                        ?>
                                        <tr>
                                            <td class="tal"><img
                                                    src="../../../css/manage/style/images/bg_column1.gif"
                                                    class="lineicon2">&nbsp;&nbsp;
                                                <?php echo $lastcld['category']; ?>
                                            </td>
                                            <td class="tac"><?php echo $this->getModuleName($lastcld['module']); ?></td>
                                            <td class="tac">
                                                <input type="text" class="small validate[required,custom[integer]] "
                                                       name="seq_<?php echo $lastcld['module_id']; ?>"
                                                       value="<?php echo $lastcld['seq']; ?>"/>
                                            </td>

                                            <?php
                                            if ($this->actExist('Del,Edit')) {
                                                ?>
                                                <td class="tac">
                                                <?php
                                                if ($this->actExist('Edit')) {
                                                    ?>
                                                    <button class="btn"
                                                            onclick="showedit(<?php echo $lastcld['module_id']; ?>)">编辑
                                                    </button>&nbsp;&nbsp;
                                                <?php
                                                }
                                                if ($this->actExist('Del')) {
                                                    ?>
                                                    <button class="btn"
                                                            onclick="del(<?php echo $lastcld['module_id']; ?>)">删除
                                                    </button></td>
                                                <?php
                                                }
                                            }
                                            ?>

                                        </tr>
                                    <?php
                                    }
                                }
                            }
                        }
                    }
                    ?>
                    </tbody>
                </table>
        </form>
    </div>
</div>

<div class="hid">
    <div id="dig_module" class="dialog-form" title="模块编辑">
        <form name="form_module" id="form_module" class="p10" method="post" action="edit">
            <input type="hidden" name="module_id"/>
            <table class="form">
                <tr>
                    <td class="label">上级目录</td>
                    <td><input type="text" class="small validate[required] " id="parent" name="parent"
                               onclick="showModule()" readonly="readonly"/>&nbsp;&nbsp;<span class="red">*</span>
                        <input type="hidden" name="parentid" id="parentid"/>

                        <div id="content_module" class="menuContent" style="display:none; position: absolute;">
                            <ul id="tree_module" class="ztree" style="margin-top:0; width:180px; height: 300px;"></ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="label">标题</td>
                    <td><input type="text" class="small validate[required] " name="category"/>&nbsp;&nbsp;<span
                            class="red">*</span></td>
                </tr>
                <tr>
                    <td class="label">排序</td>
                    <td><input type="text" class="small validate[required,custom[integer]] "
                               name="seq"/>&nbsp;&nbsp;<span class="red">*</span></td>
                </tr>
                <tr>
                    <td class="label">新窗口打开</td>
                    <td><input type="checkbox" value="_blank" name="target"/></td>
                </tr>
                <tr>
                    <td class="label">类别</td>
                    <td>
                        <select class="small" name="module">
                            <option value="-1">选择模块</option>
                            <option value="guide">内容</option>
                            <option value="feedback">在线反馈</option>
                            <option value="article">文章</option>
                            <option value="product">产品</option>
                            <option value="message">在线留言</option>
                            <option value="download">下载</option>
                            <option value="image">图片</option>
                            <option value="employee">招聘</option>
                            <option value="link">链接</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="label">链接</td>
                    <td><input type="text" class="small " name="url"/>&nbsp;&nbsp;<span
                            class="gray">类别选择“链接”的时候需要填写</span></td>
                </tr>
                <tr>
                    <td class="label">隐藏</td>
                    <td><input type="checkbox" value="1" name="ishid"/></td>
                </tr>
            </table>
        </form>
    </div>
</div>

</div>
<script
    src="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/validationEngine/jquery.validationEngine.js"></script>
<script
    src="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/validationEngine/jquery.validationEngine-zh_CN.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/zTree/jquery.ztree.core-3.5.min.js"></script>
<script
    src="<?php echo Yii::app()->request->baseUrl ?>/css/manage/plugins/zTree/jquery.ztree.excheck-3.5.min.js"></script>
<script>
    var current;
    var title;

    var module_setting = {
        data: {
            simpleData: {
                enable: true
            }
        },
        async: {
            enable: true,
            url: "tree"
        },
        callback: {
            onClick: module_onClick
        }
    };

    function module_onClick(e, treeId, treeNode) {
        if (treeNode.level >= 3) {
            alert('模块级数最多三级');
        } else {
            var zTree = $.fn.zTree.getZTreeObj("tree_module");
            zTree.checkNode(treeNode, true, null, true);
            $("#parent").attr("value", treeNode.name);
            $("#parentid").val(treeNode.id);
            $("#content_module").fadeOut("fast");
        }
    }

    $(function () {
        $("#dig_module").dialog({
            autoOpen: false,
            title: title,
            height: 450,
            width: 600,
            modal: true,
            resizable: false,
            draggable: false,
            buttons: {
                '确定': function () {
                    if ($('#form_module').validationEngine('validate')) {
                        document.forms['form_module'].submit();
                    }
                },
                '取消': function () {
                    $(this).dialog("close");
                }
            },
            close: function () {

            },
            open: function () {
                if (current != null) {
                    //编辑
                    $.get('json', {id: current}, function (res) {
                        $("input[name=module_id]").val(res.module_id);
                        $("input[name=parentid]").val(res.parentid);
                        $("input[name=category]").val(res.category);
                        $("input[name=seq]").val(res.seq);
                        if (res.target == '_self') {
                            $("input[name=target]").removeAttr("checked");
                        } else {
                            $("input[name=target]").attr("checked", "checked");
                        }
                        $("select[name=module]").val(res.module);
                        $("input[name=url]").val(res.url);

                        if (res.ishid == 1) {
                            $("input[name=ishid]").attr("checked", "checked");
                        } else {
                            $("input[name=ishid]").removeAttr("checked");
                        }

                        if (res.parentname == null) {
                            $("#parent").attr("value", "根目录");
                        } else {
                            $("#parent").attr("value", res.parentname);
                        }

                    }, 'json');
                } else {
                    $("input[name=seq]").val(0);
                    $("input[name=category]").val("");
                    $("input[name=parent]").val("");
                    $("input[name=url]").val("");
                    $("input[name=target]").removeAttr("checked");
                    $("input[name=ishid]").removeAttr("checked");
                }
            }
        });
        $.fn.zTree.init($("#tree_module"), module_setting);
        ztree_ids.push('content_module');
    });
    function showedit(id) {
        current = id;
        if (id != null) {
            title = "模块编辑";
            $("#dig_module").dialog("open");
        } else {
            title = "模块添加";
            $("#dig_module").dialog("open");
            $("select[name=module]").val(-1);
        }
    }
    function saveSeq() {
        if ($('#form_seq').validationEngine('validate')) {
            document.forms['form_seq'].submit();
        }
    }
    function showModule() {
        $("body").bind("mousedown", hideZtreeContent);
        var cityObj = $("#parent");
        var cityOffset = $("#parent").offset();
        $("#content_module").slideDown("fast");
    }
    function del(id) {
        confirm(function () {
            $.get('havechild', {id: id}, function (res) {
                if (Number(res) > 0) {
                    alert('存在子模块，请先删除子模块');
                } else {
                    window.location.href = "del?id=" + id;
                }
            });
        }, '删除模块时，模块下对应的信息也将会被删除。<br>确定删除模块？')
    }
</script>