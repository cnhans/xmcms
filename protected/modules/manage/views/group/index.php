<div id="content" >
    <div class="box" >
        <div class="title">
            <h5>用户管理>会员组管理</h5>
        </div>
        <?php
        if($this->actExist('Add')){
            ?>
            <div class="viewbar tar ptr10" >
                <button class="btn" onclick="showedit(0)" >添加</button>
            </div>
        <?php
        }
        ?>
        <div class="table">
            <table>
                <thead>
                <tr>
                    <th class="tal" >组名</th>
                    <th class="tal" >英文组名</th>
                    <?php
                    if($this->actExist('Edit,Del')){
                        ?>
                        <th class="last" style="width:150px;">工具</th>
                    <?php
                    }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php
                if (sizeof ( $groups ) > 0) {
                    foreach($groups as $g){
                        ?>
                        <tr>
                            <td class="tal"><?php echo $g['groupname'];?></td>
                            <td class="tal"><?php echo $g['enname'];?></td>
                            <?php
                            if($this->actExist('Edit,Del')){
                                ?>
                                <td class="last tac">
                                    <?php
                                    if($this->actExist('Edit')){
                                        ?>
                                        <button class="btn" onclick="showedit(<?php echo $g['group_id']; ?>)">编辑</button>
                                    <?php
                                    }
                                    if($this->actExist('Del')){
                                        ?>
                                        <button class="btn" onclick="del(<?php echo $g['group_id']; ?>)">删除</button>
                                    <?php
                                    }
                                    ?>
                                </td>
                            <?php
                            }
                            ?>
                        </tr>
                    <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
function showedit(id){
    window.location.href="showedit?id="+id;
}
function del(id){
    confirm(function(){
        window.location.href="del?id="+id;
    },'确定删除会员组？')
}
</script>