<div id="content">
    <div class="box">
        <div class="title">
            <h5>优化推广>友情链接</h5>
        </div>
        <?php
        if($this->actExist('Add')){
            ?>
            <div class="viewbar tar ptr10">
                <button class="btn" onclick="showedit(0)">添加</button>
            </div>
        <?php
        }
        ?>
        <form action="index" name="form_view">
            <input type="hidden" name="current"/>
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th class="tal">网站名称</th>
                        <th>地址</th>
                        <th>Logo地址</th>
                        <th>关键字</th>
                        <th>排序</th>
                        <th>是否审核</th>
                        <th>已读</th>
                        <th>类型</th>
                        <?php
                        if($this->actExist('Edit,Del')){
                            ?>
                            <th class="last" style="width: 150px;">工具</th>
                        <?php
                        }
                        ?>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(sizeof($friendlinks)>0){
                        foreach ( $friendlinks as $f ) {
                            ?>
                            <tr>
                                <td><?php echo $f['webname'];?></td>
                                <td class="tac"><?php echo $f['weburl']; ?></td>
                                <td class="tac"><?php echo $f['logourl'];?></td>
                                <td class="tac"><?php echo $f['keyword'];?></td>
                                <td class="tac"><?php echo $f['seq'];?></td>
                                <td class="tac"><?php echo $f['isauth']==0?"<span class='red'>否</span>":"是";?></td>
                                <td class="tac">
                                    <?php echo $f['haveread']==0?"<span class='red'>否</span>":"是";?>
                                </td>
                                <td class="tac"><?php echo $f['linktype']==1?"文字":"图片";?></td>
                                <?php if($this->actExist('Edit,Del')){
                                    ?>
                                    <td class="tac">
                                        <?php
                                        if($this->actExist('Edit')){
                                            ?>
                                            <button class="btn" onclick="showedit(<?php echo $f['friendlink_id']; ?>)">编辑</button>
                                        <?php
                                        }
                                        if($this->actExist('Del')){
                                            ?>
                                            <button class="btn" onclick="del(<?php echo $f['friendlink_id']; ?>)">删除</button>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                <?php
                                }
                                ?>
                            </tr>
                        <?php
                        }
                    }

                    ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
<script>

    function showedit(id){
        window.location.href="showedit?id="+id;
    }
    function del(id){
        confirm(function(){
            window.location.href="del?id="+id;
        },'是否删除产品？')
    }
</script>