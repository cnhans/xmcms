<?php
class PageBar{
	
	public $rows =20;
	public $total;
	public $current;
	public $jumpform = "formView";
	public $form = "form_view";
	
	public function setTotal($total) {
		$this->total = $total;
	}
	
	public function getStart(){
		return ($this->current-1)*$this->rows;
	}
	
	public function buildBar(){
		
		$this->total = intval($this->total);
		$this->rows = intval($this->rows);
		$this->current = intval($this->current);
		
		$barstr = "";
		$barstr .="<div class=\"pagination pagination-left\">";
		$barstr .="<div class=\"results\">";
		$frow = intval(($this->current-1)*$this->rows+1);
		$lrow = 0;
		$pagecount = intval($this->total%$this->rows==0?$this->total/$this->rows:$this->total/$this->rows+1);
		
		if($this->total-$frow>$this->rows){
			$lrow = $frow+$this->rows-1;
		}else{
			$lrow = $frow+$this->total%$this->rows-1;
		}
		$barstr .="<span>结果显示  ".$frow."-".$lrow." of ".$this->total."</span>";
		$barstr .="</div>";
		$barstr .="<ul class=\"pager\">";
		if(current>1){
			$barstr .="<li><a href=\"javascript:".$this->jumpform."("+($this->current-1)+",'".$this->form."')\">« 上一页</a></li>";
		}else{
			$barstr .="<li class=\"disabled\" >« 上一页</li>";
		}
		if($this->current+2<=$pagecount&&$this->current-2>=1){
			//中间
			for($i=$this->current-2;$i<=$this->current+2;$i++){
				if(i == current){
					$barstr .="<li class=\"current\">".$i."</li>";
				}else{
					$barstr .="<li><a href=\"javascript:".$this->jumpform."(".$i.",'".$this->form."')\" >".$i."</a></li>";
				}
			}
		}else if($this->current+2<=$pagecount&&$this->current-2<1){
			for($i=1;$i<=$pagecount;$i++){
				if($this->current<=$pagecount&&$i<=5){
					if($i == $this->current){
						$barstr .="<li class=\"current\">".$i."</li>";
					}else{
						$barstr .="<li><a href=\"javascript:".$this->jumpform."(".$i.",'".$this->form."')\">".$i."</a></li>";
					}
				}
			}
		}else{
			if($this->current-2>0&&$this->current+2>$pagecount){
				if($pagecount-4>0){
					for($i=$pagecount-4;i<=$pagecount;$i++){
						if($i == $this->current){
							$barstr .="<li class=\"current\">".$i."</li>";
						}else{
							$barstr .="<li><a href=\"javascript:".jumpform."(".$i.",'".$this->form."')\">".$i."</a></li>";
						}
					}
				}else{
					for($i=1;$i<=$pagecount;$i++){
						if($i == $this->current){
							$barstr .="<li class=\"current\">".$i."</li>";
						}else{
							$barstr .="<li><a href=\"javascript:".$this->jumpform."(".$i.",'".$this->form."')\">"+$i+"</a></li>";
						}
					}
				}
			}else{
				for($i=1;$i<=$pagecount;$i++){
					if($i == $this->current){
						$barstr .="<li class=\"current\">".$i."</li>";
					}else{
						$barstr .="<li><a href=\"javascript:".$this->jumpform."(".$i.",'".$this->form."')\">".$i."</a></li>";
					}
				}
			}
		}
		
		if($this->current<$pagecount){
			$barstr .="<li><a href=\"javascript:".$this->jumpform."(".($this->current+1).",'".$this->form."')\">下一页 »</a></li>";
		}else{
			$barstr .="<li class=\"disabled\" >下一页 »</li>";
		}
		$barstr .="</ul>";
		$barstr .="</div>";
		return $barstr;
		
		
	} 
	
}