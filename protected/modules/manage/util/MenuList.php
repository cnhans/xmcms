<?php
$MenuList = Array(
    0=>Array('name'=>'系统设置','enkey'=>'sysconfig','modulekey'=>'nav_sysconfig','child'=>Array(
        0=>Array('name'=>'系统信息','enkey'=>'sysconfig_systeminfo','url'=>'../console/systeminfo','isopen'=>true),
        1=>Array('name'=>'基本设置','enkey'=>'sysconfig_basicinfo','url'=>'../config/basic'),
        2=>Array('name'=>'系统邮箱设置','enkey'=>'sysconfig_email','url'=>'../config/email'),
        3=>Array('name'=>'上传文件管理','enkey'=>'sysconfig_upload','url'=>'../upload/index'),
        4=>Array('name'=>'使用手册','enkey'=>'sysconfig_doc','url'=>'http://www.x-mai.com/doc/product/cms/','isopen'=>true),
        5=>Array('name'=>'技术支持','enkey'=>'sysconfig_support','url'=>'http://www.x-mai.com/index.php/guide/aboutus','isopen'=>true)
    )),
    1=>Array('name'=>'界面设置','enkey'=>'pageconfig','modulekey'=>'nav_uiconfig','child'=>Array(
        0=>Array('name'=>'FLASH设置','enkey'=>'pageconfig_flash','url'=>'../flash/index'),
        1=>Array('name'=>'在线交流','enkey'=>'pageconfig_online','url'=>'../online/index'),
        2=>Array('name'=>'模板设置','enkey'=>'pageconfig_template','url'=>'../template/index')
    )),
    2=>Array('name'=>'栏目设置','enkey'=>'moduleconfig','modulekey'=>'nav_column','child'=>Array(
        0=>Array('name'=>'栏目管理','enkey'=>'moduleconfig_config','url'=>'../module/index')
    )),
    3=>Array('name'=>'内容管理','enkey'=>'contentconfig','modulekey'=>'nav_content','child'=>Array(
        0=>Array('name'=>'内容管理','enkey'=>'contentconfig_content','child'=>Array(
            0=>Array('name'=>'基本内容管理','enkey'=>'contentconfig_basic','url'=>'../guide/index'),
            1=>Array('name'=>'文章管理','enkey'=>'contentconfig_article','url'=>'../article/index'),
            2=>Array('name'=>'产品管理','enkey'=>'contentconfig_product','url'=>'../product/index'),
            3=>Array('name'=>'下载管理','enkey'=>'contentconfig_download','url'=>'../download/index'),
            4=>Array('name'=>'图片管理','enkey'=>'contentconfig_images','url'=>'../images/index'),
            5=>Array('name'=>'招聘管理','enkey'=>'contentconfig_employee','url'=>'../employee/index'),
            6=>Array('name'=>'片段管理','enkey'=>'contentconfig_fragment','url'=>'../fragment/index'),
            7=>Array('name'=>'查看留言','enkey'=>'contentconfig_message','url'=>'../message/index'),
            8=>Array('name'=>'查看反馈','enkey'=>'contentconfig_feedback','url'=>'../feedback/index'),
        )),
        1=>Array('name'=>'底部信息','enkey'=>'contentconfig_foot','url'=>'../config/foot'),
        2=>Array('name'=>'其他内容','enkey'=>'contentconfig_other','url'=>'../other/index')
    )),
    4=>Array('name'=>'优化推广','enkey'=>'seoconfig','modulekey'=>'nav_seo','child'=>Array(
        0=>Array('name'=>'友情链接','enkey'=>'seoconfig_friendlink','url'=>'../friendlink/index'),
        1=>Array('name'=>'刷新缓存','enkey'=>'seoconfig_clearcache','url'=>'javascript:clearCache()')
    )),
    5=>Array('name'=>'用户管理','enkey'=>'userconfig','modulekey'=>'nav_user','child'=>Array(
        0=>Array('name'=>'会员管理','enkey'=>'userconfig_user','url'=>'../user/index'),
        1=>Array('name'=>'会员组管理','enkey'=>'userconfig_group','url'=>'../group/index'),
        2=>Array('name'=>'管理员管理','enkey'=>'userconfig_admin','url'=>'../admin/index"'),
        3=>Array('name'=>'管理组管理','enkey'=>'userconfig_admingroup','url'=>'../admingroup/index'),
        4=>Array('name'=>'个人资料','enkey'=>'userconfig_config','url'=>'../admin/config'),
    ))
);




