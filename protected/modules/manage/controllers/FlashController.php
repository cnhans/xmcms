<?php
class FlashController extends ManageController {

	public function actionIndex() {
		
		$flashs = $this->connection->createCommand("select * from xm_flash where lang = '".$this->mgrlang."'")->queryAll();
		
		$data = Array();
		$data['flashs'] = $flashs;
		
		$this->render ('index',$data);
	}

    function actionShowedit($flash_id){
        $flashs = $this->connection->createCommand("select * from xm_flashimages where flash_id = {$flash_id} order by seq asc")->queryAll();

        $data = Array();
        $data['flashImages'] = $flashs;
        $data['flash_id'] = $flash_id;

        $this->render ('showedit',$data);
    }

    function actionEdit(){

        $flash_id = $_POST['flash_id'];
        $flashimage_id = $_POST['flashimage_id'];
        $imageurl = $_POST['imageurl'];
        $title = $_POST['title'];
        $imagedesc = $_POST['imagedesc'];
        $seq = $_POST['seq'];
        $link = $_POST['link'];

        if($flashimage_id==""){
            //添加
            $this->connection->createCommand("insert into xm_flashimages (flash_id,imageurl,imagedesc,seq,title,link) values ({$flash_id},'{$imageurl}','{$imagedesc}',{$seq},'{$title}','{$link}') ")->query();
        }else{
            //修改
            $imagedesc = addslashes($imagedesc);
            $this->connection->createCommand("update xm_flashimages set imageurl = '{$imageurl}',title='{$title}',seq={$seq},imagedesc='{$imagedesc}',link='{$link}' where flashimage_id = {$flashimage_id}  ")->query();
        }
        $this->redirect("showedit?flash_id=".$flash_id);
    }

    function actionDel($flashimage_id,$flash_id){
        $this->connection->createCommand("delete from xm_flashimages where flashimage_id = {$flashimage_id}")->query();
        $this->redirect("showedit?flash_id=".$flash_id);
    }

    function actionJson($id){
        $obj = $this->connection->createCommand("select * from xm_flashimages where flashimage_id = {$id}")->queryRow();
        echo json_encode($obj);
    }

}