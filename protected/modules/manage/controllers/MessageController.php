<?php
Yii::import("application.modules.manage.util.PageBar");
class MessageController extends ManageController {
	
	public function actionIndex($current=1) {
		$pb = new PageBar();
		$pb->current = $current;
		$pb->total = $this->connection->createCommand("select count(message_id) from xm_message where lang = '".$this->mgrlang."'")->queryScalar();
		
		$messages = $this->connection->createCommand("select m.*,mo.category from xm_message m left join xm_module mo on m.module_id = mo.module_id  where m.lang = '".$this->mgrlang."' order by m.createtime desc limit ".$pb->getStart().",".$pb->rows)->queryAll();
		
		$this->render ('index',array(
				'pagebar'=>$pb,
				'messages'=>$messages
		));
	}

    public function actionShowedit($id,$current){

        if($id!=0){
            $this->connection->createCommand("update xm_message set haveread = 1 where message_id = {$id}")->query();
            //编辑
            $message = $this->connection->createCommand("select * from xm_message where message_id = {$id}")->queryRow();
            $this->data['message'] = $message;
        }

        $this->data['id'] = $id;
        $this->data['current'] = $current;

        $this->render("showedit",$this->data);
    }

    public function actionEdit(){
        $message_id = $_POST['message_id'];
        $current = $_POST['current'];
        $description = $_POST['description'];
        $reply = $_POST['reply'];
        $isAuth = $_POST['isAuth'];
        $user = Yii::app()->session['user'];

        if($isAuth == ""){
            $isAuth = 0;
        }

        //进行编辑
        $this->connection->createCommand("update xm_message set description = '{$description}',reply='{$reply}',isAuth={$isAuth},haveread = 1,replyuser_id={$user['user']['user_id']},replytime=now()  where message_id = {$message_id}")->query();

       $this->redirect('index?current='.$current);
    }

}