<?php

class ConsoleController extends ManageController {
 
	public function actionIndex() {
		$this->render ('index');
	}

    function actionSysteminfo(){
        $new_feedback = $this->connection->createCommand("select count(feedback_id) from xm_feedback where haveread = 0")->queryScalar();
        $new_message = $this->connection->createCommand("select count(message_id) from xm_message where haveread = 0")->queryScalar();
        $new_friendlink = $this->connection->createCommand("select count(friendlink_id) from xm_firendlink where haveread = 0")->queryScalar();
        $new_user = $this->connection->createCommand("select count(user_id) from xm_user where isnew = 1")->queryScalar();

        $this->render ('systeminfo',Array('new_feedback'=>$new_feedback,'new_message'=>$new_message,'new_friendlink'=>$new_friendlink,'new_user'=>$new_user));
    }

    function actionShowmenu($key){
        $this->layout='//../../../protected/modules/manage/views/layouts/content';
        $this->data['key']=$key;
        $this->render("showmenu",$this->data);
    }

	public function actionMgrlang($lang){
		Yii::app()->session['mgrlang'] = $lang;
		exit(true);
	}
	
}