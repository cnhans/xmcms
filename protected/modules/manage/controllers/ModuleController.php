<?php
class ModuleController extends ManageController {

	public function actionIndex() {
		$modules = $this->connection->createCommand("select *,(select count(module_id) from xm_module where parentid = co.module_id) as cldcount from xm_module co where co.lang = '".$this->mgrlang."' and co.parentid =-1 order by co.seq asc")->queryAll();
		$i=0;
		foreach ($modules as $c){
			if($c['cldcount']>0){
                $modules[$i]['childs'] = $this->connection->createCommand("select *,(select count(module_id) from xm_module where parentid = co.module_id) as cldcount from xm_module co where co.lang = '".$this->mgrlang."' and co.parentid ='".$c['module_id']."'  order by co.seq asc")->queryAll();
				$j=0;
				foreach($modules[$i]['childs'] as $cd){
					if($cd['cldcount']>0){
                        $modules[$i]['childs'][$j]['childs'] = $this->connection->createCommand("select *,(select count(module_id) from xm_module where parentid = co.module_id) as cldcount from xm_module co where co.lang = '".$this->mgrlang."' and co.parentid ='".$cd['module_id']."'  order by co.seq asc")->queryAll();
					}
					$j++;
				}
			}
			$i++;
		}
		$this->render ('index',array('modules'=>$modules));
	}

    public function actionSeq(){
        $modules = $this->connection->createCommand("select * from xm_module where lang = '".$this->mgrlang."'")->queryAll();
        foreach($modules as $m){
            $seq = $_POST['seq_'.$m['module_id']];
            $this->connection->createCommand("update xm_module set seq = {$seq} where module_id = {$m['module_id']}")->query();
        }
        $this->redirect("index");
    }

    public function actionEdit(){

        $module_id = $_POST['module_id'];
        $parentid = $_POST['parentid'];
        $category = $_POST['category'];
        $seq = $_POST['seq'];
        $target = $_POST['target'];
        $module = $_POST['module'];
        $url = $_POST['url'];
        $ishid = $_POST['ishid'];

        if($target==""){
            $target = "_self";
        }
        if($ishid==""){
            $ishid = 0;
        }

        if($module_id==""){
            //添加
            $this->connection->createCommand("insert into xm_module (category,parentid,seq,target,module,url,lang,ishid) values ('{$category}',{$parentid},{$seq},'{$target}','{$module}','{$url}','".$this->mgrlang."',{$ishid})")->query();
            $insertid = Yii::app()->db->getLastInsertID();
            if($parentid == -1){
                $this->connection->createCommand("update xm_module set name = '{$insertid}::' where module_id = {$insertid}")->query();
            }else{
                $parentmodule = $this->connection->createCommand("select * from xm_module where module_id = {$parentid}")->queryRow();
                $this->connection->createCommand("update xm_module set name = '{$parentmodule['name']}{$insertid}::' where module_id = {$insertid}")->query();
            }

            if($module == 'guide' ){
                $this->connection->createCommand("insert into xm_guide (module_id,description,lang) values ({$insertid},'','".$this->mgrlang."')")->query();
            }

        }else{
            //修改
            $this->connection->createCommand("update xm_module set category ='{$category}',parentid={$parentid},seq={$seq},target='{$target}',module='{$module}',url= '{$url}',ishid={$ishid}  where module_id = {$module_id} ")->query();
            if($parentid == -1){
                $this->connection->createCommand("update xm_module set name = '{$module_id}::' where module_id = {$module_id}")->query();
            }else{
                $parentmodule = $this->connection->createCommand("select * from xm_module where module_id = {$parentid}")->queryRow();
                $this->connection->createCommand("update xm_module set name = '{$parentmodule['name']}{$module_id}::',parentid={$parentid} where module_id = {$module_id}")->query();
            }
        }

        $this->redirect("index");
    }

    public function actionJson($id){
        $module = $this->connection->createCommand("select m2.*,m1.category parentname from xm_module m2 left join xm_module m1 on m2.parentid = m1.module_id where m2.module_id = {$id} ")->queryRow();
        echo json_encode($module);
    }

    public function actionHavechild($id){
        $cldcunt = $this->connection->createCommand("select count(*) from xm_module where parentid = {$id}")->queryScalar();
        echo $cldcunt;
    }

    public function actionDel($id){
        $module = $this->connection->createCommand("select * from xm_module where module_id = {$id}")->queryRow();
        $mod = $module['module'];
        switch($mod){
            case 'guide':
                $this->connection->createCommand("delete from xm_guide where module_id = {$id}")->query();
                break;
            case 'article':
                $this->connection->createCommand("delete from xm_article where module_id = {$id}")->query();
                break;
            case 'product':
                $this->connection->createCommand("delete from xm_product where module_id = {$id}")->query();
                break;
            case 'message':
                $this->connection->createCommand("delete from xm_message where module_id = {$id}")->query();
                break;
            case 'download':
                $this->connection->createCommand("delete from xm_download where module_id = {$id}")->query();
                break;
            case 'image':
                $this->connection->createCommand("delete from xm_images where module_id = {$id}")->query();
                break;
            case 'employee':
                $this->connection->createCommand("delete from xm_employee where module_id = {$id}")->query();
                break;
            case 'feedback':
                $this->connection->createCommand("delete from xm_feedback where module_id = {$id}")->query();
                break;
        }

        $this->connection->createCommand("delete from xm_module where module_id = {$id}")->query();
        $this->redirect("index");
    }

    public function actionTree(){
        $modules = $this->connection->createCommand("select module_id as id,parentid pId,category as `name` from xm_module where lang = '".$this->mgrlang."' order by seq asc ")->queryAll();
        $modules[] = Array('id'=>-1,'name'=>'根目录','pid'=>0,'open'=>true);
        echo json_encode($modules);
    }
    public function actionTreeByType($type,$moduleid){
        $modules = $this->connection->createCommand("select module_id as id,parentid pId,category as `name`,module from xm_module where lang = '".$this->mgrlang."'  order by seq asc  ")->queryAll();
        $modules_array = Array();
        foreach($modules as $m){
            $m['open'] = true;
            if($m['module']!=$type){
                $m['nocheck'] = true;
            }
            if($moduleid == $m['id']){
                $m['checked'] = true;
            }
            $modules_array[] = $m;
        }
        echo json_encode($modules_array);
    }

    public function actionRoletree($lang){
        $modules = $this->connection->createCommand("select module_id as id,parentid pId,category as `name`,'true' as`open` from xm_module where lang = '".$lang."'")->queryAll();
        echo json_encode($modules);
    }
	
	public function getModuleName($module){
        switch($module){
            case 'guide':
                $module = '内容';
                break;
            case 'feedback':
                $module = '在线反馈';
                break;
            case 'article':
                $module = '文章';
                break;
            case 'product':
                $module = '产品';
                break;
            case 'message':
                $module = '在线留言';
                break;
            case 'download':
                $module = '下载';
                break;
            case 'image':
                $module = '图片';
                break;
            case 'employee':
                $module = '招聘';
                break;
            case 'link':
                $module = '链接';
                break;
        }
        return $module;
    }
	
}