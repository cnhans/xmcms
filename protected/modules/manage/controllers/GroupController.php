<?php
Yii::import("application.modules.manage.util.PageBar");
class GroupController extends ManageController {

	public function actionIndex() {
		$groups = $this->connection->createCommand("select * from xm_group   ")->queryAll();
		
		$this->render ('index',array(
				'groups'=>$groups
		));
	}

    public function actionShowedit($id){

        if($id!=0){
            $group = $this->connection->createCommand("select * from xm_group where group_id = {$id}")->queryRow();
            $this->data['group'] = $group;
        }

        $this->data['id'] = $id;

        $this->render("showedit",$this->data);
    }

    public function actionDel($id){

        $this->connection->createCommand("delete from xm_group where group_id = {$id}")->query();

        $this->redirect("index");
    }

    public function actionEdit(){
        $group_id = $_POST['group_id'];
        $groupname = $_POST['groupname'];
        $enname = $_POST['enname'];

        if($group_id == 0 ){
            //添加
            $this->connection->createCommand("insert into xm_group (groupname,lang,enname) values ('{$groupname}','".$this->mgrlang."','{$enname}')")->query();
        }else{
            $this->connection->createCommand("update xm_group set groupname = '{$groupname}',enname ='{$enname}' where group_id = {$group_id}")->query();
        }

        $this->redirect("index");

    }

}