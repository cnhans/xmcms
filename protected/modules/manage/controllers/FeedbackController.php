<?php
Yii::import("application.modules.manage.util.PageBar");
class FeedbackController extends ManageController {
	
	public function actionIndex($current=1) {
		$pb = new PageBar();
		$pb->current = $current;
		$pb->total = $this->connection->createCommand("select count(feedback_id) from xm_feedback where lang = '".$this->mgrlang."'")->queryScalar();
		
		$feedbacks = $this->connection->createCommand("select fb.*,fbt.feedbacktype,mo.category from xm_feedback fb left join xm_feedbacktype fbt on fb.feedbacktype_id = fbt.feedbacktype_id left join xm_module mo on fb.module_id = mo.module_id where fb.lang = '".$this->mgrlang."' order by fb.createtime desc limit ".$pb->getStart().",".$pb->rows)->queryAll();
		
		$this->render ('index',array(
				'pagebar'=>$pb,
				'feedbacks'=>$feedbacks
		));
	}

    public function actionView($id,$current){

        $this->data['id'] = $id;
        $this->data['current'] = $current;

        $this->connection->createCommand("update xm_feedback set haveread = 1 where feedback_id = {$id}")->query();
        $feedback = $this->connection->createCommand("select fb.*,fbt.feedbacktype  from xm_feedback fb left join xm_feedbacktype fbt on fb.feedbacktype_id = fbt.feedbacktype_id left join xm_module mo on fb.module_id = mo.module_id  where fb.feedback_id = {$id}")->queryRow();
        $this->data['feedback'] = $feedback;

        $this->render ('view',$this->data);
    }


}