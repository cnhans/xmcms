<?php
require_once Yii::app()->basePath.'/plugins/phpmailer/class.phpmailer.php';
require_once Yii::app()->basePath.'/plugins/phpmailer/class.smtp.php';
class ConfigController extends ManageController {

	public function actionBasic() {



		$basic_config = $this->connection->createCommand("select * from xm_config where lang ='".$this->mgrlang."' and keytype = 'basic' ")->queryAll();
		$basic_array = Array();
		foreach ($basic_config as $b){
			$basic_array[$b['syskey']] = $b;
		}
		
		$email_config = $this->connection->createCommand("select * from xm_config where lang ='system' and keytype = 'emailconfig'  ")->queryAll();
		$email_array = Array();
		foreach ($email_config as $e){
			$email_array[$e['syskey']] = $e;
		}
		$module = $_REQUEST['module'];
		if($module==null){
			$module = 'basic';
		}

        $this->data['basicconfig'] = $basic_array;
        $this->data['emailconfig'] = $email_array;
        $this->data['module'] = $module;

		$this->render ('basic',$this->data);
	}

    function actionEmail(){

        $email_config = $this->connection->createCommand("select * from xm_config where lang ='system' and keytype = 'emailconfig'  ")->queryAll();
        $email_array = Array();
        foreach ($email_config as $e){
            $email_array[$e['syskey']] = $e;
        }

        $this->data['emailconfig'] = $email_array;

        $this->render ('email',$this->data);
    }
	
	public function actionEdit() {
		
		$module = $_POST['module'];
		
		if($module == 'basic'){
			$webname = $_POST['webname'];
			$weblogo = $_POST['weblogo'];
			$weburl = $_POST['weburl'];
			$webkeyword = $_POST['webkeyword'];
			$webdesc = $_POST['webdesc'];
            $icp = $_POST['icp'];
            $webtel = $_POST['webtel'];
            $webphone = $_POST['webphone'];
            $webemail = $_POST['webemail'];


			$this->connection->createCommand("update xm_config set content = '".$webname."' where syskey = 'webname' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
			$this->connection->createCommand("update xm_config set content = '".$weblogo."' where syskey = 'weblogo' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
			$this->connection->createCommand("update xm_config set content = '".$weburl."' where syskey = 'weburl' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
			$this->connection->createCommand("update xm_config set content = '".$webkeyword."' where syskey = 'webkeyword' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
			$this->connection->createCommand("update xm_config set content = '".$webdesc."' where syskey = 'webdesc' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
            $this->connection->createCommand("update xm_config set content = '".$icp."' where syskey = 'icp' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
            $this->connection->createCommand("update xm_config set content = '".$webtel."' where syskey = 'webtel' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
            $this->connection->createCommand("update xm_config set content = '".$webphone."' where syskey = 'webphone' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();
            $this->connection->createCommand("update xm_config set content = '".$webemail."' where syskey = 'webemail' and keytype = 'basic' and lang = '".$this->mgrlang."'")->query();

            $this->data['message'] = "基本信息编辑成功";
            $this->forward("basic",$this->data);
		}else{
			
			$senduser = $_POST['senduser'];
			$sendemail = $_POST['sendemail'];
			$emailsmtp = $_POST['emailsmtp'];
			$emailpwd = $_POST['emailpwd'];
			
			$this->connection->createCommand("update xm_config set content = '".$senduser."' where syskey = 'senduser' and keytype = 'emailconfig' and lang = 'system'")->query();
			$this->connection->createCommand("update xm_config set content = '".$sendemail."' where syskey = 'sendemail' and keytype = 'emailconfig' and lang = 'system'")->query();
			$this->connection->createCommand("update xm_config set content = '".$emailsmtp."' where syskey = 'emailsmtp' and keytype = 'emailconfig' and lang = 'system'")->query();
			$this->connection->createCommand("update xm_config set content = '".$emailpwd."' where syskey = 'emailpwd' and keytype = 'emailconfig' and lang = 'system'")->query();

            $this->data['message'] = "Email信息编辑成功";
            $this->forward("email",$this->data);
		}


	}

    public function actionEditfoot(){

        $copyright = $_POST['copyright'];
        $postcode = $_POST['postcode'];
        $contact = $_POST['contact'];
        $othercode = $_POST['othercode'];
        $otherinfo = $_POST['otherinfo'];

        $this->connection->createCommand("update xm_config set content = '{$copyright}' where syskey = 'copyright' and keytype ='foot' and lang = '".$this->mgrlang."'")->query();
        $this->connection->createCommand("update xm_config set content = '{$postcode}' where syskey = 'postcode' and keytype ='foot' and lang = '".$this->mgrlang."'")->query();
        $this->connection->createCommand("update xm_config set content = '{$contact}' where syskey = 'contact' and keytype ='foot' and lang = '".$this->mgrlang."'")->query();
        $this->connection->createCommand("update xm_config set content = '{$othercode}' where syskey = 'othercode' and keytype ='foot' and lang = '".$this->mgrlang."'")->query();
        $this->connection->createCommand("update xm_config set content = '{$otherinfo}' where syskey = 'otherinfo' and keytype ='foot' and lang = '".$this->mgrlang."'")->query();

        $this->forward("foot");
    }

	public function actionTestEmail(){
		$email = $_GET['email'];
		
		$basic_config = $this->connection->createCommand("select * from xm_config where lang ='".$this->mgrlang."' and keytype = 'basic' ")->queryAll();
		$basic_array = Array();
		foreach ($basic_config as $b){
			$basic_array[$b['syskey']] = $b;
		}
		
		$email_config = $this->connection->createCommand("select * from xm_config where lang ='system' and keytype = 'emailconfig'  ")->queryAll();
		$email_array = Array();
		foreach ($email_config as $e){
			$email_array[$e['syskey']] = $e;
		}
		
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host = $email_array['emailsmtp']['content'];
		$mail->SMTPAuth = true;
		$mail->Username = $email_array['sendemail']['content'];
		$mail->Password = $email_array['emailpwd']['content'];
		$mail->Port=25;
		$mail->setFrom($email_array['sendemail']['content'],$email_array['senduser']['content']);
		$mail->addAddress($email, '邮箱测试账户');
		$mail->Subject = '这是来自'.$basic_array['webname']['content'].'邮箱测试邮件';
		$mail->Body='这是来自'.$basic_array['webname']['content'].'邮箱测试邮件';
		if (!$mail->send()) {
			echo json_encode(Array('result'=>false,'message'=>$mail->ErrorInfo));
		} else {
			echo json_encode(Array('result'=>true));
		}
		
	}
	
	
	public function actionFoot() {

        $foot_config = $this->connection->createCommand("select * from xm_config where lang ='".$this->mgrlang."' and keytype = 'foot' ")->queryAll();
        $foot_array = Array();
        foreach ($foot_config as $f){
            $foot_array[$f['syskey']] = $f;
        }

		$this->render ('foot',Array('foot_array'=>$foot_array));
	}
	
	
}