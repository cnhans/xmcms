<?php
Yii::import("application.modules.manage.util.PageBar");
class UserController extends ManageController {

	public function actionIndex($current=1) {
		$pb = new PageBar();
		$pb->current = $current;
		$pb->total = $this->connection->createCommand("select count(user_id) from xm_user  where isadmin = 0")->queryScalar();
		
		$users = $this->connection->createCommand("select u.*,g.groupname from xm_user u left join xm_group g on u.group_id = g.group_id  where u.isadmin = 0 order by u.createtime desc limit ".$pb->getStart().",".$pb->rows)->queryAll();
		
		$this->render ('index',array(
				'pagebar'=>$pb,
				'users'=>$users
		));
	}

    public function actionShowedit($id,$current){

        if($id !=0){
            $userinfo = $this->connection->createCommand("select u.*,g.groupname from xm_user u left join xm_group g on u.group_id = g.group_id  where u.user_id = {$id}")->queryRow();
            $this->data['userinfo'] = $userinfo;
        }

        $groups = $this->connection->createCommand("select * from xm_group")->queryAll();
        $this->data['groups'] = $groups;

        $this->data['id'] = $id;
        $this->data['current'] = $current;

		$this->render ('showedit',$this->data);
	}

    public function actionEdit(){

        $user_id = $_POST['user_id'];
        $current = $_POST['current'];

        $sex = $_POST['sex'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $tel = $_POST['tel'];

        $loginname = $_POST['loginname'];
        $username = $_POST['username'];
        $group_id = $_POST['group_id'];
        $isvalid = $_POST['isvalid']==""?0:1;
        $loginpass = $_POST['loginpass'];

        if($user_id == 0){
            $this->connection->createCommand("insert into xm_user (sex,email,phone,tel,loginname,username,group_id,isvalid,createtime,loginpass) values
             ({$sex},'{$email}','{$phone}','{$tel}','{$loginname}','{$username}',{$group_id},{$isvalid},now(),'{$loginpass}')")->query();
        }else{
            $this->connection->createCommand("update xm_user set sex = {$sex},email='{$email}',phone='{$phone}',tel='{$tel}',group_id={$group_id},isvalid={$isvalid} where user_id = {$user_id} ")->query();
        }

        $this->redirect("index?current=".$current);
    }

    public function actionDel($id,$current){

        $id = $_GET['id'];
        $current = $_GET['current'];

        $this->connection->createCommand("delete from xm_user where user_id = {$id}")->query();

        $this->redirect("index?current=".$current);
    }

    public function actionChangepwd(){

        $user_id = $_POST['user_id'];
        $oldpasswd = $_POST['oldpasswd'];
        $newpass = $_POST['newpass'];

        $msg = Array();

        $user = $this->connection->createCommand("select * from xm_user where user_id = {$user_id}")->queryRow();

        if((md5($oldpasswd) == $user['loginpass']) || ($user['loginpass']=="") ){
            $msg['type'] = true;
            $newpass = md5($newpass);
            $this->connection->createCommand("update xm_user set loginpass = '{$newpass}' where user_id = {$user_id}")->query();
        }else{
            $msg['type'] = false;
            $msg['message']="旧密码错误";
        }

        echo json_encode($msg);
    }

}