<?php
Yii::import("application.modules.manage.util.PageBar");
class AdmingroupController extends ManageController {

    public function actionIndex() {

        $groups = $this->connection->createCommand("select * from xm_admingroup")->queryAll();
        $this->data['groups'] = $groups;

        $this->render ('index',$this->data);
    }

    public function actionShowedit($id){

        if($id != 0){
            $admingroup = $this->connection->createCommand("select * from xm_admingroup where admingroup_id = {$id}")->queryRow();
            $this->data['admingroup'] = $admingroup;
        }

        $this->data['id']=$id;

        $this->render("showedit",$this->data);
    }

    public function actionEdit(){
        $admingroup_id = $_POST['admingroup_id'];
        $groupname = $_POST['groupname'];

        if($admingroup_id == 0){
            //添加
            $this->connection->createCommand("insert into xm_admingroup (groupname) values ('{$groupname}')")->query();
        }else{
            $this->connection->createCommand("update xm_admingroup set groupname = '{$groupname}' where admingroup_id = {$admingroup_id}")->query();
        }

        $this->redirect("index");
    }

    public function actionDel($id){

        $this->connection->createCommand("delete from xm_admingroup where admingroup_id = {$id}")->query();

        $this->redirect("index");
    }

    public function actionShowsetpermission($id){

        $this->data['id'] = $id;

        $admingroup = $this->connection->createCommand("select * from xm_admingroup where admingroup_id = {$id}")->queryRow();
        $this->data['admingroup'] = $admingroup;

        $permission = $this->connection->createCommand("select * from xm_admingroup_permission where admingroup_id = {$id}")->queryRow();
        $permission['operpermissionJSON'] = json_encode(unserialize($permission['operpermission']));
        $permission['modulepermissionJSON'] = json_encode(unserialize($permission['modulepermission']));
        $permission['actpermissionJSON'] = json_encode(unserialize($permission['actpermission']));
        $this->data['permission'] = $permission;

        $this->render("showsetpermission",$this->data);
    }

    public function actionSavePermission(){
        $admingroup_id = $_POST['admingroup_id'];
        $operpermission = $_POST['operpermission'];
        $operpermission_array = "";
        if($operpermission!=""){
            $operpermission_array = Array();
            foreach($operpermission as $o){
                $operpermission_array[$o]= $o;
            }
            $operpermission_array = serialize($operpermission_array);
            $operpermission = serialize($operpermission);
        }
        $modulepermission = $_POST['modulepermission'];
        if($modulepermission!=""){
            $modulepermission = serialize($modulepermission);
        }
        $actpermission = $_POST['actpermission'];
        $actpermission_array = "";
        if($actpermission!=""){
            $actpermission_array = Array();
            foreach($actpermission as $o){
                $actpermission_array[$o]= $o;
            }
            $actpermission_array = serialize($actpermission_array);
            $actpermission = serialize($actpermission);
        }
        $authlang = $_POST['authlang'];
        $self = $_POST['self'];
        if($self == ''){
            $self = 0;
        }

        $exist = $this->connection->createCommand("select count(*) from xm_admingroup_permission where admingroup_id = {$admingroup_id}")->queryScalar();
        if($exist==0){
            //添加一条
            $this->connection->createCommand("insert into xm_admingroup_permission (admingroup_id,authlang,self,operpermission,modulepermission,actpermission,operpermissionarray,actpermissionarray) values
            ({$admingroup_id},'{$authlang}',{$self},'{$operpermission}','{$modulepermission}','{$actpermission}','{$operpermission_array}','{$actpermission_array}')")->query();

        }else{
            //修改记录
            $this->connection->createCommand("update xm_admingroup_permission set authlang = '{$authlang}',self={$self},operpermission ='{$operpermission}',modulepermission='{$modulepermission}',actpermission='{$actpermission}',operpermissionarray='{$operpermission_array}',actpermissionarray='{$actpermission_array}'  where admingroup_id = {$admingroup_id} ")->query();
        }

        $this->redirect("index");
    }


}