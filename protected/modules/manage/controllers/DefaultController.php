<?php
Yii::import("application.modules.manage.util.LoginStat");
class DefaultController extends Controller
{	
	public function actionIndex()
	{
		$user = Yii::app ()->session ['user'];
		if ($user != null) {
			$this->redirect('/index.php/manage/console/index');
			exit();
		}

		if($_REQUEST['sub_DefaultIndex']!=null){

			$loginname = $_REQUEST['loginname'];
			$loginpass = $_REQUEST['loginpass'];

			$row = $this->connection->createCommand("select * from xm_user where loginname = '".$loginname."' and isadmin = 1 ")->queryRow();
			if($row!=null){
				if($row['loginpass']==md5($loginpass)){
					$loginstat = new LoginStat();
					$loginnum = $row['login_num']+1;

					$this->connection->createCommand("update xm_user set last_loginip = '".$loginstat->GetIP()."',last_logintime=now(),login_num= ".$loginnum." where user_id = ".$row['user_id'])->query();

                    session_start();
                    $_SESSION['IsAuthorized'] = true;
                    $userobj = Array();
                    $user = $this->connection->createCommand("select * from xm_user where user_id = ".$row['user_id'])->queryRow();
                    $userobj['user'] = $user;
                    $admingrouppermission = $this->connection->createCommand("select * from xm_admingroup_permission where admingroup_id = ".$user['admingroup_id'])->queryRow();
                    $userobj['admingrouppermission'] = $admingrouppermission;

                    if($loginname!='administrator'){
                        $authlang = $admingrouppermission['authlang'];
                        if($authlang != 'All'){
                            Yii::app()->session['mgrlang'] = $authlang;
                        }
                    }else{
                        Yii::app()->session['mgrlang'] = 'zh_cn';
                    }

					Yii::app()->session['user'] = $userobj;

					$this->redirect('/index.php/manage/console/index');
				}else{
					$this->render('index',Array('message'=>'密码输入有误'));
				}
			}else{
				$this->render('index',Array('message'=>'用户名不存在'));
			}
		}else{
			$this->render('index');
		}
	}

	
	public function actionLogout()
	{
		unset(Yii::app()->session['user']);
		$this->redirect('/index.php/manage/default/index');
	}
}