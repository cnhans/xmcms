<?php
Yii::import("application.modules.manage.util.PageBar");
class ProductController extends ManageController {
	
	public function actionIndex($current=1) {
		
		$pb = new PageBar();
		$pb->current = $current;
		$pb->total = $this->connection->createCommand("select count(product_id) from xm_product where lang = '".$this->mgrlang."' ".$this->self." ".$this->inmodule_1." ")->queryScalar();
		
		$products = $this->connection->createCommand("select p.*,m.category from xm_product p left join xm_module m on p.module_id = m.module_id  where p.lang = '".$this->mgrlang."' ".$this->self." ".$this->inmodule_2." order by p.orderby asc limit ".$pb->getStart().",".$pb->rows)->queryAll();

		$this->render ('index',array(			
			'pagebar'=>$pb,
			'products'=>$products
		));
	}

    public function actionShowedit($id,$current){

        if($id !=0){
            $product = $this->connection->createCommand("select * from xm_product where product_id = {$id}")->queryRow();
            $this->data['product'] = $product;

            $tags = $this->connection->createCommand(" select * from xm_tag where relid = {$id} and type = 'product'")->queryAll();
            $tagstr = "";
            if(sizeof($tags)>0){
                $i=0;
                foreach($tags as $t){
                    if($i==0){
                        $tagstr.="{$t['tag']}";
                    }else{
                        $tagstr.=",{$t['tag']}";
                    }
                    $i++;
                }
            }
            $this->data['tagstr'] = $tagstr;
        }

        $this->data['current'] = $current;
        $this->data['id'] = $id;

        $this->render("showedit",$this->data);
    }

    public function actionEdit(){
        $product_id = $_POST['product_id'];
        $productname = $_POST['productname'];
        $productimage = $_POST['productimage'];
        $productdesc = $_POST['productdesc'];
        $module_id = $_POST['module_id'];
        $orderby = $_POST['orderby'];
        $current = $_POST['current'];
        $tags = $_POST['tags'];

        if($product_id == 0){
            //添加
            $this->connection->createCommand("insert into xm_product (productname,productimage,productdesc,createtime,module_id,lang,orderby) values ('{$productname}','{$productimage}','{$productdesc}',now(),$module_id,'".$this->mgrlang."',{$orderby}) ")->query();
            $product_id =  Yii::app()->db->getLastInsertID();
        }else{
            //修改
            $this->connection->createCommand("update xm_product set productname = '{$productname}',productimage = '{$productimage}',productdesc = '{$productdesc}',module_id={$module_id},orderby='{$orderby}' where product_id = {$product_id}  ")->query();
        }

        if($product_id!=0){
            $this->connection->createCommand("delete from xm_tag where relid = {$product_id} and type = 'product'")->query();
        }

        $tag_array = explode(",", $tags);
        foreach($tag_array as $tag){
            $this->connection->createCommand("insert into xm_tag (tag,type,relid,lang) values ('{$tag}','product',{$product_id},'".$this->mgrlang."')")->query();
        }

        $this->redirect('index?current='.$current);
    }

    public function actionDel($id,$current){

        $this->connection->createCommand("delete from xm_product where product_id = {$id}")->query();
        $this->connection->createCommand("delete from xm_tag where type = 'product' and relid =  {$id}")->query();

        $this->redirect('index?current='.$current);
    }

    public function actionDels(){
        $ids = $_REQUEST['ids'];

        $this->connection->createCommand("delete from xm_product where image_id = {$ids}")->query();
        $this->connection->createCommand("delete from xm_tag where relid in ({$ids} and type = 'product'")->query();

        echo 1;
    }
	
}