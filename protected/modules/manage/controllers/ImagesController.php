<?php
Yii::import("application.modules.manage.util.PageBar");
class ImagesController extends ManageController {
	
	public function actionIndex($current=1) {
		
		$pb = new PageBar();
		$pb->current = $current;
		$pb->total = $this->connection->createCommand("select count(image_id) from xm_images where lang = '".$this->mgrlang."' ".$this->self." ".$this->inmodule_1." ")->queryScalar();
		
		$images = $this->connection->createCommand("select img.*,m.category from xm_images img left join xm_module m on img.module_id = m.module_id  where img.lang = '".$this->mgrlang."' ".$this->self." ".$this->inmodule_2." order by img.orderby asc limit ".$pb->getStart().",".$pb->rows)->queryAll();
		
		$this->render ('index',array(
				'pagebar'=>$pb,
				'images'=>$images
		));
	}

    public function actionShowedit($id,$current){

        if($id !=0){
            $image = $this->connection->createCommand("select * from xm_images where image_id = {$id}")->queryRow();
            $this->data['image'] = $image;

            $tags = $this->connection->createCommand(" select * from xm_tag where relid = {$id} and type = 'image'")->queryAll();
            $tagstr = "";
            if(sizeof($tags)>0){
                $i=0;
                foreach($tags as $t){
                    if($i==0){
                        $tagstr.="{$t['tag']}";
                    }else{
                        $tagstr.=",{$t['tag']}";
                    }
                    $i++;
                }
            }
            $this->data['tagstr'] = $tagstr;
        }

        $this->data['id'] = $id;
        $this->data['current'] = $current;

        $this->render("showedit",$this->data);
    }

    public function actionEdit(){
        $image_id = $_POST['image_id'];
        $current = $_POST['current'];
        $module_id = $_POST['module_id'];
        $title = $_POST['title'];
        $description = $_POST['description'];
        $image = $_POST['image'];
        $tags = $_POST['tags'];
        $orderby = $_POST['orderby'];

        $user = Yii::app()->session['user'];

        if($image_id!=0){
            $this->connection->createCommand("delete from xm_tag where relid = {$image_id} and type = 'image'")->query();
        }

        if($image_id == 0){
            //添加
            $this->connection->createCommand("insert into xm_images (title,image,description,createtime,sender_id,lang,module_id,orderby) values ('{$title}','{$image}','{$description}',now(),'{$user['user']['user_id']}','".$this->mgrlang."',{$module_id},{$orderby}) ")->query();
            $image_id =  Yii::app()->db->getLastInsertID();
        }else{
            //修改
            $this->connection->createCommand("update xm_images set title = '{$title}',image='{$image}',description='{$description}',module_id='{$module_id}',orderby={$orderby} where image_id = {$image_id}  ")->query();
        }

        $tag_array = explode(",", $tags);
        foreach($tag_array as $tag){
            $this->connection->createCommand("insert into xm_tag (tag,type,relid) values ('{$tag}','image',{$image_id})")->query();
        }

        $this->redirect('index?current='.$current);
    }

    public function actionDel($id,$current){

        $this->connection->createCommand("delete from xm_images where image_id = {$id}")->query();
        $this->connection->createCommand("delete from xm_tag where relid in ({$id} and type = 'image'")->query();

        $this->redirect('index?current='.$current);
    }


    public function actionDels(){
        $ids = $_REQUEST['ids'];

        $this->connection->createCommand("delete from xm_images where image_id in ({$ids})")->query();
        $this->connection->createCommand("delete from xm_tag where relid in ({$ids}) and type = 'image'")->query();

        echo 1;
    }

}
