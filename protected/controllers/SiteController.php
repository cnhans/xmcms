<?php
Yii::import("application.util.PortalCache");
class SiteController extends PortalController
{
	public function actionIndex()
	{
        $this->data['active']="home";
		$this->render('index',$this->data);
	}

    public function actionChangelang(){
        Yii::app()->request->cookies['lang'] = new CHttpCookie('lang', $_GET['lang']);;
        echo true;
    }

}