<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class EmployeeController extends PortalController
{

    public function actionIndex(){

        $portalcache = new PortalCache(Yii::app(),$this);
        $pb = new PageBar();

        $id = $_REQUEST['id'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','employee','getmodule_employee');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $args = Array();
        $args['module'] = $module;
        $args['page'] = $page;
        $args['rows'] = $pb->rows;
        $this->data['employees'] = $portalcache->getLwithArgs('getEmployees',$args,'employee_'.$id.'_'.$page);

        $pb->page = $page;
        $pb->rowcounts = $portalcache->getLwithArgs('getEmployeeRowcounts',$args,'getEmployeeRowcounts_'.$id);
        $pb->url = "/index.php/employee/index/id/{$id}";
        $this->data['pb'] = $pb;

        $this->render('index',$this->data);

    }

    public function actionView($id){
        $portalcache = new PortalCache(Yii::app(),$this);
        $employee = $portalcache->getLwithArgs('getemployeebyid',$id,'getemployee_'.$id);

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$employee['module_id'],'menulist_'.$employee['module_id']);
        $this->data['employee'] = $employee;

        $this->render('view',$this->data);
    }

}