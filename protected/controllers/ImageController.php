<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class ImageController extends PortalController
{
    public function actionIndex(){

        $portalcache = new PortalCache(Yii::app(),$this);
        $pb = new PageBar();

        $id = $_REQUEST['id'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','image','getmodule_image');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $args = Array();
        $args['module'] = $module;
        $args['page'] = $page;
        $args['rows'] = $pb->rows;
        $this->data['images'] = $portalcache->getLwithArgs('getImages',$args,'image_'.$id.'_'.$page);

        $pb->page = $page;
        $pb->rowcounts = $portalcache->getLwithArgs('getImageRowcounts',$args,'getImageRowcounts_'.$id);
        $pb->url = "/index.php/image/index/id/{$id}";
        $this->data['pb'] = $pb;

        $this->data['active']=$module['module_id'];

        $this->render('index',$this->data);
    }

    public function actionView($id){
        $portalcache = new PortalCache(Yii::app(),$this);
        $image = $portalcache->getLwithArgs('getimagebyid',$id,'getimage_'.$id);

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$image['module_id'],'menulist_'.$image['module_id']);
        $this->data['image'] = $image;

        $this->data['module'] = $portalcache->getLwithArgs('getmodulebyid',$image['module_id'],'getmodulebyid_'.$image['module_id']);
        $this->data['active']=$image['module_id'];

        $this->render('view',$this->data);
    }

}