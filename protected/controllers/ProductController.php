<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class ProductController extends PortalController
{
    public function actionIndex(){

        $portalcache = new PortalCache(Yii::app(),$this);
        $pb = new PageBar();

        $id = $_REQUEST['id'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','product','getmodule_product');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $args = Array();
        $args['module'] = $module;
        $args['page'] = $page;
        $args['rows'] = $pb->rows;
        $this->data['products'] = $portalcache->getLwithArgs('getProducts',$args,'product_'.$id.'_'.$page);

        $pb->page = $page;
        $pb->rowcounts = $portalcache->getLwithArgs('getProductsRowcounts',$args,'getProductsRowcounts_'.$id);
        $pb->url = "/index.php/product/index/id/{$id}";
        $this->data['pb'] = $pb;

        $this->data['active']=$module['module_id'];

        $this->render('index',$this->data);

    }

    public function actionView($id){
        $portalcache = new PortalCache(Yii::app(),$this);
        $product = $portalcache->getLwithArgs('getproductbyid',$id,'getproduct_'.$id);

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$product['module_id'],'menulist_'.$product['module_id']);
        $this->data['product'] = $product;
        $this->data['module'] = $portalcache->getLwithArgs('getmodulebyid',$product['module_id'],'getmodulebyid_'.$product['module_id']);
        $this->data['active']=$product['module_id'];

        $this->render('view',$this->data);
    }

}