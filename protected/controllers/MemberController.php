<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class MemberController extends PortalController
{

    public function filters() {
        $member = Yii::app ()->session ['member'];
        if($member==null){
            $this->redirect("/index.php/user/index");
        }
    }

    public function actionIndex(){

        $member = Yii::app ()->session ['member'];

        $feedback_counts = $this->connection->createCommand("select count(*) from xm_feedback where user_id = {$member['user_id']}")->queryScalar();
        $this->data['feedback_counts'] = $feedback_counts;
        $feedback_noread = $this->connection->createCommand("select count(*) from xm_feedback where user_id = {$member['user_id']} and haveread = 0")->queryScalar();
        $this->data['feedback_noread'] = $feedback_noread;
        $feedback_haveread = $this->connection->createCommand("select count(*) from xm_feedback where user_id = {$member['user_id']}  and haveread = 1")->queryScalar();
        $this->data['feedback_haveread'] = $feedback_haveread;

        $message_counts = $this->connection->createCommand("select count(*) from xm_message where user_id = {$member['user_id']}")->queryScalar();
        $this->data['message_counts'] = $message_counts;
        $message_noread = $this->connection->createCommand("select count(*) from xm_message where user_id = {$member['user_id']} and haveread = 0")->queryScalar();
        $this->data['message_noread'] = $message_noread;
        $message_haveread = $this->connection->createCommand("select count(*) from xm_message where user_id = {$member['user_id']}  and haveread = 1")->queryScalar();
        $this->data['message_haveread'] = $message_haveread;

        $this->render('index',$this->data);
    }

    function actionLoginout(){
        unset(Yii::app()->session['member']);
        $this->redirect('/index.php/user/index');
    }

    function actionShowedit(){

        $member = Yii::app ()->session ['member'];
        $this->data['user'] = $this->connection->createCommand("select * from xm_user where user_id = {$member['user_id']}")->queryRow();

        $this->render('showedit',$this->data);
    }

    function actionEdit(){

        $member = Yii::app ()->session ['member'];

        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $tel = $_POST['tel'];
        $newpass = $_POST['newpass'];

        if($newpass!=""){
            $this->connection->createCommand("update xm_user set phone = '{$phone}',email='{$email}',tel='{$tel}' where user_id = {$member['user_id']}")->query();
        }else{
            $newpass = md5($newpass);
            $this->connection->createCommand("update xm_user set phone = '{$phone}',email='{$email}',tel='{$tel}',loginpass='{$newpass}' where user_id = {$member['user_id']}")->query();
        }

        $this->redirect("showedit");
    }

    function actionFeedbacklst(){
        $member = Yii::app ()->session ['member'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];
        $pb = new PageBar();
        $start = ($page-1)*$pb->rows;
        $this->data['feedbacklst'] = $this->connection->createCommand("select * from xm_feedback where user_id = {$member['user_id']} limit {$start},{$pb->rows}")->queryAll();
        $pb->page = $page;
        $pb->rowcounts = $this->connection->createCommand("select count(*) from xm_feedback where user_id = {$member['user_id']}")->queryScalar();
        $pb->url = "/index.php/member/feedbacklst";
        $this->data['pb'] = $pb;

        $this->render('feedbacklst',$this->data);
    }

    function actionMessagelst(){

        $member = Yii::app ()->session ['member'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];
        $pb = new PageBar();
        $start = ($page-1)*$pb->rows;
        $this->data['messagelst'] = $this->connection->createCommand("select * from xm_message where user_id = {$member['user_id']} limit {$start},{$pb->rows}")->queryAll();
        $pb->page = $page;
        $pb->rowcounts = $this->connection->createCommand("select count(*) from xm_message where user_id = {$member['user_id']}")->queryScalar();
        $pb->url = "/index.php/member/messagelst";
        $this->data['pb'] = $pb;

        $this->render('messagelst',$this->data);
    }

    function actionMessagelstshow(){
        $mid = $_REQUEST['mid'];
        $this->data['message'] = $this->connection->createCommand("select m.*,u.username replyer from xm_message m left join xm_user u on m.user_id =  u.user_id where message_id = {$mid}")->queryRow();
        $this->render('messagelstshow',$this->data);
    }

    function actionFeedbacklstshow(){
        $fid = $_REQUEST['fid'];
        $this->data['feedback'] = $this->connection->createCommand("select * from xm_feedback f left join xm_feedbacktype ft on f.feedbacktype_id = ft.feedbacktype_id  where feedback_id = {$fid}")->queryRow();
        $this->render('feedbacklstshow',$this->data);
    }

    function actionFeedbacklstedit(){
        $fid = $_REQUEST['fid'];
        $this->data['feedback'] = $this->connection->createCommand("select * from xm_feedback f left join xm_feedbacktype ft on f.feedbacktype_id = ft.feedbacktype_id  where feedback_id = {$fid}")->queryRow();
        $this->render('feedbacklstedit',$this->data);
    }

}