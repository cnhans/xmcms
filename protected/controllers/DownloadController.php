<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class DownloadController extends PortalController
{
    public function actionIndex(){

        $portalcache = new PortalCache(Yii::app(),$this);
        $pb = new PageBar();

        $id = $_REQUEST['id'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','download','getmodule_download');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $args = Array();
        $args['module'] = $module;
        $args['page'] = $page;
        $args['rows'] = $pb->rows;
        $this->data['downloads'] = $portalcache->getLwithArgs('getDownloads',$args,'download_'.$id.'_'.$page);

        $pb->page = $page;
        $pb->rowcounts = $portalcache->getLwithArgs('getDownloadRowcounts',$args,'getDownloadRowcounts_'.$id);
        $pb->url = "/index.php/download/index/id/{$id}";
        $this->data['pb'] = $pb;

        $this->data['active']=$module['module_id'];

        $this->render('index',$this->data);

    }

    public function actionView($id){
        $portalcache = new PortalCache(Yii::app(),$this);
        $download = $portalcache->getLwithArgs('getdownloadbyid',$id,'getdownload_'.$id);

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$download['module_id'],'menulist_'.$download['module_id']);
        $this->data['download'] = $download;
        $this->data['module'] = $portalcache->getLwithArgs('getmodulebyid',$download['module_id'],'getmodulebyid_'.$download['module_id']);
        $this->data['active']=$download['module_id'];
        $this->render('view',$this->data);
    }

}