<?php
Yii::import("application.util.PortalCache");
class GuideController extends PortalController
{
    public function actionIndex()
    {
        $id = $_REQUEST['id'];

        $portalcache = new PortalCache(Yii::app(),$this);
        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);

        $this->data['guide'] = $portalcache->getLwithArgs('guide',$id,'guide_'.$id);
        $this->data['id'] = $id;

        $this->render('index',$this->data);
    }
}