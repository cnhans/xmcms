<?php
Yii::import("application.util.PortalCache");
class FeedbackController extends PortalController
{

    public function actionIndex(){

        $error = $_REQUEST['error'];
        $this->data['error'] = $error;

        $portalcache = new PortalCache(Yii::app(),$this);

        $id = $_REQUEST['id'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','feedback','getmodule_feedback');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $this->data['feedbacktypes'] = $portalcache->getL("getFeedbacktype");

        
        $this->render('index',$this->data);

    }

    public function actionFeedback(){
        session_start();
        $captcha = $_REQUEST['captcha'];
        if (empty($_SESSION['captcha']) || trim(strtolower($captcha)) != $_SESSION['captcha']) {
            $this->redirect('/index.php/feedback/index?error=3');
        }else{
            //数据录入
            $user = $_POST['user'];
            $title = $_POST['title'];
            $descrption = $_POST['descrption'];
            $feedbacktype_id = $_POST['feedbacktype_id'];
            $email = $_POST['email'];
            $module_id = $_POST['module_id'];

            $member = Yii::app ()->session ['member'];
            if($member!=null){
                //当前用户已经登入
                $this->connection->createCommand("insert into xm_feedback
                (user_id,title,`user`,description,createtime,lang,module_id,feedbacktype_id,email)
                values
                ({$member['user_id']},'{$title}','{$user}','{$descrption}',now(),'".Yii::app()->language."',{$module_id},{$feedbacktype_id},'{$email}')")->query();
            }else{
                $this->connection->createCommand("insert into xm_feedback
                (title,`user`,description,createtime,lang,module_id,feedbacktype_id,email)
                values
                ('{$title}','{$user}','{$descrption}',now(),'".Yii::app()->language."',{$module_id},{$feedbacktype_id},'{$email}')")->query();
            }

            $this->redirect('/index.php/feedback/index/id/'.$module_id.'?error=-1');
        }
    }


}