<?php
Yii::import("application.util.PortalCache");
Yii::import("application.util.PageBar");
class ArticleController extends PortalController
{
    public function actionIndex(){

        $portalcache = $this->data['portalcache'];
        $pb = new PageBar();

        $id = $_REQUEST['id'];
        $page = $_REQUEST['page']==null?1:$_REQUEST['page'];

        $module = null;
        if($id==null){
            $module = $portalcache->getLwithArgs('getmodule','article','getmodule_article');
            $id = $module['module_id'];
            $this->data['module'] = $module;
        }else{
            $module = $portalcache->getLwithArgs('getmodulebyid',$id,'getmodulebyid_'.$id);
            $this->data['module'] = $module;
        }

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$id,'menulist_'.$id);
        $this->data['id'] = $id;

        $args = Array();
        $args['module'] = $module;
        $args['page'] = $page;
        $args['rows'] = $pb->rows;
        $this->data['articles'] = $portalcache->getLwithArgs('getArticles',$args,'product_'.$id.'_'.$page);

        $pb->page = $page;
        $pb->rowcounts = $portalcache->getLwithArgs('getArticlesRowcounts',$args,'getProductsRowcounts_'.$id);
        $pb->url = "/index.php/article/index/id/{$id}";
        $this->data['pb'] = $pb;

        $this->data['active']=$module['module_id'];

        $this->render('index',$this->data);
    }

    public function actionView(){

        $id = $_REQUEST['id'];
        $portalcache = new PortalCache(Yii::app(),$this);

        $article = $portalcache->getLwithArgs('getArticlesById',$id,'getArticlesById_'.$id);
        $this->data['article'] = $article;

        $this->data['menulist'] = $portalcache->getLwithArgs('menulist',$article['module_id'],'menulist_'.$article['module_id']);

        $this->data['module'] = $portalcache->getLwithArgs('getmodulebyid',$article['module_id'],'getmodulebyid_'.$article['module_id']);
        $this->data['active']=$article['module_id'];

        $this->render('view',$this->data);
    }

}