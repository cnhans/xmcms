<?php
require Yii::app()->basePath.'/util/CrmFun.php';
Yii::import("application.util.PortalCache");
class PortalController extends CController
{
	
	public $layout='//layouts/portal';
	public $menu=array();
	public $breadcrumbs=array();
    public $connection;
    public $data = array();
	
	public function init()
	{
		if(isset($_GET['lang']) && $_GET['lang'] != "")
		{
			// 通过传递参数更改语言
			Yii::app()->language = $_GET['lang'];
			// 设置COOKIE，
			Yii::app()->request->cookies['lang'] = new CHttpCookie('lang', $_GET['lang']);
		}
		else if (isset(Yii::app()->request->cookies['lang']) && Yii::app()->request->cookies['lang']->value != "")
		{
			// 根据COOKIE中语言类型来设置语言
			Yii::app()->language = Yii::app()->request->cookies['lang']->value;
		}
        /*
		else
		{
			// 根据浏览器语言来设置语言
			$lang = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
			Yii::app()->language = strtolower(str_replace('-', '_', $lang[0]));
		}*/
		
		if(Yii::app()->language!='zh_cn'){
			Yii::app()->sourceLanguage = 'zh_cn';
		}

        $this->connection = Yii::app()->db;

        $this->initAppCache();
	}

    public function initAppCache(){

        $portalcache = new PortalCache(Yii::app(),$this);
        $this->data['modules'] = $portalcache->getL('modules');
        $this->data['config_basic'] =$portalcache->getL('config_basic');
        $this->data['flashs'] =$portalcache->getL('flashs');
        $this->data['other'] =$portalcache->getL('other');
    }
	
	public function langurl($lang = 'en_us')
	{
		if ($lang == Yii::app()->language)
		{
			return null;
		}
	
		$current_uri = Yii::app()->request->requestUri;
		if($current_uri == '/index.php' ){
			return $current_uri . '/site/index/lang/'.$lang;
		}else if($current_uri=='/'){
			return 'index.php/site/index/lang/'. $lang;
		}
		
		if (strrpos($current_uri, 'lang/')) {
			//防止生成的 url 传值出现重复 lang/zh_cn
			$langstr = 'lang/' . Yii::app()->language;
			// /lang/zh_cn/
			$current_uri = str_replace('/' . $langstr . '/', '/', $current_uri);
			// /lang/zh_cn
			$current_uri = str_replace('/' . $langstr, '', $current_uri);
		}
		return $current_uri . '/lang/' . $lang;
	}




}
?>
<?php
//echo CHtml::link ( ' 中文简体 ' , $this->langurl('zh_cn')) . '| ' .
//    CHtml::link ( ' English ' ,$this->langurl('en_us')) ;
?>
<?php
//echo Yii::t('index','username');
//echo Yii::t('index','username','zh_cn');
//echo Yii::t('index','username','en_us');
?>