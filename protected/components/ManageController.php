<?php
class ManageController extends CController
{
	public $layout='//../../../protected/modules/manage/views/layouts/manage';
	public $menu=array();
	public $breadcrumbs=array();
	public $mgrlang;
	public $connection;
    public $data = array();
    public $self = "";
    public $inmodule_1 = "";
    public $inmodule_2 = "";
    public $user;

	
	public function filters() {
		
		$current_uri = Yii::app()->request->requestUri;
		if($current_uri=='/index.php/manage/console/index'){
			$this->layout = "//../../../protected/modules/manage/views/layouts/manage_frame";
		}

		if(isset(Yii::app()->session['mgrlang'])){
			$this->mgrlang = Yii::app()->session['mgrlang'];
		}else{
			$this->mgrlang = 'zh_cn';
		}
		
		$this->connection = Yii::app()->db;

        $user = Yii::app ()->session ['user'];
        $this->user = $user;
        if($user!=null){
            if($user['user']['loginname']!='administrator'){
                $self = $user['admingrouppermission']['self'];
                if($self == 1){
                    $this->self = "and sender_id = ".$user['user']['user_id']." ";
                }

                $modulepermission = $user['admingrouppermission']['modulepermission'];
                $modulepermission = unserialize($modulepermission);
                if($modulepermission == ""){
                    $modulepermission = -1;
                }
                $this->inmodule_1 =  " and module_id in (".$modulepermission.") ";
                $this->inmodule_2 =  " and m.module_id in (".$modulepermission.") ";
            }
        }
		
		return array (
				array('application.filters.AuthorityFilter')
		);
	}

    public function actPermission($html,$act){
        //All,Add,Edit,Del
        if($this->user['user']['loginname'] == 'administrator'){
            echo $html;
            return;
        }
        if($this->user['admingrouppermission']['actpermissionarray']==""){
            return false;
        }
        if(array_key_exists($act,unserialize($this->user['admingrouppermission']['actpermissionarray']))){
            echo $html;
            return;
        }
        echo "";
    }

    //判断多个权限是否存在其中一个
    public function actExist($actStr){
        //All,Add,Edit,Del
        $actArray = explode(',',$actStr);
        $permission = false;
        if($this->user['user']['loginname'] == 'administrator'){
            return true;
        }
        if($this->user['admingrouppermission']['actpermissionarray']==""){
            return false;
        }
        if(array_key_exists('All',unserialize($this->user['admingrouppermission']['actpermissionarray']))){
            return true;
        }
        foreach($actArray as $a){
            if(array_key_exists($a,unserialize($this->user['admingrouppermission']['actpermissionarray']))){
                $permission = true;
                break;
            }
        }
        return $permission;
    }
	
}